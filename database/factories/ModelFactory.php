<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'activated' => true,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Employee::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'username' => $faker->sentence,
        'password' => bcrypt($faker->password),
        'employee_type' => $faker->boolean(),
        'is_active' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Employee::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->sentence,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'first_name' => $faker->firstName,
        'second_name' => $faker->sentence,
        'third_name' => $faker->sentence,
        'last_name' => $faker->lastName,
        'mobile_number' => $faker->sentence,
        'birthday' => $faker->date(),
        'gender' => $faker->sentence,
        'country_id' => $faker->randomNumber(5),
        'user_type' => $faker->boolean(),
        'user_status' => $faker->boolean(),
        'client_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Destroy::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\user::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->sentence,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'first_name' => $faker->firstName,
        'second_name' => $faker->sentence,
        'third_name' => $faker->sentence,
        'last_name' => $faker->lastName,
        'mobile_number' => $faker->sentence,
        'birthday' => $faker->date(),
        'gender' => $faker->sentence,
        'country_id' => $faker->randomNumber(5),
        'user_type' => $faker->boolean(),
        'user_status' => $faker->boolean(),
        'client_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->sentence,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'first_name' => $faker->firstName,
        'second_name' => $faker->sentence,
        'third_name' => $faker->sentence,
        'last_name' => $faker->lastName,
        'mobile_number' => $faker->sentence,
        'birthday' => $faker->date(),
        'gender' => $faker->sentence,
        'country_id' => $faker->randomNumber(5),
        'user_type' => $faker->boolean(),
        'user_status' => $faker->boolean(),
        'client_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'api_url' => $faker->sentence,
        'website_url' => $faker->sentence,
        'is_active' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\UserLevel::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'minimum_points' => $faker->randomNumber(5),
        'bonus_factor' => $faker->randomFloat,
        'maximum_number_of_reservations' => $faker->randomFloat,
        'user_type' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\PaymentMethod::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'is_active' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Reward::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Penalty::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'points' => $faker->randomNumber(5),
        'user_type' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Reward::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'points' => $faker->randomNumber(5),
        'user_type' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'is_active' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Question::class, function (Faker\Generator $faker) {
    return [
        'header' => $faker->sentence,
        'body' => $faker->text(),
        'question_type_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\QuestionType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'is_active' => $faker->boolean(),
        'user_type' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\QuestionsSubmission::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomNumber(5),
        'question_id' => $faker->randomNumber(5),
        'answer' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        'deleted_at' => null,
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Writer::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\QA::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Quality::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\SystemWriter::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\SystemProofreader::class, function (Faker\Generator $faker) {
    return [
        
        
    ];
});

