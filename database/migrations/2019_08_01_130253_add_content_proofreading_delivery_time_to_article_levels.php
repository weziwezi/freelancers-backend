<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentProofreadingDeliveryTimeToArticleLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_levels', function($table) {
            $table->integer('content_proofreading_delivery_time')->after('language_proofreading_delivery_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels', function($table) {
            $table->dropColumn('content_proofreading_delivery_time');
        });
    }
}
