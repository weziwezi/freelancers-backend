<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentProofreaderUserIdToArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function($table) {
            $table->integer('content_proofreader_user_id')->unsigned()->nullable()->after('language_proofreader_user_id');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('content_proofreader_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['content_proofreader_user_id']);
        });

        Schema::table('articles', function($table) {
            $table->dropColumn('content_proofreader_user_id');
        });
    }
}
