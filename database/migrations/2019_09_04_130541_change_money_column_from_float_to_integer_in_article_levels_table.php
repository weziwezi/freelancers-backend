<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMoneyColumnFromFloatToIntegerInArticleLevelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        
        DB::statement('UPDATE `article_levels` SET `writing_price`=`writing_price` * 100,`language_proofreading_price`=`language_proofreading_price` * 100,`content_proofreading_price`=`content_proofreading_price` * 100,`proofreading_price`=`proofreading_price` * 100,`images_uploading_price`=`images_uploading_price` * 100');
        Schema::table('article_levels', function (Blueprint $table) {

            $table->unsignedInteger('writing_price')->comment('Value X 100')->default(0)->change();
            $table->unsignedInteger('language_proofreading_price')->comment('Value X 100')->default(0)->change();
            $table->unsignedInteger('content_proofreading_price')->comment('Value X 100')->default(0)->change();
            $table->unsignedInteger('proofreading_price')->comment('Value X 100')->default(0)->change();
            $table->unsignedInteger('images_uploading_price')->comment('Value X 100')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels', function (Blueprint $table) {
            $table->float('writing_price')->comment('')->change();
            $table->float('language_proofreading_price')->comment('')->change();
            $table->float('content_proofreading_price')->comment('')->change();
            $table->float('proofreading_price')->comment('')->change();
            $table->float('images_uploading_price')->comment('')->change();
        });
        DB::statement('UPDATE `article_levels` SET `writing_price`=`writing_price` / 100,`language_proofreading_price`=`language_proofreading_price` / 100,`content_proofreading_price`=`content_proofreading_price` / 100,`proofreading_price`=`proofreading_price` / 100,`images_uploading_price`=`images_uploading_price` / 100');
    }

}
