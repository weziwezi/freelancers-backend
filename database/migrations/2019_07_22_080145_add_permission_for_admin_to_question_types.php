<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForAdminToQuestionTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 64, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 65, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 66, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 67, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 68, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 69, 'role_id' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
