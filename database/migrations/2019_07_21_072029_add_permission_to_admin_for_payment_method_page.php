<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionToAdminForPaymentMethodPage extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 34, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 35, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 36, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 37, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 38, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 39, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
