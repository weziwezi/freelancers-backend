<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesUploaderUserIdToArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function($table) {
            $table->integer('images_uploader_user_id')->nullable()->unsigned()->after('proofreader_user_id');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('images_uploader_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['images_uploader_user_id']);
        });

        Schema::table('articles', function($table) {
            $table->dropColumn('images_uploader_user_id');
        });
    }
}
