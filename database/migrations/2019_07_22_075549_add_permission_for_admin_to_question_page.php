<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForAdminToQuestionPage extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 58, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 59, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 60, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 61, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 62, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 63, 'role_id' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
