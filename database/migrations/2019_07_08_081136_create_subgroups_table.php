<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subgroups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('client_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('subgroups', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subgroups', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropForeign(['group_id']);
        });

        Schema::dropIfExists('subgroups');
    }
}
