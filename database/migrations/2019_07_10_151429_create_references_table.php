<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('subgroup_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('references', function (Blueprint $table) {
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('references', function (Blueprint $table) {
            $table->dropForeign(['subgroup_id']);
        });

        Schema::dropIfExists('references');
    }
}
