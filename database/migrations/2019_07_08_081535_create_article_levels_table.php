<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('user_id')->unsigned();
            $table->integer('number_of_words');
            $table->integer('number_of_references');
            $table->integer('number_of_sub_headlines');
            $table->integer('number_of_internal_links');
            $table->integer('writing_delivery_time');
            $table->integer('proofreading_delivery_time');
            $table->float('writing_price');
            $table->float('proofreading_price');
            $table->tinyInteger('article_type');
            $table->integer('client_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('article_levels', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['client_id']);
        });

        Schema::dropIfExists('article_levels');
    }
}
