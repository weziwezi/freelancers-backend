<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionToAdminForRewardsPage extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 40, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 41, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 42, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 43, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 44, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 45, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
