<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsFromTextToLongTextInArticlesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->boolean('should_be_seen_by_freelancers')->nullable()->change();
            $table->boolean('should_be_seen_by_client_users')->nullable()->change();
            $table->longText('coordinator_notes')->comment(' ')->nullable()->change();
            $table->longText('proofreader_notes')->comment(' ')->nullable()->change();
            $table->longText('content')->comment(' ')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->text('content')->comment('')->nullable()->change();
            $table->text('proofreader_notes')->comment('')->nullable()->change();
            $table->text('coordinator_notes')->comment('')->nullable()->change();
            $table->string('should_be_seen_by_freelancers')->nullable()->change();
            $table->string('should_be_seen_by_client_users')->nullable()->change();
        });
    }

}
