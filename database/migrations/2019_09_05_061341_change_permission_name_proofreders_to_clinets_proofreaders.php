<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePermissionNameProofredersToClinetsProofreaders extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders\' WHERE `name`=\'admin.proofreader\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders.index\' WHERE `name`=\'admin.proofreader.index\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders.create\' WHERE `name`=\'admin.proofreader.create\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders.show\' WHERE `name`=\'admin.proofreader.show\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders.edit\' WHERE `name`=\'admin.proofreader.edit\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-proofreaders.delete\' WHERE `name`=\'admin.proofreader.delete\'');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader\' WHERE `name`=\'admin.clients-proofreaders\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader.index\' WHERE `name`=\'admin.clients-proofreaders.index\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader.create\' WHERE `name`=\'admin.clients-proofreaders.create\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader.show\' WHERE `name`=\'admin.clients-proofreaders.show\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader.edit\' WHERE `name`=\'admin.clients-proofreaders.edit\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.proofreader.delete\' WHERE `name`=\'admin.clients-proofreaders.delete\'');
    }

}
