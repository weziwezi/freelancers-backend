<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPaymentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_payment_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->boolean('is_active')->default(0);
            $table->integer('country_id')->nullable()->unsigned();
            $table->string('email')->nullable();
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('third_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('address')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_iban')->nullable();
            $table->string('national_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users_payment_info', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_payment_info', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['payment_method_id']);
            $table->dropForeign(['country_id']);
        });

        Schema::dropIfExists('users_payment_info');
    }
}
