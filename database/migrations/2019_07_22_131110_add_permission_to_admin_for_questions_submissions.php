<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionToAdminForQuestionsSubmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 70, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 71, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 72, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 73, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 74, 'role_id' => 2]);
        DB::table('role_has_permissions')->insert(['permission_id' => 75, 'role_id' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
