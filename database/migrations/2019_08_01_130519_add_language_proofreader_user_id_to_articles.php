<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguageProofreaderUserIdToArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function($table) {
            $table->integer('language_proofreader_user_id')->unsigned()->nullable()->after('writer_user_id');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('language_proofreader_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['language_proofreader_user_id']);
        });

        Schema::table('articles', function($table) {
            $table->dropColumn('language_proofreader_user_id');
        });
    }
}
