<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('conditions');
            $table->string('key_word');
            $table->integer('rejection_counter')->default(0);
            $table->tinyInteger('article_status')->default(1);
            $table->tinyInteger('article_type');
            $table->integer('article_level_id')->unsigned();
            $table->integer('subgroup_id')->unsigned();
            $table->integer('writer_user_id')->unsigned()->nullable();
            $table->integer('proofreader_user_id')->unsigned()->nullable();
            $table->time('writer_reservation_time')->nullable();
            $table->time('proofreader_reservation_time')->nullable();
            $table->boolean('should_be_seen_by_freelancers')->default(0);
            $table->boolean('should_be_seen_by_client_users')->default(0);
            $table->text('content')->nullable();
            $table->text('proofreader_notes')->nullable();
            $table->text('coordinator_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('article_level_id')->references('id')->on('article_levels');
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
            $table->foreign('writer_user_id')->references('id')->on('users');
            $table->foreign('proofreader_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['article_level_id']);
            $table->dropForeign(['subgroup_id']);
            $table->dropForeign(['writer_user_id']);
            $table->dropForeign(['proofreader_user_id']);
        });

        Schema::dropIfExists('articles');
    }
}
