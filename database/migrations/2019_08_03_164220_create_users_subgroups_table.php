<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSubgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_subgroups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('subgroup_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users_subgroups', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_subgroups', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['subgroup_id']);
        });

        Schema::dropIfExists('users_subgroups');
    }
}
