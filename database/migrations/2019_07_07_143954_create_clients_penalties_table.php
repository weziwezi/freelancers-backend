<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsPenaltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_penalties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('penalty_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('clients_penalties', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('penalty_id')->references('id')->on('penalties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients_penalties', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropForeign(['penalty_id']);
        });

        Schema::dropIfExists('clients_penalties');
    }
}
