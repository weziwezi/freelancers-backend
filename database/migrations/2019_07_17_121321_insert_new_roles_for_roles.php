<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertNewRolesForRoles extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::table('roles')->insert(
                array(
                    'name' => 'Admin',
                    'guard_name' => 'sup_admin',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
        ));
        DB::table('roles')->insert(
                array(
                    'name' => 'Finance',
                    'guard_name' => 'finance',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
