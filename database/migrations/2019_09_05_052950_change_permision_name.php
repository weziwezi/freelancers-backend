<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePermisionName extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers\' WHERE `name`=\'admin.writer\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers.index\' WHERE `name`=\'admin.writer.index\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers.create\' WHERE `name`=\'admin.writer.create\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers.show\' WHERE `name`=\'admin.writer.show\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers.edit\' WHERE `name`=\'admin.writer.edit\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.clients-writers.delete\' WHERE `name`=\'admin.writer.delete\'');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer\' WHERE `name`=\'admin.clients-writers\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer.index\' WHERE `name`=\'admin.clients-writers.index\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer.create\' WHERE `name`=\'admin.clients-writers.create\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer.show\' WHERE `name`=\'admin.clients-writers.show\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer.edit\' WHERE `name`=\'admin.clients-writers.edit\'');
        DB::statement('UPDATE `permissions` SET `name`=\'admin.writer.delete\' WHERE `name`=\'admin.clients-writers.delete\'');
    }

}
