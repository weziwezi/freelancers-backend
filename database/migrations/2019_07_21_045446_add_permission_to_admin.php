<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionToAdmin extends Migration 
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::table('role_has_permissions')->insert(['permission_id' => 28, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 29, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 30, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 31, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 32, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 33, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
