<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleLevelsUserLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_levels_user_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_level_id')->unsigned();
            $table->integer('user_level_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('article_levels_user_levels', function (Blueprint $table) {
            $table->foreign('article_level_id')->references('id')->on('article_levels');
            $table->foreign('user_level_id')->references('id')->on('user_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels_user_levels', function (Blueprint $table) {
            $table->dropForeign(['article_level_id']);
            $table->dropForeign(['user_level_id']);
        });

        Schema::dropIfExists('article_levels_user_levels');
    }
}
