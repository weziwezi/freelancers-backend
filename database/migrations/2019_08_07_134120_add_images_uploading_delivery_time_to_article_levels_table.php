<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesUploadingDeliveryTimeToArticleLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_levels', function($table) {
            $table->integer('images_uploading_delivery_time')->after('proofreading_delivery_time');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels', function($table) {
            $table->dropColumn('images_uploading_delivery_time');
        });
    }
}
