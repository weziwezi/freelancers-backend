<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminRoles extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::table('role_has_permissions')->insert(['permission_id' => 22, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 23, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 24, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 25, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 26, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 27, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
