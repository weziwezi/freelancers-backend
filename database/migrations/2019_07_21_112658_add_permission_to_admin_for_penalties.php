<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionToAdminForPenalties extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 46, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 47, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 48, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 49, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 50, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 51, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
