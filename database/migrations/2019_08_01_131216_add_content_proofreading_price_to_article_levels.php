<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContentProofreadingPriceToArticleLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_levels', function($table) {
            $table->float('content_proofreading_price')->after('language_proofreading_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_levels', function($table) {
            $table->dropColumn('content_proofreading_price');
        });
    }
}
