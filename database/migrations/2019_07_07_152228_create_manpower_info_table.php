<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManpowerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manpower_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('major_id')->unsigned()->nullable();
            $table->string('sub_major')->nullable();
            $table->integer('points')->default(0);
            $table->integer('user_level_id')->unsigned();
            $table->string('skype_account_id')->nullable();
            $table->string('profile_picture')->nullable();
            $table->string('certificate_url')->nullable();
            $table->string('national_id_url')->nullable();
            $table->json('certificates')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('manpower_info', function (Blueprint $table) {
            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('user_level_id')->references('id')->on('user_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manpower_info', function (Blueprint $table) {
            $table->dropForeign(['major_id']);
            $table->dropForeign(['user_level_id']);
        });

        Schema::dropIfExists('manpower_info');
    }
}
