<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminstratorQARole extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get Roles $ids
        $ids = DB::table('permissions')->select('id')->where('name', 'like', 'admin.q-a%')->get();
        foreach ($ids as $id) {
            DB::table('role_has_permissions')->insert(['permission_id' => $id->id, 'role_id' => 1]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $ids = DB::table('permissions')->select('id')->where('name', 'like', 'admin.q-a%')->get();
        foreach ($ids as $id) {
            DB::table('role_has_permissions')->where(['permission_id' => $id->id, 'role_id' => 1])->delete();
        }
    }

}
