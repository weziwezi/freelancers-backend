<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillPermissionsForGorup extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('role_has_permissions')->insert(['permission_id' => 52, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 53, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 54, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 55, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 56, 'role_id' => 1]);
        DB::table('role_has_permissions')->insert(['permission_id' => 57, 'role_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
