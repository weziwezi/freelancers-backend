<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSomeFieldsToNotNullableInArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['article_level_id']);
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn(['article_level_id']);
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->string('conditions')->nullable()->change();
            $table->string('key_word')->nullable()->change();
            $table->string('should_be_seen_by_freelancers')->nullable()->change();
            $table->string('should_be_seen_by_client_users')->nullable()->change();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->integer('article_level_id')->nullable()->unsigned()->after('article_type');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('article_level_id')->references('id')->on('article_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['article_level_id']);
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn(['article_level_id']);
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->string('conditions')->nullable(false)->change();
            $table->string('key_word')->nullable(false)->change();
            $table->string('should_be_seen_by_freelancers')->nullable(false)->change();
            $table->string('should_be_seen_by_client_users')->nullable(false)->change();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->integer('article_level_id')->nullable()->unsigned()->after('article_type');
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('article_level_id')->references('id')->on('article_levels');
        });
    }
}
