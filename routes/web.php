<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*
  Route::middleware(['web'])->group(function () {
  Route::namespace('Brackets\AdminAuth\Http\Controllers\Auth')->group(function () {
  Route::get('/login', 'LoginController@showLoginForm')->name('brackets/admin-auth::admin/login');
  Route::post('/login', 'LoginController@login');

  Route::any('/logout', 'LoginController@logout')->name('brackets/admin-auth::admin/logout');

  Route::get('/password-reset', 'ForgotPasswordController@showLinkRequestForm')->name('brackets/admin-auth::admin/password/showForgotForm');
  Route::post('/password-reset/send', 'ForgotPasswordController@sendResetLinkEmail');
  Route::get('/password-reset/{token}', 'ResetPasswordController@showResetForm')->name('brackets/admin-auth::admin/password/showResetForm');
  Route::post('/password-reset/reset', 'ResetPasswordController@reset');

  Route::get('/activation/{token}', 'ActivationController@activate')->name('brackets/admin-auth::admin/activation/activate');
  });
  });

  Route::middleware(['web', 'admin', 'auth:' . config('admin-auth.defaults.guard')])->group(function () {
  Route::namespace('Brackets\AdminAuth\Http\Controllers')->group(function () {
  Route::get('/', 'AdminHomepageController@index')->name('brackets/admin-auth::admin');
  });
  });
 */
/*
  Route::get('/', function () {
  return redirect(('/admin/login'));
  });
 */

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/admin-users', 'Admin\AdminUsersController@index');
    Route::get('/admin/admin-users/create', 'Admin\AdminUsersController@create');
    Route::post('/admin/admin-users', 'Admin\AdminUsersController@store');
    Route::get('/admin/admin-users/{adminUser}/edit', 'Admin\AdminUsersController@edit')->name('admin/admin-users/edit');
    Route::post('/admin/admin-users/{adminUser}', 'Admin\AdminUsersController@update')->name('admin/admin-users/update');
    Route::delete('/admin/admin-users/{adminUser}', 'Admin\AdminUsersController@destroy')->name('admin/admin-users/destroy');
    Route::get('/admin/admin-users/{adminUser}/resend-activation', 'Admin\AdminUsersController@resendActivationEmail')->name('admin/admin-users/resendActivationEmail');
});

/* Auto-generated profile routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/profile', 'Admin\ProfileController@editProfile');
    Route::post('/admin/profile', 'Admin\ProfileController@updateProfile');
    Route::get('/admin/password', 'Admin\ProfileController@editPassword');
    Route::post('/admin/password', 'Admin\ProfileController@updatePassword');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/managers', 'Admin\UsersController@index');
    Route::get('/admin/managers/create', 'Admin\UsersController@create');
    Route::post('/admin/managers', 'Admin\UsersController@store');
    Route::get('/admin/managers/{user}/edit', 'Admin\UsersController@edit')->name('admin/users/edit');
    Route::post('/admin/managers/{user}', 'Admin\UsersController@update')->name('admin/users/update');
    Route::delete('/admin/managers/{user}', 'Admin\UsersController@destroy')->name('admin/users/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/manage_clients', 'Admin\ClientsController@index');
    Route::get('/admin/manage_clients/create', 'Admin\ClientsController@create');
    Route::post('/admin/manage_clients', 'Admin\ClientsController@store');
    Route::get('/admin/manage_clients/{client}/edit', 'Admin\ClientsController@edit')->name('admin/clients/edit');
    Route::post('/admin/manage_clients/{client}', 'Admin\ClientsController@update')->name('admin/clients/update');
    Route::delete('/admin/manage_clients/{client}', 'Admin\ClientsController@destroy')->name('admin/clients/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/user-levels', 'Admin\UserLevelsController@index');
    Route::get('/admin/user-levels/create', 'Admin\UserLevelsController@create');
    Route::post('/admin/user-levels', 'Admin\UserLevelsController@store');
    Route::get('/admin/user-levels/{userLevel}/edit', 'Admin\UserLevelsController@edit')->name('admin/user-levels/edit');
    Route::post('/admin/user-levels/{userLevel}', 'Admin\UserLevelsController@update')->name('admin/user-levels/update');
    Route::delete('/admin/user-levels/{userLevel}', 'Admin\UserLevelsController@destroy')->name('admin/user-levels/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/payment-methods', 'Admin\PaymentMethodsController@index');
    Route::get('/admin/payment-methods/create', 'Admin\PaymentMethodsController@create');
    Route::post('/admin/payment-methods', 'Admin\PaymentMethodsController@store');
    Route::get('/admin/payment-methods/{paymentMethod}/edit', 'Admin\PaymentMethodsController@edit')->name('admin/payment-methods/edit');
    Route::post('/admin/payment-methods/{paymentMethod}', 'Admin\PaymentMethodsController@update')->name('admin/payment-methods/update');
    //Route::delete('/admin/payment-methods/{paymentMethod}', 'Admin\PaymentMethodsController@destroy')->name('admin/payment-methods/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/rewards', 'Admin\RewardsController@index');
    Route::get('/admin/rewards/create', 'Admin\RewardsController@create');
    Route::post('/admin/rewards', 'Admin\RewardsController@store');
    Route::get('/admin/rewards/{reward}/edit', 'Admin\RewardsController@edit')->name('admin/rewards/edit');
    Route::post('/admin/rewards/{reward}', 'Admin\RewardsController@update')->name('admin/rewards/update');
    Route::delete('/admin/rewards/{reward}', 'Admin\RewardsController@destroy')->name('admin/rewards/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/penalties', 'Admin\PenaltiesController@index');
    Route::get('/admin/penalties/create', 'Admin\PenaltiesController@create');
    Route::post('/admin/penalties', 'Admin\PenaltiesController@store');
    Route::get('/admin/penalties/{penalty}/edit', 'Admin\PenaltiesController@edit')->name('admin/penalties/edit');
    Route::post('/admin/penalties/{penalty}', 'Admin\PenaltiesController@update')->name('admin/penalties/update');
    Route::delete('/admin/penalties/{penalty}', 'Admin\PenaltiesController@destroy')->name('admin/penalties/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/groups', 'Admin\GroupsController@index');
    Route::get('/admin/groups/create', 'Admin\GroupsController@create');
    Route::post('/admin/groups', 'Admin\GroupsController@store');
    Route::get('/admin/groups/{group}/edit', 'Admin\GroupsController@edit')->name('admin/groups/edit');
    Route::post('/admin/groups/{group}', 'Admin\GroupsController@update')->name('admin/groups/update');
    Route::delete('/admin/groups/{group}', 'Admin\GroupsController@destroy')->name('admin/groups/destroy');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/questions', 'Admin\QuestionsController@index');
    Route::get('/admin/questions/create', 'Admin\QuestionsController@create');
    Route::post('/admin/questions', 'Admin\QuestionsController@store');
    Route::get('/admin/questions/{question}/edit', 'Admin\QuestionsController@edit')->name('admin/questions/edit');
    Route::post('/admin/questions/{question}', 'Admin\QuestionsController@update')->name('admin/questions/update');
    Route::delete('/admin/questions/{question}', 'Admin\QuestionsController@destroy')->name('admin/questions/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/question-types', 'Admin\QuestionTypesController@index');
    Route::get('/admin/question-types/create', 'Admin\QuestionTypesController@create');
    Route::post('/admin/question-types', 'Admin\QuestionTypesController@store');
    Route::get('/admin/question-types/{questionType}/edit', 'Admin\QuestionTypesController@edit')->name('admin/question-types/edit');
    Route::post('/admin/question-types/{questionType}', 'Admin\QuestionTypesController@update')->name('admin/question-types/update');
    Route::delete('/admin/question-types/{questionType}', 'Admin\QuestionTypesController@destroy')->name('admin/question-types/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/submitted-exams', 'Admin\QuestionsSubmissionsController@index');
    //Route::get('/admin/submitted-exams/create',           'Admin\QuestionsSubmissionsController@create');
    //Route::post('/admin/submitted-exams',                 'Admin\QuestionsSubmissionsController@store');
    Route::get('/admin/submitted-exams/{user}/edit', 'Admin\QuestionsSubmissionsController@edit')->name('admin/submitted-exams/edit');
    Route::post('/admin/submitted-exams/{user}', 'Admin\QuestionsSubmissionsController@update')->name('admin/submitted-exams/update');
    Route::delete('/admin/submitted-exams/{questionsSubmission}', 'Admin\QuestionsSubmissionsController@destroy')->name('admin/submitted-exams/destroy');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/registration-request', 'Admin\RegistrationRequestController@index');
    #Route::get('/admin/registration-request/create',                           'Admin\RegistrationRequestController@create');
    #Route::post('/admin/registration-request',                                 'Admin\RegistrationRequestController@store');
    Route::get('/admin/registration-request/{user}/edit', 'Admin\RegistrationRequestController@edit')->name('admin/registration-request/edit');
    Route::post('/admin/registration-request/{user}', 'Admin\RegistrationRequestController@update')->name('admin/registration-request/update');
    #Route::delete('/admin/registration-request/{user}',                        'Admin\RegistrationRequestController@destroy')->name('admin/registration-request/destroy');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/clients/writers', 'Admin\ClientWritersController@index');
    //Route::get('/admin/writers/create',                         'Admin\ClientWritersController@create');
    //Route::post('/admin/writers',                               'Admin\ClientWritersController@store');
    Route::get('/admin/clients/writers/{user}/edit', 'Admin\ClientWritersController@edit')->name('admin/writers/edit');
    Route::post('/admin/clients/writers/{user}', 'Admin\ClientWritersController@update')->name('admin/writers/update');
    //Route::delete('/admin/writers/{writer}',                    'Admin\ClientWritersController@destroy')->name('admin/writers/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/clients/proofreaders', 'Admin\ProofreaderController@index');
    //Route::get('/admin/clients/proofreaders/create',                    'Admin\ProofreaderController@create');
    //Route::post('/admin/clients/proofreaders',                          'Admin\ProofreaderController@store');
    Route::get('/admin/clients/proofreaders/{user}/edit', 'Admin\ProofreaderController@edit')->name('admin/proofreaders/edit');
    Route::post('/admin/clients/proofreaders/{user}', 'Admin\ProofreaderController@update')->name('admin/proofreaders/update');
    //Route::delete('/admin/proofreaders/{proofreader}',          'Admin\ProofreaderController@destroy')->name('admin/proofreaders/destroy');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/quality-assurance', 'Admin\QAController@index');
    Route::get('/admin/quality-assurance/create', 'Admin\QAController@create');
    Route::post('/admin/quality-assurance', 'Admin\QAController@store');
    Route::get('/admin/quality-assurance/{user}/edit', 'Admin\QAController@edit')->name('admin/quality-assurance/edit');
    Route::post('/admin/quality-assurance/{user}', 'Admin\QAController@update')->name('admin/quality-assurance/update');
    Route::delete('/admin/quality-assurance/{user}', 'Admin\QAController@destroy')->name('admin/quality-assurance/destroy');
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/system/writers', 'Admin\SystemWritersController@index');
    // Route::get('/admin/system/writers/create', 'Admin\SystemWritersController@create');
    //Route::post('/admin/system/writers', 'Admin\SystemWritersController@store');
    Route::get('/admin/system/writers/{user}/edit', 'Admin\SystemWritersController@edit')->name('admin/system-writers/edit');
    Route::post('/admin/system/writers/{user}', 'Admin\SystemWritersController@update')->name('admin/system-writers/update');
    //Route::delete('/admin/system/writers/{user}', 'Admin\SystemWritersController@destroy')->name('admin/system-writers/destroy');
});


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/admin/system/proofreaders', 'Admin\SystemProofreadersController@index');
    //Route::get('/admin/system/proofreaders/create',             'Admin\SystemProofreadersController@create');
    //Route::post('/admin/system/proofreaders',                   'Admin\SystemProofreadersController@store');
    Route::get('/admin/system/proofreaders/{user}/edit', 'Admin\SystemProofreadersController@edit')->name('admin/system-proofreaders/edit');
    Route::post('/admin/system/proofreaders/{user}', 'Admin\SystemProofreadersController@update')->name('admin/system-proofreaders/update');
    //Route::delete('/admin/system/proofreaders/{user}','Admin\SystemProofreadersController@destroy')->name('admin/system-proofreaders/destroy');
});
