<?php

use Illuminate\Support\Facades\Route;
use App\Constants\UserType;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
App::setLocale('ar');
Route::group(['prefix' => 'v1'], function () {
    // login
    Route::post('/users/login', 'API\UserController@login');
    // countries
    Route::get('/countries', 'API\CountryController@index');
    Route::get('/groups', 'API\GroupController@getGroups');
    Route::get('/majors', 'API\MajorController@getMajors');
    // Registration
    Route::post('/registration', 'API\UserController@registration');

    Route::group(['prefix' => '/verification'], function() {
        Route::put('/', 'API\UserTokenController@verification');
        #Route::post('/proofreader');
    });


    // Must Be Login
    Route::group(['middleware' => 'auth:api'], function() {
        // Get user levels by usertype
        Route::get('/user-levels/{userType}', 'API\UserLevelController@getUserLevelsByUserType');
        // Get Groups
        // Get APIs-links
        Route::get('/apis-links', 'API\ClientController@getAPILinks');
        // article-types
        Route::get('/article-types', 'API\ArticleTypeController@getArticleTypes');
        // References
        Route::get('/references/subgorup/{subgroupId}', 'API\ReferenceController@getReferencesBySubgroupId');
        // ReferencingType
        Route::get('/ReferencingType', 'API\ReferencingTypeController@getReferencingType');
        // Payment Methods
        Route::get('/payment-methods', 'API\PaymentMethodController@getPaymentMethods');
        // Major
        // SubGroups
        Route::group(['prefix' => '/subgroups'], function () {
            Route::get('/', 'API\SubgroupController@getClientSubgroups');
            Route::get('/{subgroupId}', 'API\SubgroupController@getClientSubgroup');
            Route::post('/', 'API\SubgroupController@createClientSubgroups');
            Route::put('/{subgroupId}', 'API\SubgroupController@updateClientSubgroups');
        });
        // Panalties
        Route::group(['prefix' => '/penalties'], function () {
            Route::get('/{userType}', 'API\PenaltyController@getSystemPenalties');
            Route::post('/', 'API\PenaltyController@setClientPenalties');
        });
        // article levels
        Route::group(['prefix' => '/article-levels'], function () {
            Route::group(['middleware' => 'role:' . UserType::WEBSITE_MANAGER . ',' . UserType::WEBSITE_COORDINATOR], function () {
                Route::get('/{articleLevelId}', 'API\ArticleLevelController@getArticleLevel');
                Route::get('/', 'API\ArticleLevelController@getArticleLevels');
            });
            Route::group(['middleware' => 'role:' . UserType::WEBSITE_MANAGER], function () {
                Route::post('/', 'API\ArticleLevelController@createArticleLevel');
                Route::put('/{articleLevelId}', 'API\ArticleLevelController@updateArticleLevel');
            });
        });
        // Article
        Route::group(['prefix' => '/article', 'middleware' => 'role:' . UserType::WEBSITE_MANAGER . ',' . UserType::WEBSITE_COORDINATOR], function() {
            // Create multiple Article
            Route::post('/multiple', 'API\ArticleController@createArticles');
            // Create Single Article
            Route::post('/', 'API\ArticleController@createArticle');
            // article Approved
            Route::put('/approve', 'API\ArticleController@approvedArticles');
            // Update Single Article
            Route::put('/{articleId}', 'API\ArticleController@updateArticle');
            // get Article 
            Route::get('/{articleId}', 'API\ArticleController@getArticle');
            // get Articles
            Route::get('/', 'API\ArticleController@getArticles');
            // Delete Article By Id
            Route::delete('/{articleId}', 'API\ArticleController@deleteArticle');
        });
        // Users
        Route::group(['prefix' => '/users'], function() {
            // logged in user
            Route::group(['prefix' => '/profile'], function() {
                // get login user data
                Route::get('/', 'API\UserController@getCurrentUserProfile');
                // update login user data
                Route::put('/', 'API\UserController@updateCurrentUserProfile');
            });
            // Coordinators
            Route::group(['prefix' => '/coordinators', 'middleware' => 'role:' . UserType::WEBSITE_MANAGER], function() {
                Route::get('/', 'API\UserController@getCoordinators');
                Route::post('/', 'API\UserController@createCoordinator');
                Route::get('/{userId}', 'API\UserController@getClientCoordinatorInfo');
                Route::put('/{userId}', 'API\UserController@updateClientCoordinator');
            });
            // Writers
            Route::group(['prefix' => '/writers', 'middleware' => 'role:' . UserType::WEBSITE_MANAGER . ',' . UserType::WEBSITE_COORDINATOR], function() {
                // Update Keep it `POST` for uploading files
                Route::post('/{userId}', 'API\UserController@updateClientManpower');
                // Create
                Route::post('/', 'API\UserController@createClientManpower');
                // Get Profile
                Route::get('/{userId}', 'API\UserController@getWriter');
                // Get All & Filters
                Route::get('/', 'API\UserController@getWriters');
            });
            // Proofreaders
            Route::group(['prefix' => '/proofreaders', 'middleware' => 'role:' . UserType::WEBSITE_MANAGER . ',' . UserType::WEBSITE_COORDINATOR], function() {
                // Update Keep it `POST` for uploading files
                Route::post('/{userId}', 'API\UserController@updateClientManpower');
                // Create
                Route::post('/', 'API\UserController@createClientManpower');
                // Get All & Filters
                Route::get('/', 'API\UserController@getProofreaders');
                // Get All General Proofreaders
                Route::get('/general', 'API\UserController@getGeneralProofreaders');
                // Get All Language Proofreaders
                Route::get('/language', 'API\UserController@getLanguageProofreaders');
                // Get All Content Proofreaders
                Route::get('/content', 'API\UserController@getContentProofreaders');
                // Get Profile // Keep it in last   
                Route::get('/{userId}', 'API\UserController@getProofreader');
            });
        });
        // Registration Steps
        Route::group(['prefix' => '/registration'], function() {
            // Check what the Role must be see this pages
            Route::group(['prefix' => '/step'], function() {
                // Major Info
                Route::post('/2', 'API\UserController@registrationStepTwo');
                // Exam
                Route::get('/3', 'API\UserController@getExam');
                Route::post('/3', 'API\UserController@saveExam');
                // Upload Images
                Route::post('/4', 'API\UserController@registrationStepFour');
            });
        });
    }); // End MiddleWare
});
