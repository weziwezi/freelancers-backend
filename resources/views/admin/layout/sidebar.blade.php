<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @switch(Auth::user()->roles[0]->id)

            {{-- Super Admin --}}
            @case (1)
            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            <li class="nav-item  nav-dropdown  ">
                <a class="nav-link  nav-dropdown-toggle " href="#"><i class="nav-icon icon-people"></i> {{ trans('Clients') }}</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/manage_clients') }}"><i class="nav-icon cui-globe"></i> {{ trans('admin.client.title') }}</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/managers') }}"><i class="nav-icon cui-people"></i> {{ trans('admin.user.title') }}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ url('admin/quality-assurance') }}"><i class="nav-icon cui-people"></i> {{ trans('admin.qa.title') }}</a></li>
                </ul>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/groups') }}"><i class="nav-icon fa fa-users"></i> {{ trans('admin.group.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/user-levels') }}"><i class="nav-icon icon-chart"></i> {{ trans('admin.user-level.title') }} {!! (UserLevelWidget::getCountUserLevelsNotHasDefaultValue() ? '<span class="badge badge-danger" title="'.trans('There are some users that haven\'t default value').'">!</span>' : '') !!}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/rewards') }}"><i class="nav-icon icon-badge"></i> {{ trans('admin.reward.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/penalties') }}"><i class="nav-icon fa fa-legal"></i> {{ trans('admin.penalty.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/payment-methods') }}"><i class="nav-icon icon-credit-card"></i> {{ trans('admin.payment-method.title') }}</a></li>



            <li class="nav-item  nav-dropdown  ">
                <a class="nav-link  nav-dropdown-toggle " href="#"><i class="nav-icon icon-settings"></i> {{ trans('brackets/admin-ui::admin.sidebar.settings') }}</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/admin-users') }}"><i class="nav-icon icon-user"></i> {{ __('Manage access') }}</a>
                    </li>

                </ul>
            </li>

            {{-- <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i class="nav-icon fa fa-language"></i> {{ __('Translations') }}</a></li>--}}

            {{--<li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li>--}}


            @break

            {{-- Admin --}}
            @case (2)
            <li class="nav-title">{{ trans('Dashboard') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/question-types') }}"><i class="nav-icon fa fa-question-circle-o"></i> {{ trans('admin.question-type.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/questions') }}"><i class="nav-icon fa fa-question-circle"></i> {{ trans('admin.question.title') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/submitted-exams') }}"><i class="nav-icon cui-task"></i> {{ trans('admin.questions-submission.title') }}<span class="badge badge-light">{{ QuestionsSubmissionWidget::getCountPendingQuestionSubmission() }}</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/registration-request') }}"><i class="nav-icon icon-note"></i> {{ trans('admin.registration-request.title') }}<span class="badge badge-light">{{ RegistrationRequestWidget::getCountPendingRegistrationRequest() }}</span></a></li>
            <li class="nav-item  nav-dropdown  ">
                <a class="nav-link  nav-dropdown-toggle " href="#"><i class="nav-icon icon-people"></i> {{ trans('Clients Manpower') }}</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/clients/writers') }}"><i class="nav-icon icon-pencil"></i> {{ trans('admin.registration-request.writers') }}</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/clients/proofreaders') }}"><i class="nav-icon icon-eyeglass"></i> {{ trans('admin.registration-request.proofreaders') }}</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle " href="#"><i class="nav-icon icon-people"></i> {{ trans('System Manpower') }}</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/system/writers') }}"><i class="nav-icon icon-pencil"></i> {{ trans('admin.registration-request.writers') }}</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('admin/system/proofreaders') }}"><i class="nav-icon icon-eyeglass"></i> {{ trans('admin.registration-request.proofreaders') }}</a>
                    </li>
                </ul>
            </li>
            @break

            {{-- Finance --}}
            @case (3)

            @break

            @endswitch
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>


<div class="modal fade bd-example-modal-xl" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered bg-transparent" role="document">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="font-size: 50px;">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <img src="" class="w-50 h-auto mx-auto" />
            </div>
        </div>
    </div>
</div>
