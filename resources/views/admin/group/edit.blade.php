@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.group.actions.edit', ['name' => $group->name]))

@section('body')

    <div class="container-xl">

        <div class="card">

            <group-form
                :action="'{{ $group->resource_url }}'"
                :data="{{ $group->toJson() }}"
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.group.actions.edit', ['name' => $group->name]) }}
                    </div>

                    <div class="card-body">

                        @include('admin.group.components.form-elements')

                    </div>

                    <div class="card-footer">
	                    <button type="submit" class="btn btn-primary" :disabled="submiting">
		                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
		                    {{ trans('brackets/admin-ui::admin.btn.save') }}
	                    </button>
                    </div>

                </form>

        </group-form>

    </div>

</div>

@endsection