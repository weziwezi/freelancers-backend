<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
     <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user-level.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.user-level.columns.name') }}">
           <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('minimum_points'), 'has-success': this.fields.minimum_points && this.fields.minimum_points.valid }">
     <label for="minimum_points" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user-level.columns.minimum_points') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.minimum_points" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('minimum_points'), 'form-control-success': this.fields.minimum_points && this.fields.minimum_points.valid}" id="minimum_points" name="minimum_points" placeholder="{{ trans('admin.user-level.columns.minimum_points') }}">
           <div v-if="errors.has('minimum_points')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('minimum_points') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('bonus_factor'), 'has-success': this.fields.bonus_factor && this.fields.bonus_factor.valid }">
     <label for="bonus_factor" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user-level.columns.bonus_factor') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.bonus_factor" v-validate="'required|decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('bonus_factor'), 'form-control-success': this.fields.bonus_factor && this.fields.bonus_factor.valid}" id="bonus_factor" name="bonus_factor" placeholder="{{ trans('admin.user-level.columns.bonus_factor') }}">
           <div v-if="errors.has('bonus_factor')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('bonus_factor') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('maximum_number_of_reservations'), 'has-success': this.fields.maximum_number_of_reservations && this.fields.maximum_number_of_reservations.valid }">
     <label for="maximum_number_of_reservations" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user-level.columns.maximum_number_of_reservations') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.maximum_number_of_reservations" v-validate="'required|decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('maximum_number_of_reservations'), 'form-control-success': this.fields.maximum_number_of_reservations && this.fields.maximum_number_of_reservations.valid}" id="maximum_number_of_reservations" name="maximum_number_of_reservations" placeholder="{{ trans('admin.user-level.columns.maximum_number_of_reservations') }}">
           <div v-if="errors.has('maximum_number_of_reservations')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('maximum_number_of_reservations') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('user_type'), 'has-success': this.fields.user_type && this.fields.user_type.valid }">
     <label for="user_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user-level.columns.user_type') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="user_type" :multiple="false" v-model="form.user_type" name="user_type" :custom-label="opt => {{ json_encode($userTypes) }}.find(x => x.id == opt).name" :options="{{ json_encode($userTypes) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('user_type'), 'form-control-success': this.fields.user_type && this.fields.user_type.valid}" ></multiselect>
        <div v-if="errors.has('user_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('user_type') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('is_default'), 'has-success': this.fields.is_default && this.fields.is_default.valid }">
     <label for="is_default" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.group.columns.is_default') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <label class="switch switch-label switch-pill switch-success">
            <input autocomplete="off" class="switch-input" id="is_default" type="checkbox" v-model="form.is_default" v-validate="''" data-vv-name="is_default"  name="is_default_fake_element">
            <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
            <input autocomplete="off" type="hidden" name="is_default" :value="form.is_default">
        </label>
        <div v-if="errors.has('is_default')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('is_default') }}</div>
    </div>
</div>