@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user-level.actions.create'))

@section('body')

    <div class="container-xl">

        <div class="card">

            <user-level-form
                :action="'{{ url('admin/user-levels') }}'"
                
                inline-template>

                <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('admin.user-level.actions.create') }}
                    </div>

                    <div class="card-body">

                        @include('admin.user-level.components.form-elements')

                    </div>

                    <div class="card-footer">
	                    <button type="submit" class="btn btn-primary" :disabled="submiting">
		                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
		                    {{ trans('brackets/admin-ui::admin.btn.save') }}
	                    </button>
                    </div>

                </form>

            </user-level-form>

        </div>

    </div>

@endsection