@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.proofreader.actions.create'))

@section('body')

    <div class="container-xl">

        <div class="card">

            <clients-proofreaders-form
                :action="'{{ url('admin/proofreaders') }}'"
                
                inline-template>

                <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('admin.clients-proofreaders.actions.create') }}
                    </div>

                    <div class="card-body">

                        @include('admin.clients-proofreaders.components.form-elements')

                    </div>

                    <div class="card-footer">
	                    <button type="submit" class="btn btn-primary" :disabled="submiting">
		                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
		                    {{ trans('brackets/admin-ui::admin.btn.save') }}
	                    </button>
                    </div>

                </form>

            </clients-proofreaders-form>

        </div>

    </div>

@endsection