@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.clients-proofreaders.actions.index'))

@section('body')

<clients-proofreaders-listing
    :data="{{ $data->toJson() }}"
    :url="'{{ url('admin/clients/proofreaders') }}'"
    inline-template>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.clients-proofreaders.actions.index') }}
                </div>
                <div class="card-body" v-cloak>
                    <form @submit.prevent="">
                        <div class="row">
                            <div class="col col-xl-3 form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col col-xl-3 form-group">
                                <div class="input-group">
                                    <select v-model="client_id" class="form-control" id="client_id" name="client_id" @change="filter('client_id', client_id);">
                                        <option value="">{{ trans('admin.clients-proofreaders.select.option.all.label') }}</option>
                                        @foreach ($clients as $client)
                                        <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-auto ml-auto form-group ">
                                <select class="form-control" v-model="pagination.state.per_page">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>

                        </div>
                    </form>

                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th is='sortable' :column="'id'">{{ trans('admin.clients-proofreaders.columns.id') }}</th>
                                <th is='sortable' :column="'first_name'">{{ trans('admin.clients-proofreaders.columns.name') }}</th>
                                <th is='sortable' :column="'client_id'">{{ trans('admin.clients-proofreaders.columns.employer') }}</th>
                                <th is='sortable' :column="'username'">{{ trans('admin.clients-proofreaders.columns.username') }}</th>
                                <th is='sortable' :column="'is_active'">{{ trans('admin.clients-proofreaders.columns.is_active') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in collection">
                                <td>@{{ item.id }}</td>
                                <td>@{{ item.first_name +' '+ item.last_name }}</td>
                                <td>@{{ item.client.name }}</td>
                                <td>@{{ item.username }}</td>
                                <td>
                                    <label class="switch switch-3d switch-success">
                                        <input type="checkbox" class="switch-input" v-model="collection[index].user_status" @change="toggleSwitch(item.clients_proofreaders_url, 'user_status', collection[index])">
                                        <span class="switch-slider"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="row no-gutters">
                                        <div class="col-auto">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.clients_proofreaders_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" v-if="pagination.state.total > 0">
                        <div class="col-sm">
                            <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                        </div>
                        <div class="col-sm-auto">
                            <pagination></pagination>
                        </div>
                    </div>

                    <div class="no-items-found" v-if="!collection.length > 0">
                        <i class="icon-magnifier"></i>
                        <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                        <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</clients-proofreaders-listing>

@endsection