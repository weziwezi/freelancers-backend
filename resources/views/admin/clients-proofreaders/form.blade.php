@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.proofreader.actions.edit', ['name' => $proofreader->id]))

@section('body')

    <div class="container-xl">

        <div class="card">

            <clients-proofreaders-form
            :action="'{{ route('admin/clients/proofreaders/update', ['proofreader' => $proofreader]) }}'"
            :data="{{ $proofreader->toJson() }}"
            inline-template>
            
                <form class="form-horizontal" method="post" @submit.prevent="onSubmit" :action="this.action">

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('admin.clients-proofreaders.actions.edit', ['name' => $proofreader->id]) }}
                    </div>

                    <div class="card-body">

                        
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </clients-proofreaders-form>

        </div>

    </div>

@endsection