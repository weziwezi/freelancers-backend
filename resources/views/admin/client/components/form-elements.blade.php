<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
     <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.client.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.client.columns.name') }}">
           <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('website_url'), 'has-success': this.fields.website_url && this.fields.website_url.valid }">
     <label for="website_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.client.columns.website_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.website_url" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('website_url'), 'form-control-success': this.fields.website_url && this.fields.website_url.valid}" id="website_url" name="website_url" placeholder="{{ trans('admin.client.columns.website_url') }}">
           <div v-if="errors.has('website_url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('website_url') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('post_article_api_url'), 'has-success': this.fields.post_article_api_url && this.fields.post_article_api_url.valid }">
     <label for="post_article_api_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.client.columns.post_article_api_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.post_article_api_url" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('post_article_api_url'), 'form-control-success': this.fields.post_article_api_url && this.fields.post_article_api_url.valid}" id="post_article_api_url" name="post_article_api_url" placeholder="{{ trans('admin.client.columns.post_article_api_url') }}">
           <div v-if="errors.has('post_article_api_url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('post_article_api_url') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('get_main_categories_api_url'), 'has-success': this.fields.get_main_categories_api_url && this.fields.get_main_categories_api_url.valid }">
     <label for="get_main_categories_api_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.client.columns.get_main_categories_api_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.get_main_categories_api_url" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('get_main_categories_api_url'), 'form-control-success': this.fields.get_main_categories_api_url && this.fields.get_main_categories_api_url.valid}" id="get_main_categories_api_url" name="get_main_categories_api_url" placeholder="{{ trans('admin.client.columns.get_main_categories_api_url') }}">
           <div v-if="errors.has('get_main_categories_api_url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('get_main_categories_api_url') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('get_sub_categories_api_url'), 'has-success': this.fields.get_sub_categories_api_url && this.fields.get_sub_categories_api_url.valid }">
     <label for="get_sub_categories_api_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.client.columns.get_sub_categories_api_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.get_sub_categories_api_url" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('get_sub_categories_api_url'), 'form-control-success': this.fields.get_sub_categories_api_url && this.fields.get_sub_categories_api_url.valid}" id="get_sub_categories_api_url" name="get_sub_categories_api_url" placeholder="{{ trans('admin.client.columns.get_sub_categories_api_url') }}">
           <div v-if="errors.has('get_sub_categories_api_url')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('get_sub_categories_api_url') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('is_active'), 'has-success': this.fields.is_active && this.fields.is_active.valid }">
     <label for="is_active" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.employee.columns.is_active') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <label class="switch switch-label switch-pill switch-success">
            <input autocomplete="off" class="switch-input" id="is_active" type="checkbox" v-model="form.is_active" v-validate="''" data-vv-name="is_active"  name="is_active_fake_element">
            <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
            <input autocomplete="off" type="hidden" name="is_active" :value="form.is_active">
        </label>
        <div v-if="errors.has('is_active')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('is_active') }}</div>
    </div>
</div>



