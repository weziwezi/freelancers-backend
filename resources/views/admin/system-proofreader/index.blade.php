@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.system-proofreader.actions.index'))

@section('body')

<system-proofreader-listing
    :data="{{ $data->toJson() }}"
    :url="'{{ url('admin/system/proofreaders') }}'"
    inline-template>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.system-proofreader.actions.index') }}
                </div>
                <div class="card-body" v-cloak>
                    <form @submit.prevent="">
                        <div class="row justify-content-md-between">
                            <div class="col col-lg-7 col-xl-5 form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-auto form-group ">
                                <select class="form-control" v-model="pagination.state.per_page">

                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>

                        </div>
                    </form>

                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th is='sortable' :column="'id'">{{ trans('admin.proofreaders.columns.id') }}</th>
                                <th is='sortable' :column="'first_name'">{{ trans('admin.proofreaders.columns.name') }}</th>
                                <th is='sortable' :column="'username'">{{ trans('admin.proofreaders.columns.username') }}</th>
                                <th is='sortable' :column="'is_active'">{{ trans('admin.proofreaders.columns.is_active') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in collection">

                                <td>@{{ item.id }}</td>
                                <td>@{{ item.first_name +' '+ item.last_name }}</td>
                                <td>@{{ item.username }}</td>
                                <td>
                                    <label class="switch switch-3d switch-success">
                                        <input type="checkbox" class="switch-input" v-model="collection[index].user_status" @change="toggleSwitch(item.system_proofreaders_url, 'user_status', collection[index])">
                                        <span class="switch-slider"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="row no-gutters">
                                        <div class="col-auto">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.system_proofreaders_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" v-if="pagination.state.total > 0">
                        <div class="col-sm">
                            <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                        </div>
                        <div class="col-sm-auto">
                            <pagination></pagination>
                        </div>
                    </div>

                    <div class="no-items-found" v-if="!collection.length > 0">
                        <i class="icon-magnifier"></i>
                        <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                        <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</system-proofreader-listing>

@endsection