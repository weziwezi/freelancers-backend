@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.questions-submission.actions.index'))

@section('body')

<questions-submission-listing
    :data="{{ $data->toJson() }}"
    :url="'{{ url('admin/submitted-exams') }}'"
    inline-template>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.questions-submission.actions.index') }}
                </div>
                <div class="card-body" v-cloak>
                    <form @submit.prevent="">
                        <div class="row">
                            <div class="col col-xl-3 form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col col-xl-3 form-group">
                                <div class="input-group">
                                    <select v-model="user_type" class="form-control" id="user_type" name="user_type" @change="filter('user_type', user_type);">
                                        <option selected="selected" value="">{{ trans('admin.question-type.select.option.all.label') }}</option>
                                        @foreach ($userTypes as $userType)
                                        <option value="{{ $userType['id'] }}">{{ $userType['name'] }}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-auto ml-auto form-group ">
                                <select class="form-control" v-model="pagination.state.per_page">

                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>

                        </div>
                    </form>

                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th is='sortable' :column="'id'">{{ trans('admin.questions-submission.columns.id') }}</th>
                                <th is='sortable' :column="'user_id'">{{ trans('admin.questions-submission.columns.user_id') }}</th>
                                <th is='sortable' :column="'groups'">{{ trans('admin.questions-submission.columns.groups') }}</th>

                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in collection">
                                <td>@{{ item.id }}</td>
                                <td>@{{ item.first_name + ' ' + item.second_name + ' ' + item.third_name +' '+ item.last_name }}</td>
                                <td><h5><span v-for="(group, index2) in item.groups" class="badge badge-primary mx-1">@{{ group.name }}</span></h5></td>
                                <td>
                                    <div class="row no-gutters">
                                        <div class="col-auto">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.questions_submissions_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.view') }}" role="button"><i class="fa fa-eye"></i></a>
                                        </div>
                                        {{--<form class="col" @submit.prevent="deleteItem(item.questions_submissions_url)">
                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                        </form>--}}
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" v-if="pagination.state.total > 0">
                        <div class="col-sm">
                            <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                        </div>
                        <div class="col-sm-auto">
                            <pagination></pagination>
                        </div>
                    </div>

                    <div class="no-items-found" v-if="!collection.length > 0">
                        <i class="icon-magnifier"></i>
                        <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                        <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</questions-submission-listing>

@endsection