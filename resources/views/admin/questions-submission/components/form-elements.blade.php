<div class="accordion text-right" id="accordionExample">
    @foreach($user->questions_submissions as $questionSubmission)


    <div class="card mb-0">
        <div class="card-header bg-light p-0 m-0 collapsed" id="{{ 'header'.$questionSubmission->id }}">
            <h2 class="mb-0">
                <button class="btn btn-link w-100 text-right" type="button" data-toggle="collapse" data-target="#{{ 'collaps'.$questionSubmission->id }}" aria-expanded="false" aria-controls="{{ 'collaps'.$questionSubmission->id }}">
                    {{ $questionSubmission->question->header }}
                </button> 
            </h2>
        </div>
        <div id="{{ 'collaps'.$questionSubmission->id }}" class="collapse" aria-labelledby="{{ 'header'.$questionSubmission->id }}" data-parent="#accordionExample">
            <div class="card-body p-4">
                <h6 class="card-title">{!! $questionSubmission->question->body !!}</h6>
                <h6 class="card-text text-muted">{!! $questionSubmission->answer !!}</h6>
            </div>
        </div> 
    </div> 




    <!--div class="mt-5">
        
        {{ $user }}
        {{ $questionSubmission->question }}
    
    
        Ans
        <div class="mt-5">
            {{ $questionSubmission->answer }}
        </div>
    </div-->
    @endforeach    

</div>

