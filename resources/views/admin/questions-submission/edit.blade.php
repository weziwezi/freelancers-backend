@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.questions-submission.actions.edit', ['name' =>$user->first_name. ' '.$user->second_name. ' '.$user->third_name. ' '.$user->last_name]))

@section('body')

<div class="container-xl">

    <div class="card">

        <questions-submission-form
            :action="'{{ $user->questions_submissions_url }}'"
            :data="{{ $user->toJson() }}"
            inline-template>

            <form class="form-horizontal form-edit text-right" dir="rtl" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                <div class="card-header">
                    <i class="fa fa-eye"></i> {{ trans('admin.questions-submission.actions.edit', ['name' =>$user->first_name. ' '.$user->second_name. ' '.$user->third_name. ' '.$user->last_name]) }}
                </div>

                <div class="card-body">

                    @include('admin.questions-submission.components.form-elements')

                </div>

                <div class="card-footer">
                    <button value="approve" v-on:click="function(){form.user_status={{ $status['approve'] }};}" name="submit" type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-check'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.approved') }}
                    </button>
                    <button value="reject" v-on:click="function(){form.user_status={{ $status['reject'] }};}" name="submit" type="submit" class="btn btn-danger" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-close'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.reject') }}
                    </button>
                </div>
            </form>

        </questions-submission-form>

    </div>

</div>

@endsection