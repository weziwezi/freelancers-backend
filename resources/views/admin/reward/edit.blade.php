@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.reward.actions.edit', ['name' => $reward->name]))

@section('body')

    <div class="container-xl">

        <div class="card">

            <reward-form
                :action="'{{ $reward->resource_url }}'"
                :data="{{ $reward->toJson() }}"
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.reward.actions.edit', ['name' => $reward->name]) }}
                    </div>

                    <div class="card-body">

                        @include('admin.reward.components.form-elements')

                    </div>

                    <div class="card-footer">
	                    <button type="submit" class="btn btn-primary" :disabled="submiting">
		                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
		                    {{ trans('brackets/admin-ui::admin.btn.save') }}
	                    </button>
                    </div>

                </form>

        </reward-form>

    </div>

</div>

@endsection