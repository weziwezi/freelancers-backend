<div class="form-group row align-items-center" :class="{'has-danger': errors.has('header'), 'has-success': this.fields.header && this.fields.header.valid }">
     <label for="header" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question.columns.header') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.header" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('header'), 'form-control-success': this.fields.header && this.fields.header.valid}" id="header" name="header" placeholder="{{ trans('admin.question.columns.header') }}">
           <div v-if="errors.has('header')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('header') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('body'), 'has-success': this.fields.body && this.fields.body.valid }">
     <label for="body" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question.columns.body') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.body" v-validate="''" id="body" name="body" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('body')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('body') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('question_type_id'), 'has-success': this.fields.question_type_id && this.fields.question_type_id.valid }">
     <label for="question_type_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question.columns.question_type_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="question_type_id" :multiple="false" v-model="form.question_type_id" name="question_type_id" :custom-label="opt => {{ json_encode($question_types) }}.find(x => x.id == opt).name" :options="{{ json_encode($question_types) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('question_type_id'), 'form-control-success': this.fields.question_type_id && this.fields.question_type_id.valid}" ></multiselect>
        <div v-if="errors.has('question_type_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('question_type_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('group_id'), 'has-success': this.fields.group_id && this.fields.group_id.valid }">
     <label for="group_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question.columns.group_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="group_id" :multiple="true" v-model="form.group_id" name="group_id" :custom-label="opt => {{ json_encode($groups) }}.find(x => x.id == opt).name" :options="{{ json_encode($groups) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('group_id'), 'form-control-success': this.fields.group_id && this.fields.group_id.valid}" ></multiselect>
        <div v-if="errors.has('group_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('group_id') }}</div>
    </div>
</div>
