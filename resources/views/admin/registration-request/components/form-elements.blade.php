<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.full_name') }}</div>
    <div class="col-10">{{ $user->first_name . ' ' . $user->second_name . ' ' . $user->third_name . ' ' . $user->last_name }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.username') }}</div>
    <div class="col-10">{{ $user->username }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.email') }}</div>
    <div class="col-10">{{ $user->email }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.gender') }}</div>
    <div class="col-10">{{ $user->gender_name }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.country') }}</div>
    <div class="col-10">{{ $user->country->name }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.major') }}</div>
    <div class="col-10">{{ $user->manpower_info->major->name .' - ' .$user->manpower_info->sub_major }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.group') }}</div>
    <div class="col-10"><h5><span v-for="(group, index2) in {{ $user->groups }}" class="badge badge-primary mx-1">@{{ group.name }}</span></h5></div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.mobile_no') }}</div>
    <div class="col-10">{{ $user->mobile_number }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.skype_account') }}</div>
    <div class="col-10">{{ $user->manpower_info->skype_account_id }}</div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.uni_certificate') }}</div>
    <div class="col-10">
        @if(empty($user->manpower_info->certificate_url))
        N/A
        @elseif(strtolower(pathinfo( $user->manpower_info->certificate_url, PATHINFO_EXTENSION)) == 'pdf')
        <a href="{{ $user->manpower_info->certificate_url }}" target="_blank">{{ $user->manpower_info->certificate_url }}</a>
        @elseif(exif_imagetype($user->manpower_info->certificate_url))
        <img src="{{ $user->manpower_info->certificate_url }}" class="img-thumbnail img-100" data-toggle="modal" data-target="#imageModal" />
        @else
        N/A
        @endif
    </div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.national_id') }}</div>
    <div class="col-10">
        @if(empty($user->manpower_info->national_id_url))
         N/A
        @elseif(strtolower(pathinfo( $user->manpower_info->national_id_url, PATHINFO_EXTENSION)) == 'pdf')
        <a href="{{ $user->manpower_info->national_id_url }}" target="_blank">{{ $user->manpower_info->national_id_url }}</a>
        @elseif(exif_imagetype($user->manpower_info->national_id_url))
        <img src="{{ $user->manpower_info->national_id_url }}" class="img-thumbnail img-100" data-toggle="modal" data-target="#imageModal" />
        @else
        N/A
        @endif
    </div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.reject_resone') }}</div>
    <div class="col-10">
        <div class="row">
            <div class="col-9">
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('rejection_reason'), 'has-success': this.fields.rejection_reason && this.fields.rejection_reason.valid }">
                     <div class="col-12">
                        <multiselect id="rejection_reason" :multiple="true" v-model="form.rejection_reason" name="rejection_reason" :custom-label="opt => {{ json_encode($rejectResons) }}.find(x => x.id == opt).name" :options="{{ json_encode($rejectResons) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('rejection_reason'), 'form-control-success': this.fields.rejection_reason && this.fields.rejection_reason.valid}" ></multiselect>
                        <div v-if="errors.has('rejection_reason')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('rejection_reason') }}</div>
                    </div>
                </div>

            </div>
            <div class="col-3">
                <button value="reject" v-on:click="changeUserStatus({{ $status['rejectWithReasons'] }})" name="submit" type="submit" class="btn btn-danger" :disabled="submiting">
                    <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-close'"></i>
                    {{ trans('registration-request.columns.reject_with_reason') }}
                </button>
            </div>
        </div>
    </div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.final_reject') }}</div>
    <div class="col-10">
        <button value="reject" v-on:click="changeUserStatus({{ $status['reject'] }})" name="submit" type="submit" class="btn btn-danger" :disabled="submiting">
            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-close'"></i>
            {{ trans('registration-request.columns.reject') }}
        </button>
    </div>
</div>
<div class="mb-2 row">
    <div class="col-2">{{ trans('registration-request.columns.approval') }}</div>
    <div class="col-10">
        <button value="approve" v-on:click="changeUserStatus({{ $status['approve'] }})" name="submit" type="submit" class="btn btn-primary" :disabled="submiting">
            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-check'"></i>
            {{ trans('registration-request.columns.approve') }}
        </button>
    </div>
</div>
