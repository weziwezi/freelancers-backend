@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.registration-request.actions.edit'))

@section('body')

<div class="container-xl">

    <div class="card">

        <registration-request-form
            :action="'{{ $user->registration_request_url }}'"
            :data="{{ json_encode((object)['rejection_reason' => '','user_status' => '']) }}"
            inline-template>

            <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                <div class="card-header">
                    <i class="fa fa-eye"></i> {{ trans('admin.registration-request.actions.edit', ['name' => $user->first_name]) }}
                </div>

                <div class="card-body">

                    @include('admin.registration-request.components.form-elements')

                </div>

                <?php
                /*
                  <div class="card-footer">
                  <button value="approve" v-on:click="function(){form.user_status={{ $status['approve'] }};}" name="submit" type="submit" class="btn btn-primary" :disabled="submiting">
                  <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-check'"></i>
                  {{ trans('brackets/admin-ui::admin.btn.approved') }}
                  </button>
                  <button value="reject" v-on:click="function(){form.user_status={{ $status['reject'] }};}" name="submit" type="submit" class="btn btn-danger" :disabled="submiting">
                  <i class="fa" :class="submiting ? 'fa-spinner' : 'fa fa-close'"></i>
                  {{ trans('brackets/admin-ui::admin.btn.reject') }}
                  </button>
                  </div>
                 * 
                 */
                ?>
            </form>

        </registration-request-form>

    </div>

</div>

@endsection