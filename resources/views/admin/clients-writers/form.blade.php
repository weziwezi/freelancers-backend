@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.writer.actions.edit', ['name' => $writer->id]))

@section('body')

    <div class="container-xl">

        <div class="card">

            <clients-writers-form
            :action="'{{ route('admin/writers/update', ['writer' => $writer]) }}'"
            :data="{{ $writer->toJson() }}"
            inline-template>
            
                <form class="form-horizontal" method="post" @submit.prevent="onSubmit" :action="this.action">

                    <div class="card-header">
                        <i class="fa fa-plus"></i> {{ trans('admin.writer.actions.edit', ['name' => $writer->id]) }}
                    </div>

                    <div class="card-body">

                        
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>

            </clients-writers-form>

        </div>

    </div>

@endsection