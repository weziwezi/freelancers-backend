@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.writer.actions.edit', ['name' => $user->id]))

@section('body')

<div class="container-xl">
    <div class="card">
        <clients-writers-form
            :action="'{{ $user->clients_writers_url }}'"
            :data="{{ $user->toJson() }}"
            inline-template>

            <form class="form-horizontal form-edit" enctype="multipart/form-data" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>

                <div class="card-header">
                    <i class="fa fa-pencil"></i> {{ trans('admin.clients-writers.actions.edit', ['name' => $user->first_name.' '.$user->last_name]) }}
                </div>

                <div class="card-body">

                    @include('admin.clients-writers.components.form-elements')

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" :disabled="submiting">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>

            </form>

        </clients-writers-form>

    </div>

</div>

@endsection