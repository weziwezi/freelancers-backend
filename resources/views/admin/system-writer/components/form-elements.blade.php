<div class="form-group row align-items-center" :class="{'has-danger': errors.has('first_name'), 'has-success': this.fields.first_name && this.fields.first_name.valid }">
     <label for="first_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.first_name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.first_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('first_name'), 'form-control-success': this.fields.first_name && this.fields.first_name.valid}" id="first_name" name="first_name" placeholder="{{ trans('admin.system-writer.columns.first_name') }}">
           <div v-if="errors.has('first_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('first_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('second_name'), 'has-success': this.fields.second_name && this.fields.second_name.valid }">
     <label for="second_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.second_name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.second_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('second_name'), 'form-control-success': this.fields.second_name && this.fields.second_name.valid}" id="second_name" name="second_name" placeholder="{{ trans('admin.system-writer.columns.second_name') }}">
           <div v-if="errors.has('second_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('second_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('third_name'), 'has-success': this.fields.third_name && this.fields.third_name.valid }">
     <label for="third_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.third_name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.third_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('third_name'), 'form-control-success': this.fields.third_name && this.fields.third_name.valid}" id="third_name" name="third_name" placeholder="{{ trans('admin.system-writer.columns.third_name') }}">
           <div v-if="errors.has('third_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('third_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('last_name'), 'has-success': this.fields.last_name && this.fields.last_name.valid }">
     <label for="last_name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.last_name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.last_name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('last_name'), 'form-control-success': this.fields.last_name && this.fields.last_name.valid}" id="last_name" name="last_name" placeholder="{{ trans('admin.system-writer.columns.last_name') }}">
           <div v-if="errors.has('last_name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('last_name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('username'), 'has-success': this.fields.username && this.fields.username.valid }">
     <label for="username" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.username') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" disabled="disabled" type="text" v-model="form.username" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('username'), 'form-control-success': this.fields.username && this.fields.username.valid}" id="username" name="username" placeholder="{{ trans('admin.system-writer.columns.username') }}">
           <div v-if="errors.has('username')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('username') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('password'), 'has-success': this.fields.password && this.fields.password.valid }">
     <label for="password" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.password') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="password" v-model="form.password" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('password'), 'form-control-success': this.fields.password && this.fields.password.valid}" id="password" name="password" placeholder="{{ trans('admin.system-writer.columns.password') }}">
           <div v-if="errors.has('password')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('password') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('email'), 'has-success': this.fields.email && this.fields.email.valid }">
     <label for="email" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.email') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="email" v-model="form.email" v-validate="'required|email'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('email'), 'form-control-success': this.fields.email && this.fields.email.valid}" id="email" name="email" placeholder="{{ trans('admin.system-writer.columns.email') }}">
           <div v-if="errors.has('email')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('email') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('gender'), 'has-success': this.fields.gender && this.fields.gender.valid }">
     <label for="gender" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.gender') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="gender" :multiple="false" v-model="form.gender" v-validate="'required'" name="gender" :custom-label="opt => {{ json_encode($gender) }}.find(x => x.id == opt).name" :options="{{ json_encode($gender) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('gender'), 'form-control-success': this.fields.gender && this.fields.gender.valid}" ></multiselect>
        <div v-if="errors.has('gender')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('gender') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('country_id'), 'has-success': this.fields.country_id && this.fields.country_id.valid }">
     <label for="country_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.country_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="country_id" :multiple="false" v-model="form.country_id" v-validate="'required'" name="country_id" :custom-label="opt => {{ json_encode($countries) }}.find(x => x.id == opt).name" :options="{{ json_encode($countries) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('country_id'), 'form-control-success': this.fields.country_id && this.fields.country_id.valid}" ></multiselect>
        <div v-if="errors.has('country_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('country_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('manpower_info_majorId') ||  errors.has('manpower_info.majorId'), 'has-success': this.fields.majorId && this.fields.majorId.valid }">
     <label for="manpower_info.majorId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.majorId') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="manpower_info.majorId" :multiple="false" v-validate="'required'" v-model="form.manpower_info.major_id" name="manpower_info.majorId" :custom-label="opt => {{ json_encode($majors) }}.find(x => x.id == opt).name" :options="{{ json_encode($majors) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('manpower_info_majorId') || errors.has('manpower_info.majorId'), 'form-control-success': this.fields.majorId && this.fields.majorId.valid}" ></multiselect>
        <div v-if="errors.has('manpower_info_majorId')||errors.has('manpower_info.majorId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('manpower_info_majorId') }}@{{ errors.first('manpower_info.majorId') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('manpower_info_sub_major') || errors.has('manpower_info.sub_major'), 'has-success': this.fields.sub_major && this.fields.sub_major.valid }">
     <label for="manpower_info.sub_major" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.sub_major') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.manpower_info.sub_major" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('manpower_info_sub_major') || errors.has('manpower_info.sub_major'), 'form-control-success': this.fields.sub_major && this.fields.sub_major.valid}" id="manpower_info.sub_major" name="manpower_info.sub_major" placeholder="{{ trans('admin.system-writer.columns.sub_major') }}">
           <div v-if="errors.has('manpower_info_sub_major') || errors.has('manpower_info.sub_major')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('manpower_info_sub_major') }}@{{ errors.first('manpower_info.sub_major') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('groupsIds'), 'has-success': this.fields.groupsIds && this.fields.groupsIds.valid }">
     <label for="groupsIds" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.groupsIds') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="groupsIds" :multiple="true" v-model="form.groupsIds" v-validate="'required'" name="groupsIds" :custom-label="opt => {{ json_encode( $groups) }}.find(x => x.id == opt).name" :options="{{ json_encode($groups) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('groupsIds'), 'form-control-success': this.fields.groupsIds && this.fields.groupsIds.valid}" ></multiselect>
        <div v-if="errors.has('groupsIds')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('groupsIds') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('manpower_info_skype_account_id') || errors.has('manpower_info.skype_account_id'), 'has-success': this.fields.skype_account_id && this.fields.skype_account_id.valid }">
     <label for="manpower_info.skype_account_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.skype_account_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.manpower_info.skype_account_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('manpower_info_skype_account_id') || errors.has('manpower_info.skype_account_id'), 'form-control-success': this.fields.skype_account_id && this.fields.skype_account_id.valid}" id="manpower_info.skype_account_id" name="manpower_info.skype_account_id" placeholder="{{ trans('admin.system-writer.columns.skype_account_id') }}">
           <div v-if="errors.has('manpower_info_skype_account_id') || errors.has('manpower_info.skype_account_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('manpower_info_skype_account_id') }}@{{ errors.first('manpower_info.skype_account_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('national_id_url'), 'has-success': this.fields.national_id_url && this.fields.national_id_url.valid }">
     <label for="national_id_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.national_id_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="row">
            <div class="col-12">
                @if(!empty($user->manpower_info->national_id_url) && strtolower(pathinfo( $user->manpower_info->national_id_url, PATHINFO_EXTENSION)) == 'pdf')
                <a href="{{ $user->manpower_info->national_id_url }}" target="_blank">{{ $user->manpower_info->national_id_url }}</a>
                @elseif(!empty($user->manpower_info->national_id_url) && exif_imagetype($user->manpower_info->national_id_url))
                <img src="{{ $user->manpower_info->national_id_url }}" class="img-thumbnail img-100" data-toggle="modal" data-target="#imageModal" />
                @else
                N/A
                @endif
            </div>
        </div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('certificate_url'), 'has-success': this.fields.certificate_url && this.fields.certificate_url.valid }">
     <label for="certificate_url" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.system-writer.columns.certificate_url') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="row">
            <div class="col-12">
                @if(!empty($user->manpower_info->certificate_url) && strtolower(pathinfo( $user->manpower_info->certificate_url, PATHINFO_EXTENSION)) == 'pdf')
                <a href="{{ $user->manpower_info->certificate_url }}" target="_blank">{{ $user->manpower_info->certificate_url }}</a>
                @elseif(!empty($user->manpower_info->certificate_url) && exif_imagetype($user->manpower_info->certificate_url))
                <img src="{{ $user->manpower_info->certificate_url }}" class="img-thumbnail img-100" data-toggle="modal" data-target="#imageModal" />
                @else
                N/A
                @endif
            </div>
        </div>
    </div>
</div>
