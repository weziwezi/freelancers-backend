@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.quality-assurance.actions.index'))

@section('body')

<qa-listing
    :data="{{ $data->toJson() }}"
    :url="'{{ url('admin/quality-assurance') }}'"
    inline-template>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> {{ trans('admin.qa.actions.index') }}
                    <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/quality-assurance/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.qa.actions.create') }}</a>
                </div>
                <div class="card-body" v-cloak>
                    <form @submit.prevent="">
                        <div class="row justify-content-md-between">
                            <div class="col col-lg-7 col-xl-5 form-group">
                                <div class="input-group">
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-auto form-group ">
                                <select class="form-control" v-model="pagination.state.per_page">

                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>

                        </div>
                    </form>

                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th is='sortable' :column="'id'">{{ trans('admin.qa.columns.id') }}</th>
                                <th is='sortable' :column="'username'">{{ trans('admin.qa.columns.username') }}</th>
                                <th is='sortable' :column="'email'">{{ trans('admin.qa.columns.email') }}</th>
                                <th is='sortable' :column="'first_name'">{{ trans('admin.qa.columns.first_name') }}</th>
                                <th is='sortable' :column="'last_name'">{{ trans('admin.qa.columns.last_name') }}</th>
                                <th is='sortable' :column="'client_id'">{{ trans('admin.qa.columns.client_id') }}</th>
                                <th is='sortable' :column="'user_status'">{{ trans('admin.qa.columns.user_status') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in collection">
                                <td>@{{ item.id }}</td>
                                <td>@{{ item.username }}</td>
                                <td>@{{ item.email }}</td>
                                <td>@{{ item.first_name }}</td>
                                <td>@{{ item.last_name }}</td>
                                <td>@{{ item.client_name }}</td>
                                <td>
                                    <label class="switch switch-3d switch-success">
                                        <input type="checkbox" class="switch-input" v-model="collection[index].user_status" @change="toggleSwitch(item.q_a_s_request_url, 'user_status', collection[index])">
                                        <span class="switch-slider"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="row no-gutters">
                                        <div class="col-auto">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.q_a_s_request_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                        </div>
                                        <form class="col" @submit.prevent="deleteItem(item.q_a_s_request_url)">
                                            <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" v-if="pagination.state.total > 0">
                        <div class="col-sm">
                            <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                        </div>
                        <div class="col-sm-auto">
                            <pagination></pagination>
                        </div>
                    </div>

                    <div class="no-items-found" v-if="!collection.length > 0">
                        <i class="icon-magnifier"></i>
                        <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                        <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        <a class="btn btn-primary btn-spinner" href="{{ url('admin/users/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.qa.actions.create') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</qa-listing>

@endsection