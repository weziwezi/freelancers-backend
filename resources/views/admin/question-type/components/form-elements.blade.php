<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question-type.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input autocomplete="off" type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.question-type.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('user_type'), 'has-success': this.fields.user_type && this.fields.user_type.valid }">
     <label for="user_type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.question-type.columns.user_type') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect id="user_type" :multiple="false" v-validate="'required'" v-model="form.user_type" name="user_type" :custom-label="opt => {{ json_encode($userTypes) }}.find(x => x.id == opt).name" :options="{{ json_encode($userTypes) }}.map(type => type.id)" :searchable="true" :class="{'form-control-danger': errors.has('user_type'), 'form-control-success': this.fields.user_type && this.fields.user_type.valid}" ></multiselect>
        <div v-if="errors.has('user_type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('user_type') }}</div>
    </div>
</div>


