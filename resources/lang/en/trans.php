<?php

return [
    'New Article' => 'New Article',
    'Enrichment' => 'Enrichment',
    'Fast Enrichment' => 'Fast Enrichment',
    'Undefined' => 'Undefined',
    'Defined' => 'Defined',
    'Include Specific' => 'Include Specific',
    'Website Manager' => 'Website Manager',
    'Website Coordinator' => 'Website Coordinator',
    'Writer' => 'Writer',
    'General Proofreader' => 'General Proofreader',
    'Language Proofreader' => 'Language Proofreader',
    'Content Proofreader' => 'Content Proofreader',
    'Freelancer Images Uploader' => 'Freelancer Images Uploader',
    'Website Images Uploader' => 'Website Images Uploader',
];
?>