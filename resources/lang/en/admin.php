<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => "ID",
            'first_name' => "First name",
            'last_name' => "Last name",
            'email' => "Email",
            'password' => "Password",
            'password_repeat' => "Password Confirmation",
            'activated' => "Activated",
            'forbidden' => "Forbidden",
            'language' => "Language",
                
            //Belongs to many relations
            'roles' => "Roles",
                
        ],
    ],

    'employee' => [
        'title' => 'Employees',

        'actions' => [
            'index' => 'Employees',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'username' => "Username",
            'password' => "Password",
            'employee_type' => "Employee type",
            'is_active' => "Is active",
            
        ],
    ],

    'employee' => [
        'title' => 'Employee',

        'actions' => [
            'index' => 'Employee',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'username' => "Username",
            'email' => "Email",
            'password' => "Password",
            'first_name' => "First name",
            'second_name' => "Second name",
            'third_name' => "Third name",
            'last_name' => "Last name",
            'mobile_number' => "Mobile number",
            'birthday' => "Birthday",
            'gender' => "Gender",
            'country_id' => "Country",
            'user_type' => "User type",
            'user_status' => "User status",
            'client_id' => "Client",
            
        ],
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'user' => [
        'title' => 'User',

        'actions' => [
            'index' => 'User',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'employee' => [
        'title' => 'Employee',

        'actions' => [
            'index' => 'Employee',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'employee' => [
        'title' => 'Employees',

        'actions' => [
            'index' => 'Employees',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'username' => "Username",
            'password' => "Password",
            'employee_type' => "Employee type",
            'is_active' => "Is active",
            
        ],
    ],

    'destroy' => [
        'title' => 'Destroy',

        'actions' => [
            'index' => 'Destroy',
            'create' => 'New Destroy',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'employee' => [
        'title' => 'Employees',

        'actions' => [
            'index' => 'Employees',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'username' => "Username",
            'password' => "Password",
            'employee_type' => "Employee type",
            'is_active' => "Is active",
            
        ],
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'username' => "Username",
            'email' => "Email",
            'password' => "Password",
            'first_name' => "First name",
            'second_name' => "Second name",
            'third_name' => "Third name",
            'last_name' => "Last name",
            'mobile_number' => "Mobile number",
            'birthday' => "Birthday",
            'gender' => "Gender",
            'country_id' => "Country",
            'user_type' => "User type",
            'user_status' => "User status",
            'client_id' => "Client",
            
        ],
    ],

    'employee' => [
        'title' => 'Employees',

        'actions' => [
            'index' => 'Employees',
            'create' => 'New Employee',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'username' => "Username",
            'password' => "Password",
            'employee_type' => "Employee type",
            'is_active' => "Is active",
            
        ],
    ],

    'client' => [
        'title' => 'Clients',

        'actions' => [
            'index' => 'Clients',
            'create' => 'New Client',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'api_url' => "Api url",
            'website_url' => "Website url",
            'is_active' => "Is active",
            
        ],
    ],

    'user-level' => [
        'title' => 'User Levels',

        'actions' => [
            'index' => 'User Levels',
            'create' => 'New User Level',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'minimum_points' => "Minimum points",
            'bonus_factor' => "Bonus factor",
            'maximum_number_of_reservations' => "Maximum number of reservations",
            'user_type' => "User type",
            
        ],
    ],

    'payment-method' => [
        'title' => 'Payment Methods',

        'actions' => [
            'index' => 'Payment Methods',
            'create' => 'New Payment Method',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'is_active' => "Is active",
            
        ],
    ],

    'reward' => [
        'title' => 'Rewards',

        'actions' => [
            'index' => 'Rewards',
            'create' => 'New Reward',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'penalty' => [
        'title' => 'Penalties',

        'actions' => [
            'index' => 'Penalties',
            'create' => 'New Penalty',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'points' => "Points",
            'user_type' => "User type",
            
        ],
    ],

    'reward' => [
        'title' => 'Rewards',

        'actions' => [
            'index' => 'Rewards',
            'create' => 'New Reward',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'points' => "Points",
            'user_type' => "User type",
            
        ],
    ],

    'group' => [
        'title' => 'Groups',

        'actions' => [
            'index' => 'Groups',
            'create' => 'New Group',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'is_active' => "Is active",
            
        ],
    ],

    'question' => [
        'title' => 'Questions',

        'actions' => [
            'index' => 'Questions',
            'create' => 'New Question',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'header' => "Header",
            'body' => "Body",
            'question_type_id' => "Question type",
            
        ],
    ],

    'question-type' => [
        'title' => 'Question Types',

        'actions' => [
            'index' => 'Question Types',
            'create' => 'New Question Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'is_active' => "Is active",
            'user_type' => "User type",
            
        ],
    ],

    'questions-submission' => [
        'title' => 'Questions Submissions',

        'actions' => [
            'index' => 'Questions Submissions',
            'create' => 'New Questions Submission',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'question_id' => "Question",
            'answer' => "Answer",
            
        ],
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'username' => "Username",
            'email' => "Email",
            'password' => "Password",
            'first_name' => "First name",
            'second_name' => "Second name",
            'third_name' => "Third name",
            'last_name' => "Last name",
            'mobile_number' => "Mobile number",
            'birthday' => "Birthday",
            'gender' => "Gender",
            'country_id' => "Country",
            'user_type' => "User type",
            'user_status' => "User status",
            'client_id' => "Client",
            
        ],
    ],

    'q-a' => [
        'title' => 'Qa',

        'actions' => [
            'index' => 'Qa',
            'create' => 'New Qa',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'quality' => [
        'title' => 'Quality',

        'actions' => [
            'index' => 'Quality',
            'create' => 'New Quality',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'user' => [
        'title' => 'Systemwriters',

        'actions' => [
            'index' => 'Systemwriters',
            'create' => 'New Systemwriter',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'system-writer' => [
        'title' => 'Systemwriters',

        'actions' => [
            'index' => 'Systemwriters',
            'create' => 'New Systemwriter',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'system-proofreader' => [
        'title' => 'Systemproofreaders',

        'actions' => [
            'index' => 'Systemproofreaders',
            'create' => 'New Systemproofreader',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];