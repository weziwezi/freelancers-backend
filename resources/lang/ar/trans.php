<?php

return [
    'New Article' => 'مقال جديد',
    'Enrichment' => 'إثراء',
    'Fast Enrichment' => 'إثراء سريع',
    'Undefined' => 'غير محدد',
    'Defined' => 'محدد',
    'Include Specific' => 'محدد مع إمكانية الاضافة',
    'Website Manager' => 'مدير الموقع',
    'Website Coordinator' => 'منسق',
    'Writer' => 'كاتب',
    'General Proofreader' => 'مدقق شامل',
    'Language Proofreader' => 'مدقق لغة',
    'Content Proofreader' => 'مدقق محتوى',
    'Freelancer Images Uploader' => 'رافع صور حر',
    'Website Images Uploader' => 'رافع صور',
];
?>