import AppListing from '../app-components/Listing/AppListing';

Vue.component('system-proofreader-listing', {
    mixins: [AppListing]
});