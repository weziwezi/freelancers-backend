import AppListing from '../app-components/Listing/AppListing';

Vue.component('system-writer-listing', {
    mixins: [AppListing]
});