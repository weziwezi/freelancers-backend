import AppForm from '../app-components/Form/AppForm';

Vue.component('system-writer-form', {
    mixins: [AppForm],
    data: function () {
        return {
            form: {
                first_name: '',
                second_name: '',
                third_name: '',
                last_name: ''
            }
        }
    }

});