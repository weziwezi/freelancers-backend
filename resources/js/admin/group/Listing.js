import AppListing from '../app-components/Listing/AppListing';

Vue.component('group-listing', {
    mixins: [AppListing]
});