import AppForm from '../app-components/Form/AppForm';

Vue.component('group-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                is_active:  false ,
                
            }
        }
    }

});