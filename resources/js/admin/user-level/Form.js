import AppForm from '../app-components/Form/AppForm';

Vue.component('user-level-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                minimum_points:  '' ,
                bonus_factor:  '' ,
                maximum_number_of_reservations:  '' ,
                user_type:  false ,
                
            }
        }
    }

});