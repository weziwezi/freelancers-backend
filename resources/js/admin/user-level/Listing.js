import AppListing from '../app-components/Listing/AppListing';

Vue.component('user-level-listing', {
    mixins: [AppListing]
});