import AppListing from '../app-components/Listing/AppListing';

Vue.component('clients-writers-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            client_id: ''
        }
    }
});