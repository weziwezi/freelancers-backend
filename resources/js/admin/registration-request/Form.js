import AppForm from '../app-components/Form/AppForm';

Vue.component('registration-request-form', {
    mixins: [AppForm],
    data: function () {
        return {
            form: {
                rejection_reason: '',
                user_status: '',
            }
        }
    },
    methods: {
        changeUserStatus: function (status) {
            this.form.user_status = status;
        }
    }

});