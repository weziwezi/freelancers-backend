import AppListing from '../app-components/Listing/AppListing';

Vue.component('registration-request-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            user_type: ''
        }
    }
});