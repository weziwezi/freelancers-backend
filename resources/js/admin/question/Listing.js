import AppListing from '../app-components/Listing/AppListing';

Vue.component('question-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            question_type_id: ''
        }
    }
});