import './bootstrap';

import 'vue-multiselect/dist/vue-multiselect.min.css';
import flatPickr from 'vue-flatpickr-component';
import VueQuillEditor from 'vue-quill-editor';
import Notifications from 'vue-notification';
import Multiselect from 'vue-multiselect';
import VeeValidate from 'vee-validate';
import 'flatpickr/dist/flatpickr.css';
import VueCookie from 'vue-cookie';
import { Admin } from 'craftable';
import VModal from 'vue-js-modal'
import Vue from 'vue';

import './app-components/bootstrap';
import './index';

import 'craftable/dist/ui';

Vue.component('multiselect', Multiselect);
Vue.use(VeeValidate, {strict: true});
Vue.component('datetime', flatPickr);
Vue.use(VModal, {dialog: true});
Vue.use(VueQuillEditor);
Vue.use(Notifications);
Vue.use(VueCookie);

new Vue({
    mixins: [Admin],
});



$('#imageModal').on('show.bs.modal', function (event) {
    $(this).find('img').attr('src', $(event.relatedTarget).attr('src'));
});

$(document).ready(function () {
    var pages = window.location.pathname.split('/');
    var page = '';
    var origin = window.location.origin;
    for (var i = 0; i < pages.length; i++) {
        if (pages[i].length != 0) {
            page = page + pages[i];
            $('.app-body > .sidebar a').each(function () {
                if ($(this).attr('href') == origin + '/admin/' + page) {
                    var link = $(this);
                    link.addClass('active');
                    var parent = $(this).closest('.nav-dropdown');
                    if (parent.length)
                    {
                        parent.addClass('open').addClass('active');
                        //parent.find('.nav-dropdown-toggle').addClass('active');
                        //link.css('background','#4386aa');
                    }
                }
            });
            page = page + '/';
        }
    }


});