import AppListing from '../app-components/Listing/AppListing';

Vue.component('reward-listing', {
    mixins: [AppListing]
});