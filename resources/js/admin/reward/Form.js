import AppForm from '../app-components/Form/AppForm';

Vue.component('reward-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                points:  '' ,
                user_type:  false ,
                
            }
        }
    }

});