import AppForm from '../app-components/Form/AppForm';

Vue.component('qa-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                username:  '' ,
                email:  '' ,
                password:  '' ,
                first_name:  '' ,
                second_name:  '' ,
                third_name:  '' ,
                last_name:  '' ,
                mobile_number:  '' ,
                birthday:  '' ,
                gender:  '' ,
                country_id:  '' ,
                user_type:  false ,
                user_status:  false ,
                client_id:  '' 
            }
        }
    }

});