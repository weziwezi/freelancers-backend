import AppListing from '../app-components/Listing/AppListing';

Vue.component('qa-listing', {
    mixins: [AppListing]
});