import AppForm from '../app-components/Form/AppForm';

Vue.component('questions-submission-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                user_id:  '' ,
                question_id:  '' ,
                answer:  '' ,
                
            }
        }
    }

});