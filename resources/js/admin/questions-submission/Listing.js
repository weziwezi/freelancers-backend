import AppListing from '../app-components/Listing/AppListing';

Vue.component('questions-submission-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            user_type: ''
        }
    }
});