import AppListing from '../app-components/Listing/AppListing';

Vue.component('penalty-listing', {
    mixins: [AppListing]
});