import AppForm from '../app-components/Form/AppForm';

Vue.component('penalty-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                points:  '' ,
                user_type:  false ,
                
            }
        }
    }

});