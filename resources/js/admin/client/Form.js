import AppForm from '../app-components/Form/AppForm';

Vue.component('client-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                api_url:  '' ,
                website_url:  '' ,
                is_active:  false ,
                
            }
        }
    }

});