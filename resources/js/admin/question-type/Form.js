import AppForm from '../app-components/Form/AppForm';

Vue.component('question-type-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                is_active:  false ,
                user_type:  false ,
                
            }
        }
    }

});