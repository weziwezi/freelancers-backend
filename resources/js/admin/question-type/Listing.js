import AppListing from '../app-components/Listing/AppListing';

Vue.component('question-type-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            user_type: ''
        }
    }
});