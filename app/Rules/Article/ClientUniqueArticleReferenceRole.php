<?php

namespace App\Rules\Article;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule AS VRule;
use Illuminate\Contracts\Validation\Rule AS CRule;

class ClientUniqueArticleReferenceRole implements CRule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private function rule()
    {
        return VRule::exists('references', 'id')->where(function($query) {
                    $query->whereIn('subgroup_id', function($query) {
                        $query->select('subgroups.id')->from('subgroups')->where('client_id', Auth()->user()->client_id);
                    });
                });
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $data = ['articles_references_id' => $value];
        $rule = ['articles_references_id' => $this->rule()];

        return Validator::make($data, $rule)->passes();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Reference Not Exists';
    }

}
