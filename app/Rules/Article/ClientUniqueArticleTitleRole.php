<?php

namespace App\Rules\Article;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule AS VRule;
use Illuminate\Contracts\Validation\Rule AS CRule;

class ClientUniqueArticleTitleRole implements CRule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private function rule()
    {
        $rule = VRule::unique('articles')->using(function($query) {
            $query->whereIn('subgroup_id', function($query) {
                $query->select('subgroups.id')->from('subgroups')->where('subgroups.client_id', Auth()->user()->client_id);
            });
        });

        if (strtoupper(request()->method()) == 'PUT' && isset(request()->articleId)) {
            $rule->ignore(request()->articleId);
        }
        return $rule;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $data = ['title' => $value];
        $rule = ['title' => $this->rule()];

        return Validator::make($data, $rule)->passes();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Article Title Already Exists';
    }

}
