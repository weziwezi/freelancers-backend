<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\{
    User,
    Article
};

class ArticlePolicy
{

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Check if user can access to article or not
     * @param App\Models\User $user
     * @param App\Models\Article $article
     */
    public function access(User $user, Article $article)
    {
        // get user subgroup
        $subgroups = $user->client->subgroups->toArray();
        // check if the article in $subgroups
        if (count($subgroups) == 0 || !in_array($article->subgroup_id, array_column($subgroups, 'id')))
            abort(401);
        return true;
    }

}
