<?php

namespace App\Constants;

class RejectionReason
{

    const WRONG_GROUP = 1;
    const UNI_CERTIFICATE = 2;
    const NATIONAL_ID = 3;

    public static function getAllRejectionReason()
    {
        return [
            ['id' => self::WRONG_GROUP, 'name' => trans('Wrong Group')],
            ['id' => self::UNI_CERTIFICATE, 'name' => trans('Wrong Uni Certificate')],
            ['id' => self::NATIONAL_ID, 'name' => trans('Wrong National ID')]
        ];
    }

    public static function getRejectResonForRegistrationRequest()
    {
        $resons = [];
        foreach (self::getAllRejectionReason() as $key => $value) {
            if (in_array($value['id'], [1, 2, 3]))
                $resons[] = $value;
        }
        return $resons;
    }

}
