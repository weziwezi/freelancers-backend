<?php

namespace App\Constants;

class UserGender
{

    const USER_MALE = 1;
    const USER_FEMALE = 2;

    public static function getAllUserGenders()
    {
        return [
            ['id' => self::USER_MALE, 'name' => trans('Male')],
            ['id' => self::USER_FEMALE, 'name' => trans('Female')],
        ];
    }

    public static function getAllUserGendersIds()
    {
        return array_column(self::getAllUserGenders(), 'id');
    }

    public static function getUserGenderNameByID($genderId)
    {
        $genderArray = array_column(self::getAllUserGenders(), 'name', 'id');
        return (isset($genderArray[$genderId]) ? $genderArray[$genderId] : 'N/A');
    }

}
