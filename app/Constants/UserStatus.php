<?php

namespace App\Constants;

class UserStatus
{

    const EMAIL_VERIFICATION = 1;
    const MAJOR_INFO = 2;
    const PHONE_ACTIVATION = 3;
    const EXAM_IS_NOT_PERFORMED = 4;
    // after submit
    const EXAM_IS_PERFORMED = 5;
    // waiting status of exam
    const EXAM_IS_APPROVED = 6;
    # const FILES_ARE_NOT_UPLOADED = 7;
    const FILES_ARE_UPLOADED = 8;

    /** Its like active but can't take any reserved title until fill payment method */
    const SHOULD_OPERATE = 9;
    const ACTIVE = 10;
    const REJECTED = 11;
    const BLOCKED = 12;

}
