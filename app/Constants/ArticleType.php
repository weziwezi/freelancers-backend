<?php

namespace App\Constants;

class ArticleType
{

    const FRESH = 1;
    const ENRICHMENT = 2;
    const FAST_ENRICHMENT = 3;

    public static function getArticleTypes()
    {
        $articleTypes = [
            ['id' => self::FRESH, 'name' => trans('trans.New Article')],
            ['id' => self::ENRICHMENT, 'name' => trans('trans.Enrichment')],
                //['id' => self::FAST_ENRICHMENT, 'name' => trans('trans.Fast Enrichment')]
        ];

        return $articleTypes;
    }

    public static function getArticleTypesIds()
    {
        return array_column(self::getArticleTypes(), 'id');
    }

    public static function getArticleTypesNameById(int $id)
    {
        $articleTypes = array_column(self::getArticleTypes(), 'name', 'id');
        if (isset($articleTypes[$id])) {
            return $articleTypes[$id];
        }
        return [];
    }

}
