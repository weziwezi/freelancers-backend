<?php

namespace App\Constants;

class ReferencingType
{

    const UNDEFINED = 1;
    const DEFINED = 2;
    const INCLUDING_SPECIFIC = 3;

    public static function getReferencingType()
    {
        return [
            ['id' => self::UNDEFINED, 'name' => trans('trans.Undefined')],
            ['id' => self::DEFINED, 'name' => trans('trans.Defined')],
            ['id' => self::INCLUDING_SPECIFIC, 'name' => trans('trans.Include Specific')],
        ];
    }

    public static function getReferencingTypeIdThatNeedRefereces()
    {
        $noNeedRefrences = [self::UNDEFINED];
        return array_diff(self::getReferencingTypeId(), $noNeedRefrences);
    }

    public static function getReferencingTypeId()
    {
        return array_column(self::getReferencingType(), 'id');
    }

}
