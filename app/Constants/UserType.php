<?php

namespace App\Constants;

class UserType
{

    const WEBSITE_MANAGER = 1;
    const WEBSITE_COORDINATOR = 2;
    const WRITER = 3;

    /** Its General Proofreader */
    const PROOFREADER = 4;
    const LANGUAGE_PROOFREADER = 5;
    const CONTENT_PROOFREADER = 6;
    const FREELANCER_IMAGES_UPLOADER = 7;
    const WEBSITE_IMAGES_UPLOADER = 8;

    public static function getUserTypesIdByRoleName($role = '')
    {
        $proofreaders = [
            self::PROOFREADER,
            self::LANGUAGE_PROOFREADER,
            self::CONTENT_PROOFREADER,
        ];

        $manpower = [
            self::WRITER,
            self::FREELANCER_IMAGES_UPLOADER,
                //self::WEBSITE_IMAGES_UPLOADER //Based on Hikmat no need to return in any screen
        ];

        $client = [
            self::WEBSITE_MANAGER,
            self::WEBSITE_COORDINATOR
        ];

        // man
        // This Type for users can register no need to loged in user
        $canRegister = array_diff(array_merge($proofreaders, $manpower), [self::FREELANCER_IMAGES_UPLOADER]);

        $userTypesRole = [
            'proofreaders' => $proofreaders,
            'manpower' => array_merge($manpower, $proofreaders),
            'client' => $client,
            'canRegister' => $canRegister
        ];

        return (isset($userTypesRole[$role]) ? $userTypesRole[$role] : $userTypesRole);
    }

    public static function getUserTypes()
    {
        return [
            ['id' => self::WEBSITE_MANAGER, 'name' => trans('trans.Website Manager')],
            ['id' => self::WEBSITE_COORDINATOR, 'name' => trans('trans.Website Coordinator')],
            ['id' => self::WRITER, 'name' => trans('trans.Writer')],
            ['id' => self::PROOFREADER, 'name' => trans('trans.General Proofreader')],
            ['id' => self::LANGUAGE_PROOFREADER, 'name' => trans('trans.Language Proofreader')],
            ['id' => self::CONTENT_PROOFREADER, 'name' => trans('trans.Content Proofreader')],
            ['id' => self::FREELANCER_IMAGES_UPLOADER, 'name' => trans('trans.Freelancer Images Uploader')],
            ['id' => self::WEBSITE_IMAGES_UPLOADER, 'name' => trans('trans.Website Images Uploader')],
        ];
    }

    public static function getUserTypesCanRegister()
    {
        $userTypes = [];
        foreach (self::getUserTypes() as $key => $userType) {
            if (in_array($userType['id'], self::getUserTypesIdByRoleName('canRegister')))
                $userTypes[] = $userType;
        }
        return $userTypes;
    }

    public static function getUserTypeNameById(int $userTypeId)
    {
        $userTypes = array_column(self::getUserTypes(), 'name', 'id');
        return (isset($userTypes[$userTypeId]) ? $userTypes[$userTypeId] : 'N/A');
    }

    public static function getClientUserTypes()
    {
        $userTypes = [];
        foreach (self::getUserTypes() as $key => $userType) {
            if (in_array($userType['id'], self::getUserTypesIdByRoleName('client')))
                $userTypes[] = $userType;
        }
        return $userTypes;
    }

    /**
     * Get user type that specified for user level page
     * You can add new level by added in array [3, 4]
     * @return array 
     */
    public static function getManpowerUserTypes()
    {
        $userTypes = [];
        foreach (self::getUserTypes() as $key => $userType) {
            if (in_array($userType['id'], self::getUserTypesIdByRoleName('manpower')))
                $userTypes[] = $userType;
        }
        return $userTypes;
    }

    /**
     * Get Manpower Ids
     * @return type
     */
    public static function getManpowerUserTypesIds()
    {
        return array_column(self::getManpowerUserTypes(), 'id');
    }

    public static function getUserTypesIds()
    {
        return array_column(self::getUserTypes(), 'id');
    }

    public static function getProofreadersUserTypesIds()
    {
        return self::getUserTypesIdByRoleName('proofreaders');
    }

    public static function getUserTypesCanRegisterIds()
    {
        return self::getUserTypesIdByRoleName('canRegister');
    }

}
