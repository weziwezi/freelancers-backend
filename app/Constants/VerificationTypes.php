<?php

namespace App\Constants;

class VerificationTypes
{

    const EMAIL = 1;
    const MOBILE = 2;

}
