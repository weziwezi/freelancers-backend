<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Question;
use App\Models\Subgroup;
use App\Models\User;

class Group extends Model
{

    use SoftDeletes;

    protected $fillable = [
        "name",
        "is_active",
    ];
    
    protected $hidden = [
    ];
    
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    
    protected $casts = [
        'is_active' => 'bool'
    ];
    
    protected $appends = ['resource_url'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/groups/' . $this->getKey());
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'questions_groups')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function subgroups()
    {
        return $this->hasMany(Subgroup::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_groups')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

}
