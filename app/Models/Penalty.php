<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 21 Jul 2019 11:18:29 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Penalty
 * 
 * @property int $id
 * @property string $name
 * @property int $points
 * @property int $user_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $clients_penalties
 *
 * @package App\Models
 */
class Penalty extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'points' => 'int',
        'user_type' => 'int'
    ];
    protected $fillable = [
        'name',
        'points',
        'user_type'
    ];
    protected $visible = [
        'id',
        'name',
        'points',
        'user_type'
    ];
    protected $hidden = [
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $appends = ['resource_url'];

    public function clients_penalties()
    {
        return $this->hasMany(\App\Models\ClientsPenalty::class);
    }

    public function clients()
    {
        return $this->belongsToMany(\App\Models\Client::class);
    }

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/penalties/' . $this->getKey());
    }

}
