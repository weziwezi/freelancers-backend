<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 15 Aug 2019 13:09:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Article
 * 
 * @property int $id
 * @property string $title
 * @property string $conditions
 * @property string $key_word
 * @property int $rejection_counter
 * @property int $client_main_category_id
 * @property int $client_sub_category_id
 * @property int $article_status
 * @property int $article_type
 * @property int $article_level_id
 * @property int $subgroup_id
 * @property int $writer_user_id
 * @property int $language_proofreader_user_id
 * @property int $content_proofreader_user_id
 * @property int $proofreader_user_id
 * @property int $images_uploader_user_id
 * @property \Carbon\Carbon $writer_reservation_time
 * @property \Carbon\Carbon $proofreader_reservation_time
 * @property \Carbon\Carbon $images_uploader_reservation_time
 * @property string $should_be_seen_by_freelancers
 * @property string $should_be_seen_by_client_users
 * @property bool $referencing_type
 * @property string $content
 * @property string $proofreader_notes
 * @property string $coordinator_notes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\ArticleLevel $article_level
 * @property \App\Models\User $user
 * @property \App\Models\Subgroup $subgroup
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $references
 *
 * @package App\Models
 */
class Article extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'rejection_counter' => 'int',
        'client_main_category_id' => 'int',
        'client_sub_category_id' => 'int',
        'article_status' => 'int',
        'article_type' => 'int',
        'article_level_id' => 'int',
        'subgroup_id' => 'int',
        'writer_user_id' => 'int',
        'language_proofreader_user_id' => 'int',
        'content_proofreader_user_id' => 'int',
        'proofreader_user_id' => 'int',
        'images_uploader_user_id' => 'int',
        'referencing_type' => 'int'
    ];
    protected $dates = [
        'writer_reservation_time',
        'proofreader_reservation_time',
        'images_uploader_reservation_time'
    ];
    protected $fillable = [
        'title',
        'conditions',
        'key_word',
        'rejection_counter',
        'client_main_category_id',
        'client_sub_category_id',
        'article_status',
        'article_type',
        'article_level_id',
        'subgroup_id',
        'writer_user_id',
        'language_proofreader_user_id',
        'content_proofreader_user_id',
        'proofreader_user_id',
        'images_uploader_user_id',
        'writer_reservation_time',
        'proofreader_reservation_time',
        'images_uploader_reservation_time',
        'should_be_seen_by_freelancers',
        'should_be_seen_by_client_users',
        'referencing_type',
        'content',
        'proofreader_notes',
        'coordinator_notes'
    ];

    public function article_level()
    {
        return $this->belongsTo(\App\Models\ArticleLevel::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'writer_user_id');
    }

    public function subgroup()
    {
        return $this->belongsTo(\App\Models\Subgroup::class);
    }

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'articles_blocked_users')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function references()
    {
        return $this->belongsToMany(\App\Models\Reference::class, 'articles_references')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

}
