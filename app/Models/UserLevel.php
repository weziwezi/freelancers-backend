<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLevel extends Model
{

    use SoftDeletes;

    protected $fillable = [
        "name",
        "minimum_points",
        "bonus_factor",
        "maximum_number_of_reservations",
        "user_type",
        'is_default'
    ];
    protected $hidden = [];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $appends = ['resource_url'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/user-levels/' . $this->getKey());
    }

}
