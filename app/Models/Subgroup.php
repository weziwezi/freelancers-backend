<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Subgroup
 * 
 * @property int $id
 * @property string $name
 * @property int $client_id
 * @property int $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Client $client
 * @property \App\Models\Group $group
 * @property \Illuminate\Database\Eloquent\Collection $articles
 * @property \Illuminate\Database\Eloquent\Collection $references
 * @property \Illuminate\Database\Eloquent\Collection $user_references
 *
 * @package App\Models
 */
class Subgroup extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'client_id' => 'int',
        'group_id' => 'int'
    ];
    protected $fillable = [
        'name',
        'client_id',
        'group_id',
        'is_active'
    ];
    protected $visible = [
        'id',
        'name',
        'client_id',
        'group_id',
        'is_active'
    ];

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class);
    }

    public function articles()
    {
        return $this->hasMany(\App\Models\Article::class);
    }

    public function references()
    {
        return $this->hasMany(\App\Models\Reference::class);
    }

    public function article_levels()
    {
        return $this->belongsToMany(\App\Models\ArticleLevel::class, 'article_levels_subgroups')->withTimestamps();
    }

    public function user_references()
    {
        return $this->hasMany(\App\Models\UserReference::class);
    }

}
