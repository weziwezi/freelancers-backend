<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 29 Jul 2019 10:05:17 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersGroup
 * 
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Group $group
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersGroup extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int',
		'group_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'group_id'
	];

    protected $visible = [
        'id',
        'user_id',
        'group_id'
    ];

	public function group()
	{
		return $this->belongsTo(\App\Models\Group::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
