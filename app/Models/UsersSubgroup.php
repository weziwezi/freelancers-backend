<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 03 Aug 2019 16:45:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersSubgroup
 * 
 * @property int $id
 * @property int $user_id
 * @property int $subgroup_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Subgroup $subgroup
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersSubgroup extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int',
		'subgroup_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'subgroup_id'
	];

    protected $visible = [
        'id',
        'user_id',
        'subgroup_id'
    ];

	public function subgroup()
	{
		return $this->belongsTo(\App\Models\Subgroup::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
