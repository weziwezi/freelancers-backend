<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
#use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $second_name
 * @property string $third_name
 * @property string $last_name
 * @property string $mobile_number
 * @property \Carbon\Carbon $birthday
 * @property string $gender
 * @property int $country_id
 * @property int $user_type
 * @property int $user_status
 * @property int $client_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\Client $client
 * @property \App\Models\Country $country
 * @property \App\Models\Country $country_code
 * @property \Illuminate\Database\Eloquent\Collection $article_levels
 * @property \Illuminate\Database\Eloquent\Collection $articles
 * @property \Illuminate\Database\Eloquent\Collection $mobile_codes
 * @property \Illuminate\Database\Eloquent\Collection $questions_submissions
 * @property \Illuminate\Database\Eloquent\Collection $user_references
 * @property \Illuminate\Database\Eloquent\Collection $groups
 * @property \Illuminate\Database\Eloquent\Collection $users_notes
 * @property \Illuminate\Database\Eloquent\Collection $users_payment_infos
 * @property \Illuminate\Database\Eloquent\Collection $users_rejection_reasons
 *
 * @package App\Models
 */
class User extends Authenticatable #implements MustVerifyEmail
{

    use SoftDeletes,
        HasApiTokens,
        Notifiable;

    protected $casts = [
        'user_type' => 'int',
        'user_status' => 'int',
        'client_id' => 'int'
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $hidden = [
        'password'
    ];
    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'second_name',
        'third_name',
        'last_name',
        'mobile_number',
        'birthday',
        'gender',
        'country_id',
        'country_code_id',
        'user_type',
        'user_status',
        'client_id',
        'is_verified_email',
        'is_verified_mobile',
    ];
    protected $visible = [
        'id',
        'username',
        'email',
        'password',
        'first_name',
        'second_name',
        'third_name',
        'last_name',
        'user_type',
        'user_status',
        'client_id',
        'country_id',
        'country_code_id',
        'resource_url',
        'questions_submissions_url',
        'registration_request_url',
        'gender_name',
        'writers_url',
        'proofreaders_url',
        'client_name',
        'groups',
        'subgroups',
        'manpower_info',
        'country',
        'gender',
        'groupsIds',
        'mobile_number',
        'birthday'
    ];

//    protected $appends = ['resource_url', 'questions_submissions_url', 'registration_request_url', 'gender_name', 'writers_url', 'proofreaders_url'];

    /*     * *********************** ACCESSOR ************************* */

    // for Hased Password
    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::needsRehash($value) ? Hash::make($value) : $value;
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/managers/' . $this->getKey());
    }

    public function getSystemProofreadersUrlAttribute()
    {
        return url('/admin/system/proofreaders/' . $this->getKey());
    }

    public function getQuestionsSubmissionsUrlAttribute()
    {
        return url('/admin/submitted-exams/' . $this->getKey());
    }

    public function getRegistrationRequestUrlAttribute()
    {
        return url('/admin/registration-request/' . $this->getKey());
    }

    public function getClientsWritersUrlAttribute()
    {
        return url('/admin/clients/writers/' . $this->getKey());
    }

    public function getQASRequestUrlAttribute()
    {
        return url('/admin/quality-assurance/' . $this->getKey());
    }

    public function getClientsProofreadersUrlAttribute()
    {
        return url('/admin/clients/proofreaders/' . $this->getKey());
    }

    public function getSystemWritersUrlAttribute()
    {
        return url('/admin/system/writers/' . $this->getKey());
    }

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }

    public function country_code()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }

    public function article_levels()
    {
        return $this->hasMany(\App\Models\ArticleLevel::class);
    }

    public function articles()
    {
        return $this->belongsToMany(\App\Models\Article::class, 'articles_blocked_users')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function mobile_codes()
    {
        return $this->hasMany(\App\Models\MobileCode::class);
    }

    public function questions_submissions()
    {
        return $this->hasMany(\App\Models\QuestionsSubmission::class);
    }

    public function user_references()
    {
        return $this->hasMany(\App\Models\UserReference::class);
    }

    public function groups()
    {
        return $this->belongsToMany(\App\Models\Group::class, 'users_groups')
                        ->withPivot('id', 'deleted_at')
                        ->whereNull('users_groups.deleted_at')
                        ->withTimestamps();
    }

    public function subgroups()
    {
        return $this->belongsToMany(\App\Models\Subgroup::class, 'users_subgroups')
                        ->withPivot('id', 'deleted_at')
                        ->whereNull('users_subgroups.deleted_at')
                        ->withTimestamps();
    }

    public function users_notes()
    {
        return $this->hasMany(\App\Models\UsersNote::class);
    }

    public function users_payment_infos()
    {
        return $this->hasMany(\App\Models\UsersPaymentInfo::class);
    }

    public function users_rejection_reasons()
    {
        return $this->hasMany(\App\Models\UsersRejectionReason::class);
    }

    public function manpower_info()
    {
        return $this->hasOne(\App\Models\ManpowerInfo::class);
    }

    public function getGenderNameAttribute()
    {
        return \App\Constants\UserGender::getUserGenderNameByID($this->gender);
    }

    public function AauthAcessToken()
    {
        return $this->hasMany(OauthAccessToken::class);
    }

    public function userToken()
    {
        return $this->hasOne(\App\Models\UserToken::class);
    }

}
