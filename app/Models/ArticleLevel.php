<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 08 Aug 2019 08:00:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ArticleLevel
 * 
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $number_of_words
 * @property int $number_of_references
 * @property int $number_of_sub_headlines
 * @property int $number_of_internal_links
 * @property int $writing_delivery_time
 * @property int $language_proofreading_delivery_time
 * @property int $content_proofreading_delivery_time
 * @property int $proofreading_delivery_time
 * @property int $images_uploading_delivery_time
 * @property float $writing_price
 * @property float $language_proofreading_price
 * @property float $content_proofreading_price
 * @property float $proofreading_price
 * @property float $images_uploading_price
 * @property int $article_type
 * @property int $client_id
 * @property bool $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Client $client
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $user_levels
 * @property \Illuminate\Database\Eloquent\Collection $articles
 *
 * @package App\Models
 */
class ArticleLevel extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'user_id' => 'int',
        'number_of_words' => 'int',
        'number_of_references' => 'int',
        'number_of_sub_headlines' => 'int',
        'number_of_internal_links' => 'int',
        'writing_delivery_time' => 'int',
        'language_proofreading_delivery_time' => 'int',
        'content_proofreading_delivery_time' => 'int',
        'proofreading_delivery_time' => 'int',
        'images_uploading_delivery_time' => 'int',
        'writing_price' => 'float',
        'language_proofreading_price' => 'float',
        'content_proofreading_price' => 'float',
        'proofreading_price' => 'float',
        'images_uploading_price' => 'float',
        'article_type' => 'int',
        'client_id' => 'int',
        'is_active' => 'bool'
    ];
    protected $fillable = [
        'name',
        'user_id',
        'number_of_words',
        'number_of_references',
        'number_of_sub_headlines',
        'number_of_internal_links',
        'writing_delivery_time',
        'language_proofreading_delivery_time',
        'content_proofreading_delivery_time',
        'proofreading_delivery_time',
        'images_uploading_delivery_time',
        'writing_price',
        'language_proofreading_price',
        'content_proofreading_price',
        'proofreading_price',
        'images_uploading_price',
        'article_type',
        'client_id',
        'is_active'
    ];

    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function user_levels()
    {
        return $this->belongsToMany(\App\Models\UserLevel::class, 'article_levels_user_levels')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function subgroups()
    {
        return $this->belongsToMany(\App\Models\Subgroup::class, 'article_levels_subgroups')->withTimestamps();
    }

    public function articles()
    {
        return $this->hasMany(\App\Models\Article::class);
    }

    public function getStatusAttribute()
    {
        return url('/admin/manage_clients/' . $this->getKey());
    }

    // writing_price
    public function setWritingPriceAttribute($writing_price)
    {
        $this->attributes['writing_price'] = $writing_price * 100;
    }

    public function getWritingPriceAttribute($writing_price)
    {
        return $this->attributes['writing_price'] = $writing_price / 100;
    }

    // language_proofreading_price
    public function setLanguageProofreadingPriceAttribute($language_proofreading_price)
    {
        $this->attributes['language_proofreading_price'] = $language_proofreading_price * 100;
    }

    public function getLanguageProofreadingPriceAttribute($language_proofreading_price)
    {
        return $this->attributes['language_proofreading_price'] = $language_proofreading_price / 100;
    }

    // content_proofreading_price
    public function setContentProofreadingPriceAttribute($content_proofreading_price)
    {
        $this->attributes['content_proofreading_price'] = $content_proofreading_price * 100;
    }

    public function getContentProofreadingPriceAttribute($content_proofreading_price)
    {
        return $this->attributes['content_proofreading_price'] = $content_proofreading_price / 100;
    }

    // proofreading_price
    public function setProofreadingPriceAttribute($proofreading_price)
    {
        $this->attributes['proofreading_price'] = $proofreading_price * 100;
    }

    public function getProofreadingPriceAttribute($proofreading_price)
    {
        return $this->attributes['proofreading_price'] = $proofreading_price / 100;
    }

    // images_uploading_price
    public function setImagesUploadingPriceAttribute($images_uploading_price)
    {
        $this->attributes['images_uploading_price'] = $images_uploading_price * 100;
    }

    public function getImagesUploadingPriceAttribute($images_uploading_price)
    {
        return $this->attributes['images_uploading_price'] = $images_uploading_price / 100;
    }

}
