<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\QuestionType;
use App\Models\QuestionsSubmission;
use App\Models\Group;
use App\Models\QuestionsGroup;

/**
 * Class Question
 *
 * @property int $id
 * @property string $header
 * @property string $body
 * @property int $question_type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\QuestionType $question_type
 * @property \Illuminate\Database\Eloquent\Collection $groups
 * @property \Illuminate\Database\Eloquent\Collection $questions_submissions
 *
 * @package App\Models
 */
class Question extends Model
{

    use SoftDeletes;

    protected $casts = [
        'question_type_id' => 'int'
    ];
    protected $fillable = [
        'header',
        'body',
        'question_type_id',
    ];
    protected $visiable = [
        'id',
        'header',
        'body',
        'question_type_id',
    ];
    protected $hidden = [];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $appends = ['resource_url', 'group_id', 'question_type_name'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/questions/' . $this->getKey());
    }

    public function question_type()
    {
        return $this->belongsTo(QuestionType::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'questions_groups')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function questions_submissions()
    {
        return $this->hasMany(QuestionsSubmission::class);
    }

    public function getGroupIdAttribute()
    {
        //return $this->groups->pluck('id');
        return $this->groups()->allRelatedIds();
    }

    public function getQuestionTypeNameAttribute()
    {
        // there is only one name
        return $this->question_type()->pluck('name')[0];
    }

}
