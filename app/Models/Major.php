<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jul 2019 06:03:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Major
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $manpower_infos
 *
 * @package App\Models
 */
class Major extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function manpower_infos()
    {
        return $this->hasMany(\App\Models\ManpowerInfo::class);
    }

}
