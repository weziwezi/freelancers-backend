<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Question;
use App\Models\User;

/**
 * Class QuestionsSubmission
 * 
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Question $question
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class QuestionsSubmission extends Model {

    use SoftDeletes;

    protected $fillable = [
        "user_id",
        "question_id",
        "answer",
    ];
    protected $hidden = [];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $casts = [
        'user_id' => 'int',
        'question_id' => 'int'
    ];
    protected $appends = ['resource_url']; //, 'user_full_name'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/submitted-exams/' .$this->user_id);
    }

    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

//    public function getUserFullNameAttribute()
//    {
//        return implode(' ', array_filter($this->user->only(['first_name', 'second_name', 'third_name', 'last_name'])));
//    }
}
