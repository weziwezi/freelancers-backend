<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Question;

/**
 * Class QuestionType
 * 
 * @property int $id
 * @property string $name
 * @property bool $is_active
 * @property int $user_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $questions
 *
 * @package App\Models
 */
class QuestionType extends Model
{

    use SoftDeletes;

    protected $fillable = [
        "name",
        "is_active",
        "user_type",
    ];
    protected $hidden = [];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $casts = [
        'is_active' => 'bool',
        'user_type' => 'int'
    ];
    protected $appends = ['resource_url'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/question-types/' . $this->getKey());
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

   
    
}
