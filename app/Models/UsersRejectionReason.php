<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jul 2019 09:22:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersRejectionReason
 * 
 * @property int $id
 * @property int $user_id
 * @property int $rejection_reason
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsersRejectionReason extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'user_id' => 'int',
        'rejection_reason' => 'int'
    ];
    protected $fillable = [
        'user_id',
        'rejection_reason'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

}
