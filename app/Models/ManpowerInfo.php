<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jul 2019 05:52:56 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ManpowerInfo
 * 
 * @property int $id
 * @property int $user_id
 * @property int $major_id
 * @property string $sub_major
 * @property int $points
 * @property int $user_level_id
 * @property string $skype_account_id
 * @property string $profile_picture
 * @property string $certificate_url
 * @property string $national_id_url
 * @property array $certificates
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Major $major
 * @property \App\Models\UserLevel $user_level
 *
 * @package App\Models
 */
class ManpowerInfo extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'manpower_info';
    protected $casts = [
        'user_id' => 'int',
        'major_id' => 'int',
        'points' => 'int',
        'user_level_id' => 'int',
        'certificates' => 'json'
    ];
    protected $fillable = [
        'user_id',
        'major_id',
        'sub_major',
        'points',
        'user_level_id',
        'skype_account_id',
        'profile_picture',
        'certificate_url',
        'national_id_url',
        'certificates'
    ];

    protected $visible = [
        'id',
        'user_id',
        'major_id',
        'sub_major',
        'points',
        'user_level_id',
        'skype_account_id',
        'profile_picture',
        'certificate_url',
        'national_id_url',
        'certificates'
    ];
    
    
    public function major()
    {
        return $this->belongsTo(\App\Models\Major::class);
    }

    public function user_level()
    {
        return $this->belongsTo(\App\Models\UserLevel::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

//     public function setImgUriAttribute($value)
//         // setCertificateUrlAttribute
//     {
//         $attribute_name = "img_uri";
////         $attribute_name = "certificate_url";
//         $disk = "uploads";
//         // if a base64 was sent, store it in the db
//         if (starts_with($value, 'data:image')) {
//             // 0. Make the image
//             $image = \Image::make($value);
//             $randomNumber = mt_rand(1000, 9000);
//             // 1. Generate a filename.
//             $filename = "questions-images/" . $this->lesson->title . "-" . $randomNumber . '.jpg';
//
//             $image->filename = $this->lesson->title . "-" . $randomNumber . '.jpg';
//             // 2. Store the image on disk.
//             AWSHelper::upload($filename,(string) $image->stream(), 'public');
//             // 3. Save the path to the database
//             $this->attributes[$attribute_name]  = AWSHelper::getUrl($filename);
//         }
//     }

}
