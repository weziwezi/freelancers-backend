<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Jul 2019 08:32:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QuestionsGroup
 * 
 * @property int $id
 * @property int $question_id
 * @property int $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Group $group
 * @property \App\Models\Question $question
 *
 * @package App\Models
 */
class QuestionsGroup extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'question_id' => 'int',
        'group_id' => 'int'
    ];
    protected $fillable = [
        'question_id',
        'group_id'
    ];

    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class);
    }

    public function question()
    {
        return $this->belongsTo(\App\Models\Question::class);
    }
    
}
