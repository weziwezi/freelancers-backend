<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model 
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'code',
    ];

    protected $visible = [
        'id',
        'name',
        'code',
    ];

    /**
     * get all Countries with <b>ID</b> and <b>Name</b> fields
     * @return array
     */
    public static function getCountriesForDropDownList()
    {
        return self::all(['id', 'name']);
    }
}
