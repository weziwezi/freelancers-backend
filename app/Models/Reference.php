<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 04 Aug 2019 07:32:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Reference
 * 
 * @property int $id
 * @property string $url
 * @property int $subgroup_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Subgroup $subgroup
 *
 * @package App\Models
 */
class Reference extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'subgroup_id' => 'int'
	];

	protected $fillable = [
		'url',
		'subgroup_id'
	];

	public function subgroup()
	{
		return $this->belongsTo(\App\Models\Subgroup::class);
	}
}
