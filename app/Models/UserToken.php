<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 29 Aug 2019 13:25:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserToken
 * 
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserToken extends Eloquent
{

    protected $table = 'user_token';
    protected $casts = [
        'user_id' => 'int'
    ];
    protected $hidden = [
        'token'
    ];
    protected $fillable = [
        'type',
        'user_id',
        'token'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

}
