<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 21 Jul 2019 07:17:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PaymentMethod
 * 
 * @property int $id
 * @property string $name
 * @property bool $is_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users_payment_infos
 *
 * @package App\Models
 */
class PaymentMethod extends Eloquent
{

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'is_active' => 'bool'
    ];
    protected $fillable = [
        'name',
        'is_active'
    ];
    protected $appends = ['resource_url'];

    public function users_payment_infos()
    {
        return $this->hasMany(\App\Models\UsersPaymentInfo::class);
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/payment-methods/' . $this->getKey());
    }

}
