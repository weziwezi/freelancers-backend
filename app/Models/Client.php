<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\{
    Penalty,
    Subgroup
};

class Client extends Model
{

    use SoftDeletes;

    protected $fillable = [
        "name",
        "post_article_api_url",
        "get_main_categories_api_url",
        "get_sub_categories_api_url",
        "website_url",
        "is_active",
    ];
    protected $hidden = [
    ];
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    protected $appends = ['resource_url'];

    /*     * *********************** ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/manage_clients/' . $this->getKey());
    }

    /**
     * get all clients with <b>ID</b> and <b>Name</b> fields
     * @return array
     */
    public static function getAllClientsForDroupDownList()
    {
        return self::all(['id', 'name']);
    }

    public function penalties()
    {
        return $this->belongsToMany(Penalty::class, 'clients_penalties', 'client_id', 'penalty_id')
                        ->withPivot('id', 'deleted_at')
                        ->withTimestamps();
    }

    public function subgroups()
    {
        return $this->hasMany(Subgroup::class, 'client_id');
    }

}
