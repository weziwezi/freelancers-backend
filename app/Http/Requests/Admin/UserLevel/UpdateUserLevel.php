<?php

namespace App\Http\Requests\Admin\UserLevel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Constants\UserType;

class UpdateUserLevel extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.user-level.edit', $this->userLevel);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'string'],
            'minimum_points' => ['sometimes', 'integer'],
            'bonus_factor' => ['sometimes', 'numeric'],
            'maximum_number_of_reservations' => ['sometimes', 'numeric'],
            'user_type' => ['sometimes', 'integer', 'in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower'))],
            'is_default' => 'sometimes|boolean',
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
