<?php

namespace App\Http\Requests\Admin\UserLevel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Constants\UserType;

class StoreUserLevel extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.user-level.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'minimum_points' => ['required', 'integer'],
            'bonus_factor' => ['required', 'numeric'],
            'maximum_number_of_reservations' => ['required', 'numeric'],
            'user_type' => ['required', 'integer', 'in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower'))],
            'is_default' => 'sometimes|boolean',
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
