<?php

namespace App\Http\Requests\Admin\Reward;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Constants\UserType;

class StoreReward extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.reward.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'points' => ['required', 'numeric'],
            'user_type' => ['required', 'integer', 'in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower'))],
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
