<?php

namespace App\Http\Requests\Admin\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateClient extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.client.edit', $this->client);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'string'],
            'post_article_api_url' => ['sometimes', 'string', Rule::unique('clients', 'post_article_api_url')->whereNull('deleted_at')->ignore($this->client->id)],
            'get_main_categories_api_url' => ['sometimes', 'string', Rule::unique('clients', 'get_main_categories_api_url')->whereNull('deleted_at')->ignore($this->client->id)],
            'get_sub_categories_api_url' => ['sometimes', 'string', Rule::unique('clients', 'get_sub_categories_api_url')->whereNull('deleted_at')->ignore($this->client->id)],
            'website_url' => ['nullable', 'string'],
            'is_active' => ['sometimes', 'boolean'],
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
