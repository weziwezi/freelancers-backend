<?php

namespace App\Http\Requests\Admin\Client;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreClient extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.client.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'post_article_api_url' => ['required', 'string', 'url', Rule::unique('clients', 'post_article_api_url')->whereNull('deleted_at')],
            'get_main_categories_api_url' => ['required', 'string', 'url', Rule::unique('clients', 'get_main_categories_api_url')->whereNull('deleted_at')],
            'get_sub_categories_api_url' => ['required', 'string', 'url', Rule::unique('clients', 'get_sub_categories_api_url')->whereNull('deleted_at')],
            'website_url' => ['nullable', 'string', 'url'],
            'is_active' => ['required', 'boolean'],
        ];
    }

    public function messages(): array
    {
        parent::messages();
        return [
            'post_article_api_url.required' => _('The Post Article API URL field is required.'),
            'get_main_categories_api_url.required' => _('The Main Caregories API URL field is required.'),
            'get_sub_categories_api_url.required' => _('The Sub Categories API URL field is required.'),
            'website_url.required' => _('The Website URL field is required.'),
            'website_url.is_active' => _('Status is required.'),
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
