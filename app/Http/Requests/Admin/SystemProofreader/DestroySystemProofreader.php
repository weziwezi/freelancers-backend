<?php namespace App\Http\Requests\Admin\SystemProofreader;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class DestroySystemProofreader extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.system-proofreader.delete', $this->systemProofreader);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }
}
