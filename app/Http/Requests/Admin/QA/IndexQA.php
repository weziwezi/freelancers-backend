<?php

namespace App\Http\Requests\Admin\QA;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexQA extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.q-a.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'orderBy' => 'in:id,username,email,first_name,second_name,third_name,last_name,mobile_number,birthday,gender,country_id,user_type,user_status,client_id|nullable',
            'orderDirection' => 'in:asc,desc|nullable',
            'search' => 'string|nullable',
            'page' => 'integer|nullable',
            'per_page' => 'integer|nullable',
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
