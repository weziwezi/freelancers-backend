<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreUser extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.user.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'username' => ['required', Rule::unique('users', 'username'), 'string'],
            'email' => ['required', 'email', Rule::unique('users', 'email'), 'string'],
            'password' => ['required', 'confirmed', 'min:7', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'user_status' => ['required', 'boolean'],
            'client_id' => ['required', 'integer'],
        ];
    }

    public function messages(): array
    {
        parent::messages();
        return [
            'client_id.unique' => _('This Website already taken'),
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
