<?php

namespace App\Http\Requests\Admin\Question;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateQuestion extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.question.edit', $this->question);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'header' => ['sometimes', 'string'],
            'body' => ['nullable', 'string'],
            'question_type_id' => ['sometimes', 'integer'],
            'group_id' => ['sometimes']
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
