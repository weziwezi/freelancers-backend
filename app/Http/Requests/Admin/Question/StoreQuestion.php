<?php

namespace App\Http\Requests\Admin\Question;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreQuestion extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.question.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'header' => ['required', 'string'],
            'body' => ['nullable', 'string'],
            'question_type_id' => ['required', 'integer', 'exists:question_types,id'],
            'group_id' => ['required']
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
