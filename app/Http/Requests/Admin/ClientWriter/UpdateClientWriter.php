<?php

namespace App\Http\Requests\Admin\ClientWriter;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateClientWriter extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.clients-writers.edit', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes',
            'second_name' => 'sometimes',
            'third_name' => 'sometimes',
            'last_name' => 'sometimes',
            'password' => 'sometimes',
            'user_status' => 'sometimes',
            'email' => 'sometimes',
            'gender' => 'sometimes',
            'country_id' => 'sometimes',
            'groupsIds' => 'sometimes',
            'manpower_info.major_id' => 'sometimes',
            'manpower_info.sub_major' => 'sometimes',
            'manpower_info.skype_account_id' => 'sometimes',
        ];
    }

    public function messages(): array
    {
        parent::messages();
        return [
            'manpower_info.skype_account_id.required' => 'Skype Id is required',
            'manpower_info.major_id.required' => 'Major is required',
            'manpower_info.sub_major.required' => 'Sub Major is required'
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
