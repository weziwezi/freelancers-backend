<?php

namespace App\Http\Requests\Admin\QuestionType;

use App\Constants\UserType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreQuestionType extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.question-type.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            //'is_active' => ['required', 'boolean'],
            'user_type' => ['required', 'integer', 'in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower'))],
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
