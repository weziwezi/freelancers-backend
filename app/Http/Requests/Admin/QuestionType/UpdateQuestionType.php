<?php

namespace App\Http\Requests\Admin\QuestionType;

use App\Constants\UserType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateQuestionType extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.question-type.edit', $this->questionType);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'string'],
            'is_active' => ['sometimes', 'boolean'],
            'user_type' => ['sometimes', 'integer', 'in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower'))],
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
