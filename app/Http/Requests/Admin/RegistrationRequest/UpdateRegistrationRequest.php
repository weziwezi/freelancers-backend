<?php

namespace App\Http\Requests\Admin\RegistrationRequest;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use App\Constants\UserStatus;
use Illuminate\Validation\Rule;

class UpdateRegistrationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {

        return Gate::allows('admin.registration-request.edit', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'user_status' => ['required', 'integer', 'in:' . implode(',', [UserStatus::EXAM_IS_APPROVED, UserStatus::SHOULD_OPERATE, UserStatus::REJECTED])],
            'rejection_reason' => ['required_if:user_status,' . UserStatus::EXAM_IS_APPROVED]
        ];
    }

    public function messages()
    {
        return [
            'rejection_reason.required_if' => _('Please Select Reject Reson'),
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
