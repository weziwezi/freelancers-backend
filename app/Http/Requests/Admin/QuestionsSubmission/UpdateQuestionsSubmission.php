<?php

namespace App\Http\Requests\Admin\QuestionsSubmission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use App\Constants\UserStatus;
use Illuminate\Validation\Rule;

class UpdateQuestionsSubmission extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {

        return Gate::allows('admin.questions-submission.edit', $this->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'user_status' => ['required', 'integer', 'in:' . implode(',', [UserStatus::EXAM_IS_APPROVED, UserStatus::REJECTED])]
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
