<?php

namespace App\Http\Requests\Admin\QuestionsSubmission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexQuestionsSubmission extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.questions-submission.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
        'orderBy' => 'in:id,user_id,question_id|nullable',
        'orderDirection' => 'in:asc,desc|nullable',
        'search' => 'string|nullable',
        'user_type' => 'integer|nullable',
        'page' => 'integer|nullable',
        'per_page' => 'integer|nullable',
        ];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
