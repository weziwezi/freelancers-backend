<?php

namespace App\Http\Requests\Admin\QuestionsSubmission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class DestroyQuestionsSubmission extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.questions-submission.delete', $this->questionsSubmission);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [];
    }

    protected function failedAuthorization()
    {
        abort(404);
    }

}
