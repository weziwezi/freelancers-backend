<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateSubgroupAndReferencesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => [
                'required',
                Rule::unique('subgroups')->where('group_id', $this->group_id)
            ],
            'group_id' => 'required|exists:groups,id,is_active,1,deleted_at,NULL',
            'references' => 'sometimes|array',
                #'references.*' => ''
        ];
    }

    public function messages(): array
    {
        return [
                #'references.*.url' => 'Refrence number (:attribute:i) must be URL',
        ];
    }

}
