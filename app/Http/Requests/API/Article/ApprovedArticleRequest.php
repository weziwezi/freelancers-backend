<?php

namespace App\Http\Requests\API\Article;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\ArticleStatus;

class ApprovedArticleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'articlesIds' => 'required|array',
            'articlesIds.*' => 'required|integer|exists:articles,id,deleted_at,NULL,article_status,' . ArticleStatus::NOT_READY_FOR_RESERVATION,
        ];
    }

}
