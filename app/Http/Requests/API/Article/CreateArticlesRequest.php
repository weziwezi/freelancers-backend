<?php

namespace App\Http\Requests\API\Article;

use Illuminate\Foundation\Http\FormRequest;


class CreateArticlesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website_subgroup' => 'required|integer|exists:subgroups,id,client_id,' . auth()->user()->client_id,
            'main_category' => 'required|integer',
            'sub_category' => 'required|integer',
            'article_level' => 'required|exists:article_levels,id,user_id,' . auth()->user()->id,
            'display_for' => 'required|integer|in:1,2,3',
            'titles' => 'required|string|min:5'
        ];
    }

}
