<?php

namespace App\Http\Requests\API\Article;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Constants\ReferencingType;
use App\Rules\Article\{
    ClientUniqueArticleTitleRole,
    ClientUniqueArticleReferenceRole
};

class UpdateArticleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website_subgroup' => 'sometimes|integer|exists:subgroups,id,client_id,' . auth()->user()->client_id,
            'main_category' => 'sometimes|integer',
            'sub_category' => 'sometimes|integer',
            'article_level' => 'sometimes|exists:article_levels,id,user_id,' . auth()->user()->id,
            'display_for' => 'sometimes|integer|in:1,2,3',
            'title' => ['sometimes', 'string', 'min:5', 'max:255', new ClientUniqueArticleTitleRole],
            'conditions' => 'sometimes|nullable|string|min:10|max:255',
            'key_word' => 'sometimes|nullable|string|min:10|max:255',
            'referencing_type' => 'required_with:articles_references|integer|in:' . implode(',', ReferencingType::getReferencingTypeId()),
            'articles_references' => 'array|required_if:referencing_type,' . implode(',', ReferencingType::getReferencingTypeIdThatNeedRefereces()),
            'articles_references.*' => [
                'integer',
                'required_if:referencing_type,' . implode(',', ReferencingType::getReferencingTypeIdThatNeedRefereces()),
                new ClientUniqueArticleReferenceRole
            ],
        ];
    }

}
