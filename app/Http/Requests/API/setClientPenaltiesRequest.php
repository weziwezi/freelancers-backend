<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\UserType;

class setClientPenaltiesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type' => 'required|in:' . implode(',', UserType::getUserTypesIdByRoleName('manpower')),
            'penalty_id' => 'array',
            'penalty_id.*' => 'integer|nullable|exists:penalties,id,user_type,' . $this->user_type,
        ];
    }

}
