<?php

namespace App\Http\Requests\API\ArticleLevel;

use Illuminate\Foundation\Http\FormRequest;

class SearchArticleLevelRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_type' => 'sometimes|integer',
            'subgroups' => 'sometimes|array',
            'subgroups.*' => 'sometimes|integer'
        ];
    }

}
