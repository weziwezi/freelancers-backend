<?php

namespace App\Http\Requests\API\ArticleLevel;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\{
    ArticleType,
    UserType
};
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Models\UserLevel;

class CreateArticleLevelRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // check if the user is Website Manager
        if (Auth::user()->user_type && Auth::user()->user_type == UserType::WEBSITE_MANAGER) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:200', Rule::unique('article_levels')->where('client_id', Auth::user()->client_id)],
            'number_of_words' => 'required|integer|max:2147483647|min:0',
            'number_of_references' => 'required|integer|max:2147483647|min:0',
            'number_of_sub_headlines' => 'required|integer|max:2147483647|min:0',
            #'number_of_internal_links' => 'required|integer|max:2147483647|min:0', calculate from backend
            'writing_delivery_time' => 'required|integer|max:2147483647|min:0',
            'language_proofreading_delivery_time' => 'required|integer|max:2147483647|min:0',
            'content_proofreading_delivery_time' => 'required|integer|max:2147483647|min:0',
            'proofreading_delivery_time' => 'required|integer|max:2147483647|min:0',
            'images_uploading_delivery_time' => 'required|integer|max:2147483647|min:0',
            'writing_price' => 'required|numeric|max:999999.99|min:0',
            'language_proofreading_price' => 'required|numeric|max:999999.99|min:0',
            'content_proofreading_price' => 'required|numeric|max:999999.99|min:0',
            'proofreading_price' => 'required|numeric|max:999999.99|min:0',
            'images_uploading_price' => 'required|numeric|max:999999.99|min:0',
            'article_type' => 'required|in:' . implode(',', ArticleType::getArticleTypesIds()),
            'subgroups' => 'required|array|min:1',
            'subgroups.*' => 'required|integer|exists:subgroups,id,client_id,' . Auth()->user()->client_id,
            'user_levels' => ['required', 'array', function($attribute, $value, $fail) {

                    // convert json to array
                    //$value = json_decode($value, true);
                    // check if the user level indexes same as userType
                    $userTypes = array_keys($value);
                    $manpower = UserType::getUserTypesIdByRoleName('manpower');
                    sort($userTypes);
                    sort($manpower);
                    if ($manpower != $userTypes) {
                        return $fail($attribute . ' is invalid.');
                    }

                    // check if the values in user_levels is correct
                    foreach ($value as $userType => $userLevels) {
                        $userLevels = array_unique($userLevels);
                        $foundUserLevels = UserLevel::where('user_type', $userType)->whereIn('id', $userLevels)->get();
                        if (!$foundUserLevels->count() || $foundUserLevels->count() != count($userLevels)) {
                            return $fail($attribute . ' is invalid.');
                        }
                    };
                }]
        ];
    }

}
