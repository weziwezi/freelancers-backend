<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\{
    PasswordRule,
    SkypeAccountRule
};
use App\Constants\{
    UserType,
    UserGender
};

class CreateClientManpowerRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getPrefix()) {
            case 'api/v1/users/proofreaders':
                $userType = implode(',', UserType::getProofreadersUserTypesIds());
                break;
            case 'api/v1/users/writers':
                $userType = UserType::WRITER;
                break;
            default :
                $userType = implode(',', UserType::getManpowerUserTypesIds());
        }

        return [
            'first_name' => 'required|string',
            'second_name' => 'required|string',
            'third_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string|unique:users,username|min:6',
            'email' => 'required|email|unique:users,email|',
            'password' => ['required', new PasswordRule],
            'country_code_id' => 'required|integer|exists:countries,id',
            'mobile_number' => 'required|string|min:5',
            'birthday' => 'required|date_format:Y-m-d',
            'gender' => 'required|integer|in:' . implode(',', UserGender::getAllUserGendersIds()),
            'country_id' => 'required|integer|exists:countries,id',
            'major_id' => 'required|integer|exists:majors,id',
            'sub_major' => 'required|string',
            'subgroups_ids' => 'required|array',
            'subgroups_ids.*' => 'required|exists:subgroups,id',
            'user_type' => 'required|integer|in:' . implode(',', UserType::getManpowerUserTypesIds()),
            'user_level_id' => 'required|integer|exists:user_levels,id',
            'skype_account_id' => ['required_if:user_type,==,4,5,6', new SkypeAccountRule, 'unique:manpower_info,skype_account_id'],
            'certificate' => 'sometimes|file|max:10000',
            'national_id' => 'sometimes|file|max:10000',
        ];
    }

}
