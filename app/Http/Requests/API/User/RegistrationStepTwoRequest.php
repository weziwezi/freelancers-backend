<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SkypeAccountRule;
use App\Constants\{
    UserStatus,
    UserType
};

class RegistrationStepTwoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth()->user();
        if ($user->user_status == UserStatus::MAJOR_INFO && in_array($user->user_type, UserType::getUserTypesCanRegisterIds()))
            return true;

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'major_id' => 'required|integer|exists:majors,id',
            'sub_major' => 'required|string|max:255',
            'country_code_id' => 'required|integer|exists:countries,id',
            'mobile_number' => 'required|string|min:5',
            'skype_account_id' => ['required', 'max:255', new SkypeAccountRule, 'unique:manpower_info,skype_account_id'],
            'groups_ids' => 'required|array',
            'groups_ids.*' => 'required|exists:groups,id',
        ];
    }

}
