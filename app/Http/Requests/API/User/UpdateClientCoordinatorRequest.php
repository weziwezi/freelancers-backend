<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use App\Rules\PasswordRule;

class UpdateClientCoordinatorRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes|string|max:255',
            'last_name' => 'sometimes|string|max:255',
            'username' => 'sometimes|string|min:6|max:255|unique:users,username,' . $this->userId . ',id',
            'email' => 'sometimes|email|max:255|unique:users,email,' . $this->userId . ',id',
            'password' => ['sometimes', new PasswordRule],
            'status' => 'sometimes|boolean'
        ];
    }

}
