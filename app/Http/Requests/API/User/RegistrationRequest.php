<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PasswordRule;
use App\Constants\{
    UserGender,
    UserType
};

class RegistrationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'third_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users,username',
            'email' => 'required|string|max:255|email|unique:users,email',
            'password' => ['required', new PasswordRule],
            'country_id' => 'required|integer|exists:countries,id',
            'gender' => 'required|integer|in:' . implode(',', UserGender::getAllUserGendersIds()),
            'user_type' => 'required|integer|in:' . implode(',', UserType::getUserTypesCanRegisterIds())
        ];
    }

}
