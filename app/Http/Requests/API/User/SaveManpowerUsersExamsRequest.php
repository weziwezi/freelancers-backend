<?php

namespace App\Http\Requests\API\User;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Constants\{
    UserType,
    UserStatus
};

class SaveManpowerUsersExamsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth()->user();
        if ($user->user_status == UserStatus::EXAM_IS_NOT_PERFORMED && in_array($user->user_type, UserType::getUserTypesCanRegisterIds()))
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array',
            'ids.*' => ['required', 'integer', Rule::exists('questions', 'id')->where(function($query) {
                            $query->whereIn('question_type_id', function($query) {
                                        $query->select('question_types.id')->from('question_types')->where(['is_active' => true, 'user_type' => Auth()->user()->user_type]);
                                    });
                        })
            ],
            'answers' => 'required|array|size:' . count(request()->ids),
            'answers.*' => 'required|string'
        ];
    }

}
