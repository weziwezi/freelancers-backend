<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\API\User\GetClientManpowerRequest;
use App\Constants\UserType;

class GetWritersRequest extends GetClientManpowerRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'page' => 'sometimes|integer',
            'per_page' => 'sometimes|integer',
            'user_type' => 'sometimes|integer|in:' . UserType::WRITER,
        ]);
    }

}
