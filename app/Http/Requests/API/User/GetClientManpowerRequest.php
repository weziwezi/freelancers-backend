<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\UserType;

class GetClientManpowerRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes|string',
            'second_name' => 'sometimes|string',
            'third_name' => 'sometimes|string',
            'last_name' => 'sometimes|string',
            'username' => 'sometimes|string',
            'email' => 'sometimes|email',
            'user_type' => 'sometimes|integer|in:' . implode(',', UserType::getManpowerUserTypesIds()),
                /* 'gender' => 'required|integer|in:' . implode(',', UserGender::getAllUserGendersIds()),
                  'country_id' => 'required|integer|exists:countries,id',
                  'major_id' => 'required|integer|exists:majors,id',
                  'sub_major' => 'required|string',
                  'subgroups_ids' => 'required|array',
                  'subgroups_ids.*' => 'required|exists:subgroups,id',
                  'user_type' => 'required|integer|in:' . $userType,
                  'user_level_id' => 'required|integer|exists:user_levels,id',
                  'skype_account_id' => 'required_if:user_type,==,4,5,6|string|unique:manpower_info,skype_account_id,' . $this->userId . ',user_id',
                  'certificate' => 'sometimes|file|max:10000',
                  'national_id' => 'sometimes|file|max:10000', */
        ];
    }

}
