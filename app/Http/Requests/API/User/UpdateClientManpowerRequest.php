<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\{
    PasswordRule,
    SkypeAccountRule
};
use App\Constants\{
    UserGender,
    UserType
};

class UpdateClientManpowerRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->route()->getPrefix()) {
            case 'api/v1/users/proofreaders':
                $userType = implode(',', UserType::getProofreadersUserTypesIds());
                break;
            case 'api/v1/users/writers':
                $userType = UserType::WRITER;
                break;
            default :
                $userType = implode(',', UserType::getManpowerUserTypesIds());
        }
        return [
            'first_name' => 'sometimes|string',
            'second_name' => 'sometimes|string',
            'third_name' => 'sometimes|string',
            'last_name' => 'sometimes|string',
            'username' => 'sometimes|string|min:6|unique:users,username,' . $this->userId . ',id',
            'email' => 'sometimes|email|unique:users,email,' . $this->userId . ',id',
            'password' => ['sometimes', new PasswordRule],
            'country_code_id' => 'sometimes|integer|exists:countries,id',
            'mobile_number' => 'sometimes|string|min:5',
            'birthday' => 'sometimes|date_format:Y-m-d',
            'gender' => 'sometimes|integer|in:' . implode(',', UserGender::getAllUserGendersIds()),
            'country_id' => 'sometimes|integer|exists:countries,id',
            'major_id' => 'sometimes|integer|exists:majors,id',
            'sub_major' => 'sometimes|string',
            'subgroups_ids' => 'sometimes|array',
            'subgroups_ids.*' => 'sometimes|exists:subgroups,id',
            'user_type' => 'sometimes|integer|in:' . $userType,
            'user_level_id' => 'sometimes|integer|exists:user_levels,id',
            'skype_account_id' => ['sometimes:user_type,==,4,5,6', new SkypeAccountRule, 'unique:manpower_info,skype_account_id,' . $this->userId . ',user_id'],
            'certificate' => 'sometimes|file|max:10000',
            'national_id' => 'sometimes|file|max:10000',
            'status' => 'sometimes|boolean',
            'groups_ids' => 'sometimes|array',
            'groups_ids.*' => 'sometimes|exists:groups,id',
        ];
    }

}
