<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Rules\PasswordRule;

class UpdateCurrentUserProfileRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'sometimes|string|min:3|max:255',
            'lastname' => 'sometimes|string|min:3|max:255',
            'email' => ['sometimes', 'email', Rule::unique('users')->ignore(Auth()->user()->id)],
            'mobile_number' => 'sometimes|string|min:10|max:20',
            'current_password' => 'required_with:new_password',
            'new_password' => ['required_with:current_password', 'confirmed', new PasswordRule]
        ];
    }

}
