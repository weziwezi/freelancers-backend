<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\{
    UserType,
    UserStatus
};

class GetManpowerUsersExamsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth()->user();
        if ($user->user_status == UserStatus::EXAM_IS_NOT_PERFORMED && in_array($user->user_type, UserType::getUserTypesCanRegisterIds()))
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                //
        ];
    }

}
