<?php

namespace App\Http\Requests\API\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SkypeAccountRule;
use App\Constants\{
    UserStatus,
    UserType
};

class RegistrationStepFourRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth()->user();
        if ($user->user_status == UserStatus::EXAM_IS_APPROVED && in_array($user->user_type, UserType::getUserTypesCanRegisterIds()))
            return true;

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'certificate' => 'required|file|max:10000',
            'national_id' => 'required|file|max:10000',
        ];
    }

}
