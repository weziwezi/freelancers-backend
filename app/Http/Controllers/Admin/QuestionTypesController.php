<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\QuestionType\IndexQuestionType;
use App\Http\Requests\Admin\QuestionType\StoreQuestionType;
use App\Http\Requests\Admin\QuestionType\UpdateQuestionType;
use App\Http\Requests\Admin\QuestionType\DestroyQuestionType;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\QuestionType;
use App\Constants\UserType;

class QuestionTypesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexQuestionType $request
     * @return Response|array
     */
    public function index(IndexQuestionType $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(QuestionType::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'name', 'is_active', 'user_type'],
                // set columns to searchIn
                ['id', 'name'],
                function ($query) {
            if ($userType = request()->input('user_type')) {
                $query->where('user_type' , $userType);
            }
        });

        array_map(function($questionType) {
            //dd($questionType->questions);
            $questionType->user_type_name = UserType::getUserTypeNameById($questionType->user_type);
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }

        $userTypes = UserType::getUserTypesCanRegister();
        return view('admin.question-type.index', ['data' => $data, 'userTypes' => $userTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.question-type.create');

        return view('admin.question-type.create', [
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreQuestionType $request
     * @return Response|array
     */
    public function store(StoreQuestionType $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        //dd($sanitized);
        // Store the QuestionType
        $questionType = QuestionType::create($sanitized);


        if ($request->ajax()) {
            return ['redirect' => url('admin/question-types'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/question-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  QuestionType $questionType
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(QuestionType $questionType)
    {
        $this->authorize('admin.question-type.show', $questionType);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  QuestionType $questionType
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(QuestionType $questionType)
    {
        $this->authorize('admin.question-type.edit', $questionType);

        return view('admin.question-type.edit', [
            'questionType' => $questionType,
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateQuestionType $request
     * @param  QuestionType $questionType
     * @return Response|array
     */
    public function update(UpdateQuestionType $request, QuestionType $questionType)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values QuestionType
        $questionType->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/question-types'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/question-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyQuestionType $request
     * @param  QuestionType $questionType
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyQuestionType $request, QuestionType $questionType)
    {
        $questionType->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
