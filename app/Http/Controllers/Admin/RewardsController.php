<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Reward\IndexReward;
use App\Http\Requests\Admin\Reward\StoreReward;
use App\Http\Requests\Admin\Reward\UpdateReward;
use App\Http\Requests\Admin\Reward\DestroyReward;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\Reward;
use App\Constants\UserType;

class RewardsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexReward $request
     * @return Response|array
     */
    public function index(IndexReward $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Reward::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'name', 'points', 'user_type'],
                // set columns to searchIn
                ['id', 'name']
        );

        array_map(function($userLevel) {
            $userLevel->user_type_name = UserType::getUserTypeNameById($userLevel->user_type);
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.reward.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.reward.create');

        return view('admin.reward.create', [
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreReward $request
     * @return Response|array
     */
    public function store(StoreReward $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Reward
        $reward = Reward::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/rewards'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/rewards');
    }

    /**
     * Display the specified resource.
     *
     * @param  Reward $reward
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Reward $reward)
    {
        $this->authorize('admin.reward.show', $reward);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Reward $reward
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Reward $reward)
    {
        $this->authorize('admin.reward.edit', $reward);

        return view('admin.reward.edit', [
            'reward' => $reward,
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateReward $request
     * @param  Reward $reward
     * @return Response|array
     */
    public function update(UpdateReward $request, Reward $reward)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Reward
        $reward->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/rewards'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/rewards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyReward $request
     * @param  Reward $reward
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyReward $request, Reward $reward)
    {
        $reward->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
