<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Penalty\IndexPenalty;
use App\Http\Requests\Admin\Penalty\StorePenalty;
use App\Http\Requests\Admin\Penalty\UpdatePenalty;
use App\Http\Requests\Admin\Penalty\DestroyPenalty;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\Penalty;
use App\Constants\UserType;

class PenaltiesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexPenalty $request
     * @return Response|array
     */
    public function index(IndexPenalty $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Penalty::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'name', 'points', 'user_type'],
                // set columns to searchIn
                ['id', 'name']
        );

        array_map(function($userLevel) {
            $userLevel->user_type_name = UserType::getUserTypeNameById($userLevel->user_type);
            $userLevel->makeVisible('resource_url');
            $userLevel->makeVisible('user_type_name');
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.penalty.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.penalty.create');

        return view('admin.penalty.create', [
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePenalty $request
     * @return Response|array
     */
    public function store(StorePenalty $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Penalty
        $penalty = Penalty::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/penalties'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/penalties');
    }

    /**
     * Display the specified resource.
     *
     * @param  Penalty $penalty
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Penalty $penalty)
    {
        $this->authorize('admin.penalty.show', $penalty);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Penalty $penalty
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Penalty $penalty)
    {
        $this->authorize('admin.penalty.edit', $penalty);

        return view('admin.penalty.edit', [
            'penalty' => $penalty,
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePenalty $request
     * @param  Penalty $penalty
     * @return Response|array
     */
    public function update(UpdatePenalty $request, Penalty $penalty)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Penalty
        $penalty->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/penalties'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/penalties');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyPenalty $request
     * @param  Penalty $penalty
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyPenalty $request, Penalty $penalty)
    {
        $penalty->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
