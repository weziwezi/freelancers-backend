<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\UserLevel\IndexUserLevel;
use App\Http\Requests\Admin\UserLevel\StoreUserLevel;
use App\Http\Requests\Admin\UserLevel\UpdateUserLevel;
use App\Http\Requests\Admin\UserLevel\DestroyUserLevel;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\UserLevel;
use App\Constants\UserType;

class UserLevelsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexUserLevel $request
     * @return Response|array
     */
    public function index(IndexUserLevel $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(UserLevel::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'name', 'minimum_points', 'bonus_factor', 'maximum_number_of_reservations', 'user_type'],
                // set columns to searchIn
                ['id', 'name']
        );

        array_map(function($userLevel) {
            $userLevel->user_type_name = UserType::getUserTypeNameById($userLevel->user_type);
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.user-level.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.user-level.create');

        return view('admin.user-level.create', [
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserLevel $request
     * @return Response|array
     */
    public function store(StoreUserLevel $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the UserLevel
        $userLevel = UserLevel::create($sanitized);
        if ($userLevel) {
            if (!empty($sanitized['is_default'])) {
                $userLevel->where('user_type', $userLevel->user_type)->where('id', '!=', $userLevel->id)->update(['is_default' => false]);
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/user-levels'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/user-levels');
    }

    /**
     * Display the specified resource.
     *
     * @param  UserLevel $userLevel
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(UserLevel $userLevel)
    {
        $this->authorize('admin.user-level.show', $userLevel);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  UserLevel $userLevel
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(UserLevel $userLevel)
    {
        $this->authorize('admin.user-level.edit', $userLevel);

        return view('admin.user-level.edit', [
            'userLevel' => $userLevel,
            'userTypes' => UserType::getManpowerUserTypes()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUserLevel $request
     * @param  UserLevel $userLevel
     * @return Response|array
     */
    public function update(UpdateUserLevel $request, UserLevel $userLevel)
    {
        // Sanitize input
        $sanitized = $request->validated();
        // Check if there is default level

        if (!empty($sanitized['is_default'])) {
            $userLevel->where('user_type', $userLevel->user_type)->update(['is_default' => false]);
        }

        // Update changed values UserLevel
        $userLevel->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/user-levels'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/user-levels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyUserLevel $request
     * @param  UserLevel $userLevel
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyUserLevel $request, UserLevel $userLevel)
    {
        if (!empty($userLevel->is_default))
            return response(['message' => trans('brackets/admin-ui::admin.operation.IsdefaultValue')], 422);

        $userLevel->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
