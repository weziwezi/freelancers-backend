<?php

namespace App\Http\Controllers\Admin;

use App\Constants\UserStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\DestroyUser;
use App\Http\Requests\Admin\User\IndexUser;
use App\Http\Requests\Admin\User\StoreUser;
use App\Http\Requests\Admin\User\UpdateUser;
use App\Models\Client;
use App\Models\User;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Http\Response;

class UsersController extends Controller
{

    private function getUserAppends()
    {
        return array('resource_url', 'questions_submissions_url', 'registration_request_url', 'gender_name');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  IndexUser $request
     * @return Response|array
     */
    public function index(IndexUser $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'username', 'email', 'first_name', 'last_name', 'user_status', 'client_id'],
                // set columns to searchIn
                ['id', 'username', 'email', 'first_name', 'last_name']
        );

        array_map(function(User $user) {
            $user->setAppends($this->getUserAppends());
            $user->user_status = (UserStatus::ACTIVE == $user->user_status);
            $user->client_name = $user->client['name'];
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }
        return view('admin.user.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.user.create');

        return view('admin.user.create', [
            'clients' => Client::getAllClientsForDroupDownList()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUser $request
     * @return Response|array
     */
    public function store(StoreUser $request)
    {
        // Sanitize input
        $sanitized = $request->validated();


        $sanitized = $this->beforeSetInDatabase($sanitized);

        // Store the User
        $user = User::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/managers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/managers');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $user->setAppends($this->getUserAppends());
        $this->authorize('admin.user.show', $user);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('admin.user.edit', $user);
        $user->setAppends($this->getUserAppends());
        return view('admin.user.edit', [
            'user' => $this->beforeRenderToEditView($user),
            'clients' => Client::getAllClientsForDroupDownList()
        ]);
    }

    /**
     * Process data before render in view form
     * @param User $user
     * @return User
     */
    private function beforeRenderToEditView(User $user)
    {
        if ($user->user_status == UserStatus::ACTIVE)
            $user->user_status = 1;
        else
            $user->user_status = 0;
        return $user;
    }

    /**
     * Edit data before set in database
     * @param array $sanitized
     * @return array
     */
    private function beforeSetInDatabase($sanitized)
    {
        $sanitized['user_type'] = 1;
        $sanitized['user_status'] = ($sanitized['user_status'] ? UserStatus::ACTIVE : UserStatus::BLOCKED);
        return $sanitized;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUser $request
     * @param  User $user
     * @return Response|array
     */
    public function update(UpdateUser $request, User $user)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized = $this->beforeSetInDatabase($sanitized);
        // Update changed values User
        $user->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/managers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/managers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyUser $request
     * @param  User $user
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyUser $request, User $user)
    {
        $user->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
