<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\RegistrationRequest\{
    IndexRegistrationRequest,
    StoreRegistrationRequest,
    UpdateRegistrationRequest,
    DestroyRegistrationRequest
};
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\User;
use App\Constants\{
    UserStatus,
    RejectionReason,
    UserType
};

class RegistrationRequestController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexUser $request
     * @return Response|array
     */
    public function index(IndexRegistrationRequest $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'username', 'email', 'first_name', 'second_name', 'third_name', 'last_name', 'mobile_number', 'birthday', 'gender', 'country_id', 'user_type', 'user_status', 'client_id'],
                // set columns to searchIn
                ['id', 'username', 'email', 'first_name', 'second_name', 'third_name', 'last_name', 'mobile_number', 'gender', 'user_type'],
                function($query) {
            if (!empty(request()->user_type)) {
                $query->where('users.user_type', request()->user_type);
            }
            $query->where('user_status', UserStatus::FILES_ARE_UPLOADED)->with('country')->with('groups')->with('manpower_info')->with('manpower_info.major');
        }
        );

        array_map(function($user) {
            $user->manpower_info->load('major');
            $user->manpower_info->makeVisible('major');
            $user->append('registration_request_url');
        }, $data->all());

        $userTypes = UserType::getUserTypesCanRegister();
        if ($request->ajax()) {
            return ['data' => $data, 'userTypes' => $userTypes];
        }

        return view('admin.registration-request.index', ['data' => $data, 'userTypes' => $userTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.user.create');

        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUser $request
     * @return Response|array
     */
    public function store(StoreUser $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the User
        $user = User::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/users'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('admin.user.show', $user);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('admin.registration-request.edit', $user);

        return view('admin.registration-request.edit', [
            'user' => $user,
            'status' => [
                'approve' => UserStatus::SHOULD_OPERATE,
                'reject' => UserStatus::REJECTED,
                'rejectWithReasons' => UserStatus::EXAM_IS_APPROVED
            ],
            'rejectResons' => RejectionReason::getRejectResonForRegistrationRequest()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUser $request
     * @param  User $user
     * @return Response|array
     */
    public function update(UpdateRegistrationRequest $request, User $user)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values User
        $user->update($sanitized);

        if ($sanitized['user_status'] == UserStatus::EXAM_IS_APPROVED) {
            // for send one insert Query
            $rejection_reason = array();
            foreach ($sanitized['rejection_reason'] as $key => $value) {
                $rejection_reason[] = ['rejection_reason' => $value];
            }

            // change status 
            if (count($rejection_reason)) {
                //$user->users_rejection_reasons()->createMany($rejection_reason);
                if (in_array(RejectionReason::WRONG_GROUP, $sanitized['rejection_reason'])) {
                    $user->user_status = UserStatus::EMAIL_VERIFICATION;
                    $user->save();
                } elseif (in_array(RejectionReason::UNI_CERTIFICATE, $sanitized['rejection_reason']) || in_array(RejectionReason::NATIONAL_ID, $sanitized['rejection_reason'])) {
                    $user->user_status = UserStatus::EXAM_IS_APPROVED;
                    $user->save();
                }
            }
        }
        if ($request->ajax()) {
            return ['redirect' => url('admin/registration-request'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/registration-request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyUser $request
     * @param  User $user
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyUser $request, User $user)
    {
        $user->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
