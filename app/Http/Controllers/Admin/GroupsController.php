<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Group\IndexGroup;
use App\Http\Requests\Admin\Group\StoreGroup;
use App\Http\Requests\Admin\Group\UpdateGroup;
use App\Http\Requests\Admin\Group\DestroyGroup;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\Group;

class GroupsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexGroup $request
     * @return Response|array
     */
    public function index(IndexGroup $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Group::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'is_active'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.group.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.group.create');

        return view('admin.group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreGroup $request
     * @return Response|array
     */
    public function store(StoreGroup $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Group
        $group = Group::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/groups'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/groups');
    }

    /**
     * Display the specified resource.
     *
     * @param  Group $group
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Group $group)
    {
        $this->authorize('admin.group.show', $group);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Group $group
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Group $group)
    {
        $this->authorize('admin.group.edit', $group);

        return view('admin.group.edit', [
            'group' => $group,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateGroup $request
     * @param  Group $group
     * @return Response|array
     */
    public function update(UpdateGroup $request, Group $group)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Group
        $group->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/groups'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyGroup $request
     * @param  Group $group
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyGroup $request, Group $group)
    {
        $group->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
