<?php

namespace App\Http\Controllers\Admin;

use App\Constants\UserStatus;
use App\Constants\UserType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\QuestionsSubmission\DestroyQuestionsSubmission;
use App\Http\Requests\Admin\QuestionsSubmission\IndexQuestionsSubmission;
use App\Http\Requests\Admin\QuestionsSubmission\StoreQuestionsSubmission;
use App\Http\Requests\Admin\QuestionsSubmission\UpdateQuestionsSubmission;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\{
    User,
    QuestionsSubmission
};

class QuestionsSubmissionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexQuestionsSubmission $request
     * @return Response|array
     */
    public function index(IndexQuestionsSubmission $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)
                ->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'users.first_name', 'users.second_name', 'users.third_name', 'users.last_name'],
                // set columns to searchIn
                ['id', 'users.first_name', 'users.second_name', 'users.third_name', 'users.last_name', 'users.user_type'],
                function ($query) {
            if (!empty(request()->user_type)) {
                $query->where('users.user_type', request()->user_type);
            }
            $query->where('users.user_status', UserStatus::EXAM_IS_PERFORMED);
            $query->with('groups');
        }
        );
        array_map(function($user) {
            $user->append('questions_submissions_url');
        }, $data->all());

        $userTypes = UserType::getUserTypesCanRegister();
        if ($request->ajax()) {
            return ['data' => $data, 'userTypes' => $userTypes];
        }

        return view('admin.questions-submission.index', ['data' => $data, 'userTypes' => $userTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.questions-submission.create');

        return view('admin.questions-submission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreQuestionsSubmission $request
     * @return Response|array
     */
    public function store(StoreQuestionsSubmission $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the QuestionsSubmission
        $questionsSubmission = QuestionsSubmission::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/submitted-exams'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/submitted-exams');
    }

    /**
     * Display the specified resource.
     *
     * @param  QuestionsSubmission $questionsSubmission
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(QuestionsSubmission $questionsSubmission)
    {
        $this->authorize('admin.questions-submission.show', $questionsSubmission);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  QuestionsSubmission $questionsSubmission
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {


        $user = $user->with(['questions_submissions' =>
                    function($query) {
                        $query->with(['question' => function ($query) {
                                /* $query->with(['groups' => function($query) {
                                  $query->where('is_active', true);
                                  $query->select('name');
                                  }]); */
                            }]);
                    }])->where('id', $user->id)->get()[0];

        $this->authorize('admin.questions-submission.edit', $user);


        return view('admin.questions-submission.edit', [
            'user' => $user,
            'status' => [
                'approve' => UserStatus::EXAM_IS_APPROVED,
                'reject' => UserStatus::REJECTED
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateQuestionsSubmission $request
     * @param  QuestionsSubmission $questionsSubmission
     * @return Response|array
     */
    public function update(UpdateQuestionsSubmission $request, User $user)
    {
        // Sanitize input
        $sanitized = $request->validated();
        // Update changed values QuestionsSubmission

        $user->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/submitted-exams'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/submitted-exams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyQuestionsSubmission $request
     * @param  QuestionsSubmission $questionsSubmission
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyQuestionsSubmission $request, QuestionsSubmission $questionsSubmission)
    {
        $questionsSubmission->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
