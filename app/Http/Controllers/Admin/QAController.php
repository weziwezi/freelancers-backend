<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\QA\IndexQA;
use App\Http\Requests\Admin\QA\StoreQA;
use App\Http\Requests\Admin\QA\UpdateQA;
use App\Http\Requests\Admin\QA\DestroyQA;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\{
    User,
    Client
};
use App\Constants\{
    UserStatus,
    UserType
};

class QAController extends Controller
{

    private function getUserAppends()
    {
        return array('q_a_s_request_url', 'gender_name');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  IndexQA $request
     * @return Response|array
     */
    public function index(IndexQA $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'username', 'email', 'first_name', 'last_name', 'user_status', 'client_id'],
                // set columns to searchIn
                ['id', 'username', 'email', 'first_name', 'last_name'],
                function ($query) {
            $query->where('user_type', UserType::WEBSITE_COORDINATOR);
        }
        );

        array_map(function(User $user) {
            $user->setAppends($this->getUserAppends());
            $user->makeVisible($this->getUserAppends());
            $user->user_status = (UserStatus::ACTIVE == $user->user_status);
            $user->client_name = $user->client['name'];
        }, $data->all());



        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.q-a.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.q-a.create');

        return view('admin.q-a.create', [
            'clients' => Client::getAllClientsForDroupDownList()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreQA $request
     * @return Response|array
     */
    public function store(StoreQA $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized = $this->beforeSetInDatabase($sanitized);
        // Store the QA
        $qA = User::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/quality-assurance'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/quality-assurance');
    }

    /**
     * Display the specified resource.
     *
     * @param  QA $qA
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {

        $user->setAppends($this->getUserAppends());
        $user->makeVisible($this->getUserAppends());
        $this->authorize('admin.q-a.show', $user);

        // TODO your code goes here
    }

    /**
     * Process data before render in view form
     * @param User $user
     * @return User
     */
    private function beforeRenderToEditView(User $user)
    {
        if ($user->user_status == UserStatus::ACTIVE)
            $user->user_status = 1;
        else
            $user->user_status = 0;
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  QA $qA
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('admin.q-a.edit', $user);
        $user->setAppends($this->getUserAppends());
        $user->makeVisible($this->getUserAppends());
        return view('admin.q-a.edit', [
            'user' => $this->beforeRenderToEditView($user),
            'clients' => Client::getAllClientsForDroupDownList()
        ]);
    }

    /**
     * Edit data before set in database
     * @param array $sanitized
     * @return array
     */
    private function beforeSetInDatabase($sanitized)
    {
        $sanitized['user_type'] = UserType::WEBSITE_COORDINATOR;
        $sanitized['user_status'] = ($sanitized['user_status'] ? UserStatus::ACTIVE : UserStatus::BLOCKED);
        return $sanitized;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateQA $request
     * @param  QA $qA
     * @return Response|array
     */
    public function update(UpdateQA $request, User $user)
    {
        // Sanitize input
        $sanitized = $request->validated();
        $sanitized = $this->beforeSetInDatabase($sanitized);
        // Update changed values QA
        $user->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/quality-assurance'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/quality-assurance');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyQA $request
     * @param  QA $qA
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyQA $request, User $user)
    {
        /**
         * @todo if the coordinator has articles, the articles change status to Release to make another Coordinator Reserved.
         */
        $user->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
