<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Brackets\AdminListing\Facades\AdminListing;
use \App\Constants\{
    UserType,
    UserStatus,
    RejectionReason,
    UserGender
};
use App\Models\{
    User,
    Client
};
use App\Http\Requests\Admin\ClientWriter\{
    IndexClientWriter,
    UpdateClientWriter,
    DestroyClientWriter
};

class ClientWritersController extends Controller
{

    private function getUserAppends()
    {
        return array('clients_writers_url');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  IndexUser $request
     * @return Response|array
     */
    public function index(IndexClientWriter $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(User::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'username', 'email', 'first_name', 'second_name', 'third_name', 'last_name', 'mobile_number', 'birthday', 'gender', 'country_id', 'user_type', 'user_status', 'client_id'],
                // set columns to searchIn
                ['id', 'username', 'email', 'first_name', 'second_name', 'third_name', 'last_name', 'mobile_number', 'gender', 'client_id'],
                function ($query) {

            $where = ['user_type' => UserType::WRITER];
            if ($clinet_id = request()->input('client_id')) {
                $where['client_id'] = $clinet_id;
            }

            $query->where($where)->whereNotNull('client_id')->with('country')->with('groups')->with('manpower_info')->with('manpower_info.major');
        });
        array_map(function($user) {
            $user->append($this->getUserAppends());
            $user->load('client');
            $user->makeVisible('client');
            $user->makeVisible($this->getUserAppends());
        }, $data->all());

        if ($request->ajax()) {
            return ['data' => $data];
        }
        $clients = Client::all('id', 'name')->makeHidden('resource_url')->toArray();
        return view('admin.clients-writers.index', ['data' => $data, 'clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.user.create');

        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUser $request
     * @return Response|array
     */
    public function store(StoreUser $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the User
        $user = User::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/users'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $user->setAppends($this->getUserAppends());
        $this->authorize('admin.user.show', $user);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('admin.clients-writers.edit', $user);
        $user->loadMissing('manpower_info')->load(['groups' => function($query) {
                $query->select('groups.id');
            }]);
        $user->groupsIds = array_column($user->groups->toArray(), 'id');
        $user->append($this->getUserAppends());
        $user->makeVisible($this->getUserAppends());
        return view('admin.clients-writers.edit', [
            'user' => $user,
            'gender' => UserGender::getAllUserGenders(),
            'countries' => \App\Models\Country::all(),
            'majors' => \App\Models\Major::all(),
            'groups' => \App\Models\Group::all(),
            'status' => [
                'approve' => UserStatus::SHOULD_OPERATE,
                'reject' => UserStatus::REJECTED,
                'rejectWithReasons' => UserStatus::EXAM_IS_APPROVED
            ],
            'rejectResons' => RejectionReason::getRejectResonForRegistrationRequest()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUser $request
     * @param  User $user
     * @return Response|array
     */
    public function update(UpdateClientWriter $request, User $user)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values User
        $user->update($sanitized);
        $user->manpower_info->update([
            'skype_account_id' => $sanitized['manpower_info']['skype_account_id'],
            'sub_major' => $sanitized['manpower_info']['sub_major'],
            'major_id' => $sanitized['manpower_info']['major_id']
        ]);
        if (isset($sanitized['groupsIds']))
            $user->groups()->sync($sanitized['groupsIds']);

        if ($request->ajax()) {
            return ['redirect' => url('admin/clients/writers/'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/clients/writers/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyUser $request
     * @param  User $user
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyClientWriter $request, User $user)
    {
        $user->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
