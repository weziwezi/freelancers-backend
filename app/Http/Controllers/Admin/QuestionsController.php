<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Question\IndexQuestion;
use App\Http\Requests\Admin\Question\StoreQuestion;
use App\Http\Requests\Admin\Question\UpdateQuestion;
use App\Http\Requests\Admin\Question\DestroyQuestion;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\{
    Question,
    QuestionType,
    Group
};

class QuestionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexQuestion $request
     * @return Response|array
     */
    public function index(IndexQuestion $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Question::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'header', 'question_type_id'],
                // set columns to searchIn
                ['id', 'header', 'body'],
                function ($query) {
            if ($question_type_id = request()->input('question_type_id')) {
                $query->where('question_type_id', $question_type_id);
            }
        });

        if ($request->ajax()) {
            return ['data' => $data];
        }

        $questionTypes = QuestionType::all()->toArray();

        return view('admin.question.index', ['data' => $data, 'questionTypes' => $questionTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.question.create');

        return view('admin.question.create', [
            'question_types' => QuestionType::where('is_active', 1)->get(),
            'groups' => Group::where('is_active', 1)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreQuestion $request
     * @return Response|array
     */
    public function store(StoreQuestion $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Question
        $question = Question::create($sanitized);

        $question->groups()->sync($sanitized['group_id']);

        if ($request->ajax()) {
            return ['redirect' => url('admin/questions'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  Question $question
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Question $question)
    {
        $this->authorize('admin.question.show', $question);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Question $question
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Question $question)
    {

        $this->authorize('admin.question.edit', $question);
        return view('admin.question.edit', [
            'question' => $question,
            'question_types' => QuestionType::all(),
            'groups' => Group::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateQuestion $request
     * @param  Question $question
     * @return Response|array
     */
    public function update(UpdateQuestion $request, Question $question)
    {
        // Sanitize input
        $sanitized = $request->validated();
        // Update changed values Question
        $question->update($sanitized);


        $question->groups()->sync($sanitized['group_id']);

        if ($request->ajax()) {
            return ['redirect' => url('admin/questions'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyQuestion $request
     * @param  Question $question
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyQuestion $request, Question $question)
    {
        $question->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
