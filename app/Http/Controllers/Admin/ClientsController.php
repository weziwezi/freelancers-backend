<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Client\IndexClient;
use App\Http\Requests\Admin\Client\StoreClient;
use App\Http\Requests\Admin\Client\UpdateClient;
use App\Http\Requests\Admin\Client\DestroyClient;
use Brackets\AdminListing\Facades\AdminListing;
use App\Models\Client;

class ClientsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexClient $request
     * @return Response|array
     */
    public function index(IndexClient $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Client::class)->processRequestAndGet(
                // pass the request with params
                $request,
                // set columns to query
                ['id', 'name', 'post_article_api_url', 'get_main_categories_api_url', 'get_sub_categories_api_url', 'website_url', 'is_active'],
                // set columns to searchIn
                ['id', 'name', 'post_article_api_url', 'get_main_categories_api_url', 'get_sub_categories_api_url', 'website_url']
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.client.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.client.create');

        return view('admin.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreClient $request
     * @return Response|array
     */
    public function store(StoreClient $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Client
        $client = Client::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/manage_clients'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/manage_clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  Client $client
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Client $client)
    {
        $this->authorize('admin.client.show', $client);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Client $client
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Client $client)
    {
        $this->authorize('admin.client.edit', $client);

        return view('admin.client.edit', [
            'client' => $client,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateClient $request
     * @param  Client $client
     * @return Response|array
     */
    public function update(UpdateClient $request, Client $client)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Client
        $client->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/manage_clients'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/manage_clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyClient $request
     * @param  Client $client
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyClient $request, Client $client)
    {
        $client->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
