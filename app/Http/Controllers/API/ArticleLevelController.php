<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ArticleLevelRepositoryInterface;
use App\Http\Requests\API\ArticleLevel\{
    CreateArticleLevelRequest,
    UpdateArticleLevelRequest,
    SearchArticleLevelRequest
};

class ArticleLevelController extends Controller
{

    protected $articleLevelRepository;

    public function __construct(ArticleLevelRepositoryInterface $articleLevelRepository)
    {
        $this->articleLevelRepository = $articleLevelRepository;
    }

    public function createArticleLevel(CreateArticleLevelRequest $request)
    {
        $result = $this->articleLevelRepository->addArticleLevel($request->all());
        if (count($result)) {
            return response()->success($result, 'success');
        } else {
            return response()->error('invalid data');
        }
    }

    public function updateArticleLevel(UpdateArticleLevelRequest $request, int $articleLevelId)
    {
        $result = $this->articleLevelRepository->editArticleLevel($request->all(), $articleLevelId);
        if (count($result)) {
            return response()->success($result, 'success');
        } else {
            return response()->error('invalid data');
        }
    }

    public function getArticleLevel(int $articleLevelId)
    {
        
        $result = $this->articleLevelRepository->getArticleLevel($articleLevelId);
        if (count($result)) {
            return response()->success($result, 'success');
        } else {
            return response()->error('invalid data');
        }
    }

    public function getArticleLevels(SearchArticleLevelRequest $request)
    {
        return response()->success($this->articleLevelRepository->getArticleLevels($request->all()), 'success');
    }

}
