<?php

namespace App\Http\Controllers\API;

use App\Constants\UserType;
use App\Interfaces\PenaltyRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\setClientPenaltiesRequest;

class PenaltyController extends Controller
{

    protected $penaltyRepository;

    public function __construct(PenaltyRepositoryInterface $penaltyRepository)
    {
        $this->penaltyRepository = $penaltyRepository;
    }

    public function getSystemPenalties(int $userType)
    {
        $manpowerUserTypes = UserType::getUserTypesIdByRoleName('manpower');

        if (!in_array($userType, $manpowerUserTypes)) {
            $message = 'user type must be a manpower user type';
            return response()->forbidden($message);
        }

        $penalties = $this->penaltyRepository->getSystemPenaltiesByUserType($userType);

        return !empty($penalties) ? response()->success($penalties, 'success') : response()->error('no penalties to return');
    }

    /**
     * Create or Update Penalties 
     */
    public function setClientPenalties(setClientPenaltiesRequest $request)
    {
        return response()->success($this->penaltyRepository->setClientPenalties($request->all()), 'success');
    }

}
