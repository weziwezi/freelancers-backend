<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ArticleRepositoryInterface;
// request forms
use App\Http\Requests\API\Article\{
    CreateArticlesRequest,
    CreateArticleRequest,
    UpdateArticleRequest,
    SearchArticleRequest,
    ApprovedArticleRequest
};

class ArticleController extends Controller
{

    protected $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * Create Multi Article
     * @param CreateArticlesRequest $request
     * @return type
     */
    public function createArticles(CreateArticlesRequest $request)
    {
        $result = $this->articleRepository->insertArticles($request->all());
        if ($result['flag']) {
            return response()->success(['titlesExtsis' => $result['titlesExtsis']], 'success');
        }
        return response()->error($result['message']);
    }

    /**
     * Create Article
     * @param CreateArticleRequest $request
     * @return response
     */
    public function createArticle(CreateArticleRequest $request)
    {
        $result = $this->articleRepository->insertArticle($request->all());
        if ($result['flag']) {
            return response()->success([], $result['message']);
        }
        return response()->error($result['message']);
    }

    /**
     * Update Article
     * @param UpdateArticleRequest $request
     * @param int $articleId
     * @return array
     */
    public function updateArticle(UpdateArticleRequest $request, int $articleId)
    {
        if ($this->articleRepository->updateArticle($request->all(), $articleId))
            return response()->success([], 'success');
        return response()->error('can\'t Update');
    }

    /**
     * Get article information using id
     * @param int $articleId
     * @return array article information
     */
    public function getArticle(int $articleId)
    {
        return response()->success($this->articleRepository->getArticle($articleId), 'success');
    }

    /**
     * Get All Articles by filters
     * @param SearchArticleRequest $request
     * @return Array Articles Information
     */
    public function getArticles(SearchArticleRequest $request)
    {
        return response()->success($this->articleRepository->getArticles($request->all()), 'success');
    }

    public function deleteArticle($articleId)
    {
        if ($this->articleRepository->deleteArticle($articleId))
            return response()->success([], 'Success');
        return response()->error('invalid');
    }

    public function approvedArticles(ApprovedArticleRequest $request)
    {
        $updateResult = $this->articleRepository->approvedArticles($request->articlesIds);
        if (count($updateResult))
            return response()->success(['count' => $updateResult[0]], 'success');
        return response()->error('some error');
    }

}
