<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserTokenRepositoryInterface;
use App\Http\Requests\API\UserTokenVerificationRequest;

class UserTokenController extends Controller
{

    protected $usertokenRepository;

    public function __construct(UserTokenRepositoryInterface $usertokenRepository)
    {
        $this->usertokenRepository = $usertokenRepository;
    }

    public function verification(UserTokenVerificationRequest $request)
    {
        if ($this->usertokenRepository->verificationToken($request->all()))
            return response()->success([], 'success');
        else
            return response()->unauthorized(__('invalid token'));
    }

}
