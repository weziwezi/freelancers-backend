<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\GroupRepositoryInterface;

class GroupController extends Controller
{

    private $groupRepository;

    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    public function getGroups()
    {
        return response()->success($this->groupRepository->getSystemGroups(), 'success');
    }

}
