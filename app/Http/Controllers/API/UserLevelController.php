<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Interfaces\UserLevelRepositoryInterface;
use App\Constants\UserType;

class UserLevelController extends Controller
{

    private $userLevelRepository;

    public function __construct(UserLevelRepositoryInterface $userLevelRepository)
    {
        $this->userLevelRepository = $userLevelRepository;
    }

    public function getUserLevelsByUserType(int $userType)
    {
        if (!in_array($userType, UserType::getUserTypesIdByRoleName('manpower')))
            return response()->error('Invalid Data');

        return response()->success($this->userLevelRepository->getUserLevelsByUserType($userType), 'success');
    }

}
