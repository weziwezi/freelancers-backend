<?php

namespace App\Http\Controllers\API;

use App\Interfaces\CountryRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    protected $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function index()
    {
        $countries = $this->countryRepository->getCountries();

        if(empty($countries)) {
            $message = 'Could not find data';
            return response()->notFound($countries, $message);
        }

        $message = 'success';
        return response()->success($countries, $message);
    }
}
