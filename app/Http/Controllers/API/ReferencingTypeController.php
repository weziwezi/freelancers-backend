<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\ReferencingType;

class ReferencingTypeController extends Controller
{

    public function getReferencingType()
    {
        return response()->success(ReferencingType::getReferencingType(), 'success');
    }

}
