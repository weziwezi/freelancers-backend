<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ClientRepositoryInterface;

class ClientController extends Controller
{

    protected $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getAPILinks()
    {
        return response()->success($this->clientRepository->getAuthAPILink(), 'success');
    }

}
