<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ReferanceRepositoryInterface;

class ReferenceController extends Controller
{

    protected $refernaceRepository;

    public function __construct(ReferanceRepositoryInterface $referanceRepository)
    {
        $this->refernaceRepository = $referanceRepository;
    }

    public function getReferencesBySubgroupId(int $subgroupId)
    {
        return response()->success($this->refernaceRepository->getReferencesBySubgroupId($subgroupId), 'success');
    }

}
