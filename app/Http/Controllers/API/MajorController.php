<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\MajorRepositoryInterface;

class MajorController extends Controller
{

    private $majorRepository;

    public function __construct(MajorRepositoryInterface $majorRepository)
    {
        $this->majorRepository = $majorRepository;
    }

    public function getMajors()
    {
        return response()->success($this->majorRepository->getMajors(), 'success');
    }

}
