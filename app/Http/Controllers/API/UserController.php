<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserRepositoryInterface;
// Requests
use App\Http\Requests\API\User\{
    UpdateCurrentUserProfileRequest,
    UpdateClientManpowerRequest,
    CreateClientManpowerRequest,
    GetWritersRequest,
    GetProofreadersRequest,
    CreateCoordinatorRequest,
    UpdateClientCoordinatorRequest,
    LoginRequest,
    RegistrationRequest,
    RegistrationStepTwoRequest,
    GetManpowerUsersExamsRequest,
    SaveManpowerUsersExamsRequest,
    RegistrationStepFourRequest
};
// Constants
use App\Constants\{
    UserStatus,
    UserType
};

/**
 * Class UserRepository is responsible for handling the users operations (login, register, etc...)
 *
 * @property \App\Repositories\UserRepository $userRepository
 */
class UserController extends Controller
{

    private $userRepository;

    /**
     * @desc class constructor
     *
     * @param \App\Interfaces\UserRepositoryInterface $userRepository
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @desc system users login function
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $result = $this->userRepository->login($request->toArray());

        if (empty($result)) {
            // Get the message from the database once we decide the response messages:
            $message = 'failed to login';
            return response()->error($message);
        }


        $userStatus = $result['user_status'];

        if ($userStatus == UserStatus::BLOCKED) {
            // Get the message from the database once we decide the response messages:
            $message = 'user is blocked';
            return response()->forbidden($message);
        } elseif ($userStatus == UserStatus::EMAIL_VERIFICATION) {
            // Get the message from the database once we decide the response messages:
            $message = 'please verify your email';
            return response()->forbidden($message);
            #} elseif ($userStatus == UserStatus::ACTIVE) {
        } else { #if ($userStatus == UserStatus::ACTIVE) {
            // Get the message from the database once we decide the response messages:
            $message = 'successfully logged in';
            return response()->success($result, $message);
        }
    }

    /**
     * @desc This function will call the required functions to register a website coordinator
     *
     * @param CreateCoordinatorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function createCoordinator(CreateCoordinatorRequest $request)
    {
        $result = $this->userRepository->addCoordinator($request->toArray());

        return $result !== null ? response()->success($result, 'successfully created') : response()->error('failed to create record');
    }

    /**
     * This function will call the required functions to Create Client Manpower
     * @param CreateClientManpowerRequest $request
     * @return mixed
     * @throws \Throwable
     */
    public function createClientManpower(CreateClientManpowerRequest $request)
    {
        $result = $this->userRepository->createClientUser($request);
        return $result !== null ? response()->success($result, 'successfully created') : response()->error('failed to create record');
    }

    /**
     * Update Client Manpower User
     * @param UpdateClientManpowerRequest $request
     * @param int $userId User ID
     * @return type
     */
    public function updateClientManpower(UpdateClientManpowerRequest $request, int $userId)
    {
        $result = $this->userRepository->updateClientUser($request, $userId);
        return $result !== null ? response()->success($result, 'successfully updated') : response()->error('failed to update record');
    }

    /**
     * @param UpdateClientCoordinatorRequest $request
     * @return mixed
     * @throws \Throwable
     */
    public function updateClientCoordinator(UpdateClientCoordinatorRequest $request, int $userId)
    {
        $result = $this->userRepository->updateClientCoordinatorInfo($request->toArray(), $userId);
        return $result !== null ? response()->success($result, 'successfully updated') : response()->error('failed to update record');
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getClientManpowerInfo(int $userId)
    {
        $userData = $this->userRepository->getClientManpowerInfoByUserId($userId);
        return !empty($userData) ? response()->success($userData, 'success') : response()->notFound($userData, 'user record does not exist');
    }

    /**
     * @param int $userId
     * @return mixed
     */
    public function getClientCoordinatorInfo(int $userId)
    {
        $userData = $this->userRepository->getClientCoordinatorInfoByUserId($userId);

        return !empty($userData) ? response()->success($userData, 'success') : response()->notFound($userData, 'user record does not exist');
    }

    /**
     * Return Logged in User Profile
     * @return array
     */
    public function getCurrentUserProfile()
    {
        return response()->success($this->userRepository->getCurrentUserProfile(), 'success');
    }

    /**
     * Update logged in user profile
     * @param UpdateCurrentUserProfileRequest $request
     * @return reponse
     */
    public function updateCurrentUserProfile(UpdateCurrentUserProfileRequest $request)
    {
        if ($this->userRepository->updateCurrentUserProfile($request->all()))
            return response()->success([], 'success');
        return response()->error('Can\'t Update');
    }

    /**
     * Get All Writers
     * @param GetWritersRequest $request
     * @return array
     */
    public function getWriters(GetWritersRequest $request)
    {
        return response()->success($this->userRepository->getWriters($request->all()), 'success');
    }

    /**
     * Get All Proofreaders
     * @param GetProofreadersRequest $request
     * @return array
     */
    public function getProofreaders(GetProofreadersRequest $request)
    {
        return response()->success($this->userRepository->getProofreaders($request->all()), 'success');
    }

    /**
     * Get All General Proofreaders
     * @param GetProofreadersRequest $request
     * @return array
     */
    public function getGeneralProofreaders(GetProofreadersRequest $request)
    {
        return response()->success($this->userRepository->getGeneralProofreaders($request->all()), 'success');
    }

    /**
     * Get All Language Proofreaders
     * @param GetProofreadersRequest $request
     * @return array
     */
    public function getLanguageProofreaders(GetProofreadersRequest $request)
    {
        return response()->success($this->userRepository->getLanguageProofreaders($request->all()), 'success');
    }

    /**
     * Get All Content Proofreaders
     * @param GetProofreadersRequest $request
     * @return array
     */
    public function getContentProofreaders(GetProofreadersRequest $request)
    {
        return response()->success($this->userRepository->getContentProofreaders($request->all()), 'success');
    }

    public function getCoordinators(UpdateClientCoordinatorRequest $request)
    {
        return response()->success($this->userRepository->getCoordinators($request->all()), 'success');
    }

    public function getWriter(int $userId)
    {
        $userData = $this->userRepository->getClientManpowerInfoByUserId($userId, UserType::WRITER);
        return !empty($userData) ? response()->success($userData, 'success') : response()->notFound($userData, 'user record does not exist');
    }

    public function getProofreader(int $userId)
    {
        $userData = $this->userRepository->getClientManpowerInfoByUserId($userId, UserType::getProofreadersUserTypesIds());
        return !empty($userData) ? response()->success($userData, 'success') : response()->notFound($userData, 'user record does not exist');
    }

    /**
     * 
     * @param RegistrationRequest $request
     * @return type
     */
    public function registration(RegistrationRequest $request)
    {
        $token = $this->userRepository->registrationUser($request->all());
        if (count($token))
            return response()->success($token, 'success');
        return response()->error('Sorry Can\'t Register Now Please Contact us');
    }

    /**
     * 
     * @param RegistrationStepTwoRequest $request Use this Request Validation for Required
     * @param UpdateClientManpowerRequest $requestUpdate Use this For Update
     * @return type
     */
    public function registrationStepTwo(RegistrationStepTwoRequest $request, UpdateClientManpowerRequest $requestUpdate)
    {
        $result = $this->userRepository->registrationUserStepTwo($requestUpdate);
        if (count($result))
            return response()->success($result, 'success');
        return response()->error('Can\'t update');
    }

    public function registrationStepFour(RegistrationStepFourRequest $request, UpdateClientManpowerRequest $requestUpdate)
    {
        if ($this->userRepository->registrationUserStepFour($requestUpdate))
            return response()->success([], 'please wait approved');
        return response()->error('Can\'t submit');
    }

    public function getExam(GetManpowerUsersExamsRequest $request)
    {
        $exams = $this->userRepository->getManpowerUsersExams();
        return $exams;
    }

    public function saveExam(GetManpowerUsersExamsRequest $request)
    {
        if ($this->userRepository->createManpowerUsersExams($request->all()))
            return response()->success([], 'please wait approved');
        return response()->error('Can\'t submit');
    }

}
