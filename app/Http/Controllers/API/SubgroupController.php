<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\SubgroupRepositoryInterface;
use App\Http\Requests\API\{
    CreateSubgroupAndReferencesRequest,
    UpdateSubgroupAndReferencesRequest
};

class SubgroupController extends Controller
{

    private $subgroupRepository;

    public function __construct(SubgroupRepositoryInterface $subgroupRepository)
    {
        $this->subgroupRepository = $subgroupRepository;
    }

    public function getClientSubgroups()
    {
        return response()->success($this->subgroupRepository->getClientSubgroups(), 'Success');
    }

    public function getClientSubgroup(int $subgroupId)
    {
        return response()->success($this->subgroupRepository->getClientSubgroup($subgroupId), 'Success');
    }

    public function createClientSubgroups(CreateSubgroupAndReferencesRequest $request)
    {
        return response()->success($this->subgroupRepository->createSubgroupAndReferences($request->all()), 'Success');
    }

    public function updateClientSubgroups(UpdateSubgroupAndReferencesRequest $request, int $subgroupId)
    {
        return response()->success($this->subgroupRepository->updateSubgroupAndReferences($subgroupId, $request->all()), 'Success');
    }

}
