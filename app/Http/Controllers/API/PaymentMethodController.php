<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\PaymentMethodRepositoryInterface;
/**
 * 
 */
class PaymentMethodController extends Controller
{

    private $PaymentMethodRepository;

    /**
     * 
     * @param PaymentMethodRepositoryInterface $PaymentMethodRepository
     */
    public function __construct(PaymentMethodRepositoryInterface $PaymentMethodRepository)
    {
        $this->PaymentMethodRepository = $PaymentMethodRepository;
    }

    public function getPaymentMethods()
    {
        return response()->success($this->PaymentMethodRepository->getPaymentMethods(), 'Success');
    }

}
