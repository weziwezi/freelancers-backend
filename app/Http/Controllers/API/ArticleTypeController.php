<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\ArticleType;

class ArticleTypeController extends Controller
{

    public function getArticleTypes()
    {
        return response()->success(ArticleType::getArticleTypes(), 'success');
    }

}
