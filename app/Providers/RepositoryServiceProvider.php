<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// Interfaces
use App\Interfaces\{
    CountryRepositoryInterface,
    PenaltyRepositoryInterface,
    UserRepositoryInterface,
    UserLevelRepositoryInterface,
    SubgroupRepositoryInterface,
    GroupRepositoryInterface,
    PaymentMethodRepositoryInterface,
    MajorRepositoryInterface,
    ClientRepositoryInterface,
    ArticleLevelRepositoryInterface,
    ArticleRepositoryInterface,
    ReferanceRepositoryInterface,
    UserTokenRepositoryInterface
};
// Repositories
use App\Repositories\{
    CountryRepository,
    PenaltyRepository,
    UserRepository,
    UserLevelRepository,
    SubgroupRepository,
    GroupRepository,
    PaymentMethodRepository,
    MajorRepository,
    ClientRepository,
    ArticleLevelRepository,
    ArticleRepository,
    ReferanceRepository,
    UserTokenRepository
};

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
                UserRepositoryInterface::class,
                UserRepository::class
        );
        $this->app->bind(
                UserLevelRepositoryInterface::class,
                UserLevelRepository::class
        );
        $this->app->bind(
                SubgroupRepositoryInterface::class,
                SubgroupRepository::class
        );
        $this->app->bind(
                GroupRepositoryInterface::class,
                GroupRepository::class
        );

        $this->app->bind(
                CountryRepositoryInterface::class,
                CountryRepository::class
        );
        $this->app->bind(
                PaymentMethodRepositoryInterface::class,
                PaymentMethodRepository::class
        );
        $this->app->bind(
                MajorRepositoryInterface::class,
                MajorRepository::class
        );

        $this->app->bind(
                PenaltyRepositoryInterface::class,
                PenaltyRepository::class
        );
        $this->app->bind(
                ClientRepositoryInterface::class,
                ClientRepository::class
        );
        $this->app->bind(
                ArticleLevelRepositoryInterface::class,
                ArticleLevelRepository::class
        );
        $this->app->bind(
                ArticleRepositoryInterface::class,
                ArticleRepository::class
        );
        $this->app->bind(
                ReferanceRepositoryInterface::class,
                ReferanceRepository::class
        );
        $this->app->bind(
                UserTokenRepositoryInterface::class,
                UserTokenRepository::class
        );
    }

}
