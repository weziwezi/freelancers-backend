<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as HttpResponse;

class ResponseMacroServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data, $message) {
            return Response::json([
                        'message' => $message,
                        'data' => $data,
                            ], HttpResponse::HTTP_OK);
        });


        Response::macro('error', function ($message) {
            return Response::json([
                        'message' => $message,
                            ], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        });

        Response::macro('forbidden', function ($message) {
            return Response::json([
                        'message' => $message,
                            ], HttpResponse::HTTP_FORBIDDEN);
        });

        Response::macro('notFound', function ($data, $message) {
            return Response::json([
                        'message' => $message,
                        'data' => $data,
                            ], HttpResponse::HTTP_NOT_FOUND);
        });

        Response::macro('created', function ($message) {
            return Response::json([
                        'message' => $message,
                            ], HttpResponse::HTTP_CREATED);
        });
        Response::macro('unauthorized', function ($message) {
            return Response::json([
                        'message' => $message,
                            ], HttpResponse::HTTP_UNAUTHORIZED);
        });
    }

}
