<?php

namespace App\Repositories;

use App\Interfaces\UserLevelRepositoryInterface;
use App\Models\UserLevel;
use App\Constants\BusinessCriteria;

class UserLevelRepository extends BaseRepository implements UserLevelRepositoryInterface
{

    protected $userLevelModel;

    public function __construct(UserLevel $userLevel)
    {
        $this->userLevelModel = $userLevel;
    }

    public function getUserLevelsByUserType(int $userType): array
    {
        #return $this->userLevelModel->select('id', 'name')->where('user_type', $userType)->paginate(BusinessCriteria::NUMBER_OF_PAGES_PER_PAGINATION)->toArray();
        return $this->userLevelModel->select('id', 'name')->where('user_type', $userType)->get()->toArray();
    }

}
