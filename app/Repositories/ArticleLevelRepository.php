<?php

namespace App\Repositories;

use App\Interfaces\ArticleLevelRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Constants\{
    UserType,
    ArticleType
};
use App\Models\{
    ArticleLevel,
    Client,
};
use App\Constants\BusinessCriteria;

class ArticleLevelRepository extends BaseRepository implements ArticleLevelRepositoryInterface
{

    protected $articleLevelModel;
    protected $clientModel;

    /**
     * 
     * @param ArticleLevel $articleLevel
     * @param Client $client
     */
    public function __construct(ArticleLevel $articleLevel, Client $client)
    {
        $this->articleLevelModel = $articleLevel;
        $this->clientModel = $client;
    }

    /**
     * Return only one article level By Id
     * @param int $articleLevelId
     * @return array
     */
    public function getArticleLevel(int $articleLevelId): array
    {
        if (!$articleLevel = $this->articleLevelModel->where(['id' => $articleLevelId, 'client_id' => Auth::user()->client_id])->first()) {
            return [];
        }

        $articleLevel->load('user_levels:user_levels.id');
        $articleLevel->load('subgroups');
        $articleLevel->makeHidden(['client_id', 'user_id', 'number_of_internal_links', 'deleted_at', 'updated_at', 'created_at', 'subgroups']);
        $articleLevel->subgroups_ids = array_column($articleLevel->subgroups->toArray(), 'id');
        $articleLevel->user_levels->makeHidden(['pivot', 'resource_url']);

        return $articleLevel->toArray();
    }

    /**
     * Return All Article Level
     * @return array
     */
    public function getArticleLevels(array $data): array
    {
        $select = [
            'id',
            'name',
            'number_of_words',
            'number_of_references',
            'number_of_sub_headlines',
            'writing_delivery_time',
            'language_proofreading_delivery_time',
            'content_proofreading_delivery_time',
            'proofreading_delivery_time',
            'images_uploading_delivery_time',
            'writing_price',
            'language_proofreading_price',
            'content_proofreading_price',
            'proofreading_price',
            'images_uploading_price',
            'article_type',
            'is_active',
        ];



        $articleLevels = $this->articleLevelModel
                ->select($select)
                ->where('client_id', Auth::user()->client_id)
                ->with('subgroups');

        if (isset($data['article_type']))
            $articleLevels = $articleLevels->where('article_type', $data['article_type']);

        if (isset($data['subgroups']))
            $articleLevels = $articleLevels->whereHas('subgroups', function($query) {
                $query->whereIn('subgroups.id', (array) request()->subgroups);
            });

        $articleLevels = $articleLevels->get()->toArray();

        $articleLevels = array_map(function($articleLevel) {
            $articleLevel['status'] = [
                'id' => $articleLevel['id'],
                'is_active' => $articleLevel['is_active'],
            ];
            $articleLevel['subgroups_ids'] = array_column($articleLevel['subgroups'], 'id');
            $articleLevel['article_type_name'] = ArticleType::getArticleTypesNameById($articleLevel['article_type']);
            unset($articleLevel['subgroups']);
            return $articleLevel;
        }, $articleLevels);
        return $articleLevels;
    }

    private function prepareUserLevels($userLevels)
    {
        $userLevelsArray = [];
        foreach ($userLevels as $userType => $userLevels) {
            $userLevelsArray = array_merge($userLevels, $userLevelsArray);
        }

        return $userLevelsArray;
    }

    /**
     * create new article level
     * @param array $data
     * @return array
     */
    public function addArticleLevel(array $data): array
    {
        if (!$client_id = Auth::user()->client_id) {
            return [];
        }

        $data['client_id'] = $client_id;
        $data['user_id'] = Auth::user()->id;

        // Calculate number_of_internal_links that when user request the article
        /* if (!$words_per_internal_link = $this->clientModel->find($client_id)->words_per_internal_link) {
          return [];
          } */
        // 
        // Check if Data inserted same as data send it
        $newArticleLevel = $this->articleLevelModel->create($data);
        if (!$newArticleLevel->exists) {
            return [];
        }

        $newArticleLevel->user_levels()->sync($this->prepareUserLevels($data['user_levels']));
        $newArticleLevel->subgroups()->sync($data['subgroups']);

        return ['id' => $newArticleLevel->id];
    }

    /**
     * update article level
     * @param array $data
     * @param int $articleLevelId
     * @return array
     */
    public function editArticleLevel(array $data, int $articleLevelId): array
    {
        if (!$auth_client_id = Auth::user()->client_id) {
            return [];
        }
        // check if the articleLevel Exists
        if (!$articleLevel = $this->articleLevelModel->find($articleLevelId)) {
            return [];
        }

        // check if the article can edit from auth
        if ($articleLevel->client_id != $auth_client_id) {
            return [];
        }

        //$data['client_id'] = $auth_client_id;
        //$data['user_id'] = Auth::user()->id;
        // 
        // Calculate number_of_internal_links that when user request the article
        /* if (!$words_per_internal_link = $this->clientModel->find($client_id)->words_per_internal_link) {
          return [];
          } */
        //
        // To Do Check if Data inserted same as data send it
        if (!$articleLevel->update($data)) {
            return [];
        }

        if (isset($data['user_levels'])) {
            $articleLevel->user_levels()->sync($this->prepareUserLevels($data['user_levels']));
        }

        if (isset($data['subgroups'])) {
            $articleLevel->subgroups()->sync($data['subgroups']);
        }

        return ['id' => $articleLevel->id];
    }

}
