<?php

namespace App\Repositories;

use App\Interfaces\PaymentMethodRepositoryInterface;
use App\Models\PaymentMethod;
use App\Constants\BusinessCriteria;

class PaymentMethodRepository extends BaseRepository implements PaymentMethodRepositoryInterface
{

    protected $paymentMethodModel;

    public function __construct(PaymentMethod $paymentMethodModel)
    {
        $this->paymentMethodModel = $paymentMethodModel;
    }

    public function getPaymentMethods(): array
    {
        $paymentMethods = $this->paymentMethodModel
                ->select('id', 'name')
                ->where('is_active', '1')
                ->get();
                //->paginate(BusinessCriteria::NUMBER_OF_PAGES_PER_PAGINATION)

        if ($paymentMethods) {
            return $paymentMethods->makeHidden('resource_url')->toArray();
        }
        return [];
    }

}
