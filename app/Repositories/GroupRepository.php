<?php

namespace App\Repositories;

use App\Interfaces\GroupRepositoryInterface;
use App\Models\Group;
use App\Constants\BusinessCriteria;

class GroupRepository extends BaseRepository implements GroupRepositoryInterface
{

    protected $groupModel;

    public function __construct(Group $group)
    {
        $this->groupModel = $group;
    }

    public function getSystemGroups(): array
    {
        $groups = $this->groupModel->select('id', 'name')->where('is_active', '1')->get();
        if ($groups) {
            return $groups->makeHidden('resource_url')->toArray();
        }
        return [];
    }

}
