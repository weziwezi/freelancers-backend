<?php

namespace App\Repositories;

use App\Interfaces\PenaltyRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Models\{
    Penalty,
    Client
};

class PenaltyRepository extends BaseRepository implements PenaltyRepositoryInterface
{

    protected $penaltyModel;
    protected $clientModel;

    public function __construct(Penalty $penaltyModel, Client $client)
    {
        $this->penaltyModel = $penaltyModel;
        $this->clientModel = $client;
    }

    public function getSystemPenaltiesByUserType(int $userType): array
    {
        $penaltiesData = $this->penaltyModel::where('user_type', $userType)->get();

        $penalties = [];

        $clientId = Auth::user()->client_id;
        if (empty($clientId)) {
            return [];
        }

        $clientPenalties = $this->clientModel->find($clientId)->penalties->where('user_type', $userType);

        $clientPenalties = array_column($clientPenalties->toArray(), 'id', 'id');

        foreach ($penaltiesData as $penaltyData) {
            $penalties[] = [
                'id' => $penaltyData->id,
                'name' => $penaltyData->name,
                'checked' => (boolean) isset($clientPenalties[$penaltyData->id])
            ];
        }

        return $penalties;
    }

    public function setClientPenalties(array $data): array
    {
        $clientId = Auth::user()->client_id;
        if (empty($clientId)) {
            return [];
        }
        // Get the Client
        $client = $this->clientModel->find($clientId);
        // Get the penalty id that already have by `user type`
        $clientPenalties = $client->penalties->where('user_type', $data['user_type']);
        // Remove old Data 
        $client->penalties()->detach($clientPenalties);
        // Remove empty value   
        $data['penalty_id'] = array_filter($data['penalty_id']);
        // Don't use sync in this case, the sync remove all old data without check the type of it.
        if (count($data['penalty_id'])) {
            $client->penalties()->attach($data['penalty_id']);
        }

        return $client->load('penalties')->toArray();
    }

}
