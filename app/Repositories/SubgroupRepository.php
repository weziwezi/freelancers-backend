<?php

namespace App\Repositories;

use App\Interfaces\SubgroupRepositoryInterface;
use App\Models\Subgroup;
use Illuminate\Support\Facades\Auth;

#use App\Constants\BusinessCriteria;

class SubgroupRepository extends BaseRepository implements SubgroupRepositoryInterface
{

    protected $subgroupModel;

    public function __construct(Subgroup $subgroup)
    {
        $this->subgroupModel = $subgroup;
    }

    public function getClientSubgroups(): array
    {
        $clientId = Auth::user()->client_id;
        if (empty($clientId)) {
            return [];
        }

        // Make the result like 
        /**
         * Make the result like 
         * 'id','name','status' => {'id', 'is_active'}
         */
        $subgroups = $this->subgroupModel->select('id', 'name', 'is_active')->where('client_id', $clientId)->get()->toArray();
        $subgroups = array_map(function($subgroup) {
            $subgroup['status'] = [
                'id' => $subgroup['id'],
                'is_active' => $subgroup['is_active']
            ];
            unset($subgroup['is_active']);
            return $subgroup;
        }, $subgroups);

        return $subgroups;

        #return $this->subgroupModel->select('id', 'name')->where('client_id', $clientId)->paginate(BusinessCriteria::NUMBER_OF_PAGES_PER_PAGINATION)->toArray();
        #return $this->subgroupModel->select('id', 'name')->where('client_id', $clientId)->get()->toArray();
    }

    public function getClientSubgroup(int $subgorupId): array
    {
        $subgroup = $this->subgroupModel::find($subgorupId);
        if (empty($subgroup))
            return [];
        $references = $subgroup->references->toArray();
        $references = array_column($references, 'url', 'id');
        $references = array_values($references);

        return array_merge($subgroup->toArray(), ['references' => $references]);
    }

    public function createSubgroupAndReferences(array $data): ?array
    {

        $clientId = Auth::user()->client_id;
        if (empty($clientId)) {
            return [];
        }

        $data['client_id'] = $clientId;

        $subgroup = $this->subgroupModel::create($data);
        // prepare and insert refrenses
        if (isset($data['references'])) {
            // remove all empty url and duplicate
            $data['references'] = array_unique($data['references']);
            $data['references'] = array_filter($data['references']);
            if (count($data['references'])) {
                $references = [];
                foreach ($data['references'] as $key => $url) {
                    $references[] = ['url' => $url];
                }
                $subgroup->references()->createMany($references);
            }
        }

        #TODO make it better than @Muathabuzr
        $references = $subgroup->references->toArray();
        $references = array_column($references, 'url', 'id');
        $references = array_values($references);

        return array_merge($subgroup->toArray(), ['references' => $references]);

        #return (array) $subgroup->load('references')->toArray();
    }

    public function updateSubgroupAndReferences(int $subgroupId, array $data): ?array
    {
        $subgroup = $this->subgroupModel->find($subgroupId);
        $subgroup->update($data);
        // prepare and update refrenses
        if (isset($data['references'])) {
            // delete the old one
            $subgroup->references()->delete();
            // remove all empty url and duplicate
            $data['references'] = array_unique($data['references']);
            $data['references'] = array_filter($data['references']);
            if (count($data['references'])) {
                $references = [];
                foreach ($data['references'] as $key => $url) {
                    $references[] = ['url' => $url];
                }
                $subgroup->references()->createMany($references);
            }
        }

        #TODO make it better than @Muathabuzr

        $references = $subgroup->references->toArray();
        $references = array_column($references, 'url', 'id');
        $references = array_values($references);

        return array_merge($subgroup->toArray(), ['references' => $references]);

        //return (array_merge($subgroup->toArray(), ['references' => array_column(, 'url', 'id')]));
        /* dd($subgroup->toArray());
          dd($subgroup->makeVisible('references')->loadMissing(['references:url,subgroup_id'])->toArray());
          dd($subgroup->makeVisible('references')->load('references')->toArray());

          return (array) $subgroup->append('references')->load('references')->toArray(); */
    }

}
