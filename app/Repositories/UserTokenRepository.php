<?php

namespace App\Repositories;

use App\Interfaces\UserTokenRepositoryInterface;
use App\Models\{
    UserToken
};
use App\Constants\{
    VerificationTypes,
    UserStatus,
    BusinessCriteria
};

class UserTokenRepository extends BaseRepository implements UserTokenRepositoryInterface
{

    protected $userTokenModel;

    /**
     * 
     * @param ArticleLevel $articleLevel
     * @param Client $client
     */
    public function __construct(UserToken $userToken)
    {
        $this->userTokenModel = $userToken;
    }

    public function generateToken(int $userId): string
    {
        return md5(uniqid());
    }

    public function createUserEmailToken(int $userId): array
    {
        $token = $this->generateToken($userId);
        $userToken = $this->userTokenModel->create(['user_id' => $userId, 'token' => $token, 'type' => VerificationTypes::EMAIL])->makeVisible('token');
        return $userToken->toArray();
    }

    public function createUserMobileToken(int $userId): array
    {
        $token = random_int(10000, 99999);
        $userToken = $this->userTokenModel->create(['user_id' => $userId, 'token' => $token, 'type' => VerificationTypes::MOBILE])->makeVisible('token');
        return $userToken->toArray();
    }

    public function verificationToken(array $data): bool
    {
        $userToken = $this->userTokenModel->where('token', $data['token'])->get();

        if ($userToken->count() != 1) {
            return false;
        }
        $userToken = $userToken->first();
        $where = array();
        if ($userToken->type == VerificationTypes::EMAIL) {
            $where['is_verified_email'] = true;
            if ($userToken->user->user_status == UserStatus::EMAIL_VERIFICATION) {
                $where['user_status'] = UserStatus::MAJOR_INFO;
            }
        }
        if ($userToken->type == VerificationTypes::MOBILE) {
            $where['is_verified_mobile'] = true;
            if ($userToken->user->user_status == UserStatus::PHONE_ACTIVATION) {
                $where['user_status'] = UserStatus::EXAM_IS_NOT_PERFORMED;
            }
        }
        return ($userToken->user->update($where) ? $userToken->delete() : false);
    }

}
