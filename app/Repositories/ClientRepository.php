<?php

namespace App\Repositories;

use App\Interfaces\ClientRepositoryInterface;
use App\Models\Client;
use Illuminate\Support\Facades\Auth;

class ClientRepository extends BaseRepository implements ClientRepositoryInterface
{

    protected $clientModel;

    public function __construct(Client $client)
    {
        $this->clientModel = $client;
    }

    public function getAuthAPILink(): array
    {
        $clientId = Auth::user()->client_id;
        if (empty($clientId)) {
            return [];
        }
        $client = Auth::user()->client->toArray();
        return [
            'main_categories_api' => $client['get_main_categories_api_url'],
            'sub_categories_api' => $client['get_sub_categories_api_url'],
        ];
    }

}
