<?php

namespace App\Repositories;

use App\Interfaces\MajorRepositoryInterface;
use App\Models\Major;
use App\Constants\BusinessCriteria;

class MajorRepository extends BaseRepository implements MajorRepositoryInterface
{

    protected $majorModel;

    public function __construct(Major $major)
    {
        $this->majorModel = $major;
    }

    public function getMajors(): array
    {
        if ($majors = $this->majorModel->select('id', 'name')->get()) {
            return $majors->toArray();
        }
        return [];
    }

}
