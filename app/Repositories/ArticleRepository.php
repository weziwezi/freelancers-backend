<?php

namespace App\Repositories;

use App\Interfaces\ArticleRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Constants\{
    UserType,
    ArticleType,
    ArticleStatus,
    ReferencingType,
    BusinessCriteria
};
use App\Models\{
    Article,
    Subgroup
};

class ArticleRepository extends BaseRepository implements ArticleRepositoryInterface
{

    protected $articleModel;
    protected $subgroupModel;

    public function __construct(Article $articleModel, Subgroup $subgroupModel)
    {
        $this->articleModel = $articleModel;
        $this->subgroupModel = $subgroupModel;
    }

    /**
     * Insert More than one article based on multi title
     * @param array $data
     * @return array
     */
    public function insertArticles(array $data): array
    {
        $titleExists = [];
        // remove the space, uniqe, and empty values, then reset the indexes
        $titles = array_values(array_filter(array_unique(explode("\n", $data['titles']))));
        // check if the use has this articles 
        $checkTitlesResults = $this->articleModel->select('title')->whereIn('title', $titles)->with(['subgroup' => function($query) {
                        $query->where('client_id', Auth()->user()->client_id);
                    }])->get()->toArray();
        // Remove the exists titles if found
        if (count($checkTitlesResults)) {
            $checkTitlesResults = array_column($checkTitlesResults, 'title');
            // get the exists titles
            $titleExists = array_intersect($titles, $checkTitlesResults);
            // get the title not exists
            $titles = array_diff($titles, $checkTitlesResults);
            // reset indexes
            $titles = array_values($titles);
        }
        // check if there is titles to insert
        if (count($titles) == 0) {
            return ['flag' => 0, 'message' => 'all the titles alreay exists'];
        }
        $insertedData = [];
        foreach ($titles as $key => $title) {
            $insertedData [] = [
                'title' => $title,
                'subgroup_id' => $data['website_subgroup'],
                'client_main_category_id' => $data['main_category'],
                'client_sub_category_id' => $data['sub_category'],
                'article_status' => ArticleStatus::NOT_READY_FOR_RESERVATION,
                'article_level_id' => $data['article_level'],
                'should_be_seen_by_freelancers' => (in_array($data['display_for'], [1, 3]) ? 1 : 0),
                'should_be_seen_by_client_users' => (in_array($data['display_for'], [2, 3]) ? 1 : 0),
                'referencing_type' => ReferencingType::UNDEFINED,
                'article_type' => ArticleType::FRESH,
            ];
        }

        $insertResult = $this->articleModel->insert($insertedData);
        if ($insertResult) {
            return ['flag' => 1, 'titlesExtsis' => $titleExists];
        }
        return ['flag' => 0, 'message' => 'Sorry.., Can\'t added'];
    }

    /**
     * Insert Article
     * @param array $data
     * @return array
     */
    public function insertArticle(array $data): array
    {
        $insertedData = [
            'title' => $data['title'],
            'subgroup_id' => $data['website_subgroup'],
            'client_main_category_id' => $data['main_category'],
            'client_sub_category_id' => $data['sub_category'],
            'article_status' => ArticleStatus::NOT_RESERVED,
            'article_level_id' => $data['article_level'],
            'article_type' => ArticleType::FRESH,
            'should_be_seen_by_freelancers' => (in_array($data['display_for'], [1, 3]) ? 1 : 0),
            'should_be_seen_by_client_users' => (in_array($data['display_for'], [2, 3]) ? 1 : 0),
            'conditions' => $data['conditions'],
            'key_word' => $data['key_word'],
            'referencing_type' => $data['referencing_type'],
        ];
        $article = $this->articleModel::create($insertedData);
        if ($article->exists) {
            $article->references()->sync($data['articles_references']);
            return ['flag' => 1, 'message' => _('add successfully')];
        }
        return ['flag' => 0, 'message' => 'Sorry.., Can\'t added'];
    }

    /**
     * Update the Article
     * @param array $data
     * @param int $articleId
     * @return bool
     */
    public function updateArticle(array $data, int $articleId): bool
    {
        $article = $this->articleModel->find($articleId);
        if (!$article)
            return false;

        Auth()->user()->can('access', $article);

        //$subgroups = Auth()->user()->client->subgroups->toArray();
        // check if the article in $subgroups
        //if (count($subgroups) == 0)
        //return false;
        // get article
        //$article = $this->articleModel->find($articleId);
        // check if article in subgroups
        //if ($article && in_array($article->subgroup_id, array_column($subgroups, 'id'))) {
        // Check if data and rename
        if (isset($data['website_subgroup'])) {
            $data['subgroup_id'] = $data['website_subgroup'];
            unset($data['website_subgroup']);
        }
        if (isset($data['main_category'])) {
            $data['client_main_category_id'] = $data['main_category'];
            unset($data['main_category']);
        }
        if (isset($data['sub_category'])) {
            $data['client_sub_category_id'] = $data['sub_category'];
            unset($data['sub_category']);
        }
        if (isset($data['article_level'])) {
            $data['article_level_id'] = $data['article_level'];
            unset($data['article_level']);
        }
        if (isset($data['display_for'])) {
            $data['should_be_seen_by_freelancers'] = (in_array($data['display_for'], [1, 3]) ? 1 : 0);
            $data['should_be_seen_by_client_users'] = (in_array($data['display_for'], [2, 3]) ? 1 : 0);
            unset($data['display_for']);
        }
        $articles_references = false;
        if (isset($data['articles_references'])) {
            $articles_references = $data['articles_references'];
            unset($data['articles_references']);
        }

        // Based on Hekmat when user choose the UNDEFINED must be remove the old referances
        if (isset($data['referencing_type']) && $data['referencing_type'] == ReferencingType::UNDEFINED) {
            $articles_references = [];
        }
        if ($article) {
            $article->update($data);
            if ($articles_references !== false) {
                $articles_references = array_values(array_filter($articles_references));
                $article->references()->sync($articles_references);
            }
            return true;
        }
        //}
        return false;
    }

    private function setDisplayFor($article)
    {
        $article->display_for = 0;
        if ($article->should_be_seen_by_client_users == 0 && $article->should_be_seen_by_freelancers == 1)
            $article->display_for = 1;
        elseif ($article->should_be_seen_by_client_users == 1 && $article->should_be_seen_by_freelancers == 0)
            $article->display_for = 2;
        elseif ($article->should_be_seen_by_client_users == 1 && $article->should_be_seen_by_freelancers == 1)
            $article->display_for = 3;
    }

    public function getArticle(int $articleId): array
    {
        $article = $this->articleModel->find($articleId);
        if (!$article)
            return [];
        Auth()->user()->can('access', $article);

        /* $subgroups = Auth()->user()->client->subgroups->toArray();
          // check if the article in $subgroups
          if (count($subgroups) == 0)
          return false;
          // get article

          // check if article in subgroups
          if (!$article || !in_array($article->subgroup_id, array_column($subgroups, 'id')))
          return []; */


        // referencing_type
        // render Display for
        $this->setDisplayFor($article);
        $article->makeHidden(['pivot', 'created_at', 'updated_at', 'deleted_at', 'should_be_seen_by_client_users', 'should_be_seen_by_freelancers'])
                ->load('references:references.id')
                ->references
                ->makeHidden(['pivot', 'subgroup_id', 'created_at', 'updated_at', 'deleted_at']);

        $article = $article->toArray();
        $article['references'] = array_column($article['references'], 'id');

        return $article;
    }

    /**
     * get articles by search / filters
     * @param array $data
     * @return array Articles informations
     */
    public function getArticles(array $data): array
    {
        $perPage = BusinessCriteria::NUMBER_OF_PAGES_PER_PAGINATION;
        // Remove Pagination argument from search parameters
        if (isset($data['page'])) {
            unset($data['page']);
        }
        if (isset($data['per_page'])) {
            $perPage = $data['per_page'];
            unset($data['per_page']);
        }
        // get the user subgroups
        $subgroups = Auth()->user()->client->subgroups->toArray();
        if (count($subgroups) == 0)
            return [];
        // Get Articles
        $articles = $this->articleModel
                ->where($data)
                ->whereIn('subgroup_id', array_column($subgroups, 'id'))
                ->paginate($perPage);
        // Customies Result
        $articles->map(function($article) {
            $article->subgroup_name = $article->subgroup['name'];
            $article->article_level_name = $article->article_level['name'];
            $this->setDisplayFor($article);
            $article->makeHidden(['pivot', 'created_at', 'updated_at', 'deleted_at', 'should_be_seen_by_client_users', 'should_be_seen_by_freelancers', 'subgroup', 'article_level']);
        });

        $articlesArray = $articles->toArray();
        foreach ($articlesArray['data'] as $key => $article) {
            $articlesArray['data'][$key]['index'] = $key + $articlesArray['from'];
        }
        return $articlesArray;
    }

    public function deleteArticle(int $articleId): bool
    {
        $article = $this->articleModel->find($articleId);
        if (!$article)
            return false;
        Auth()->user()->can('access', $article);
        /* // get Subgroups
          $subgroups = Auth()->user()->client->subgroups->toArray();
          // check if the article in $subgroups
          if (count($subgroups) == 0)
          return false;
          // get article
          $article = $this->articleModel->find($articleId);
          // check if article in subgroups
          if ($article && in_array($article->subgroup_id, array_column($subgroups, 'id'))) */
        return $article->delete();
        return false;
    }

    /**
     * Approve Article or many Article
     * @param type $data
     * @return bool
     */
    public function approvedArticles(array $articlesIds): array
    {
        $subgroups = Auth()->user()->client->subgroups->toArray();
        // check if the article in $subgroups
        if (count($subgroups) == 0)
            return [];

        $articles = $this->articleModel::whereIn('id', $articlesIds)
                ->whereIn('subgroup_id', array_column($subgroups, 'id'))
                ->whereNull('deleted_at')
                ->whereNotNull('title')
                ->whereNotNull('client_main_category_id')
                ->whereNotNull('client_sub_category_id')
                ->whereNotNull('article_level_id')
                ->whereNotNull('should_be_seen_by_freelancers')
                ->whereNotNull('should_be_seen_by_client_users')
                ->whereNotNull('referencing_type')
                ->where('article_type', ArticleType::FRESH)
                ->where('article_status', ArticleStatus::NOT_READY_FOR_RESERVATION)
                ->get();
        // check if the article is all articles
        if (count($articlesIds) != $articles->count())
            return [];
        // Update the data
        return (array) $this->articleModel::whereIn('id', $articlesIds)->update(['article_status' => ArticleStatus::NOT_RESERVED]);
    }

}
