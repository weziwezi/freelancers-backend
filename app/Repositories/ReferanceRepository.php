<?php

namespace App\Repositories;

use App\Interfaces\ReferanceRepositoryInterface;
use App\Models\Reference;

class ReferanceRepository extends BaseRepository implements ReferanceRepositoryInterface
{

    protected $referanceModel;

    public function __construct(Reference $referanceModel)
    {
        $this->referanceModel = $referanceModel;
    }

    public function getReferencesBySubgroupId(int $subgroupId): array
    {
        $references = $this->referanceModel->select('id', 'url')->where('subgroup_id', $subgroupId)->get();
        if ($references) {
            return($references->toArray());
        }
        return [];
    }

}
