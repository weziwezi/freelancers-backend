<?php


namespace App\Repositories;

use App\Interfaces\CountryRepositoryInterface;
use App\Models\Country;

class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    protected $countryModel;

    public function __construct(Country $country)
    {
        $this->countryModel = $country;
    }

    public function getCountries(): array
    {
        $countries = $this->countryModel::all();

        return $countries ? $countries->toArray() : [];
    }
}