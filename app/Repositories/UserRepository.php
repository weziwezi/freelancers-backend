<?php

namespace App\Repositories;

use App\Http\Requests\API\User\{
    UpdateClientManpowerRequest,
    CreateClientManpowerRequest
};
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\{
    Auth,
    Hash,
    DB
};
use App\Helpers\AWSHelper;
use App\Constants\{
    UserStatus,
    UserType,
    BusinessCriteria
};
use App\Models\{
    User,
    Subgroup,
    UsersGroup,
    UsersSubgroup,
    ManpowerInfo,
    UserLevel,
    UserToken,
    QuestionType,
    Question,
    QuestionsSubmission
};
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Null_;
use App\Interfaces\UserTokenRepositoryInterface;

/**
 * Class UserRepository is responsible for handling the users operations (login, register, etc...)
 *
 * @property \App\Models\User $userModel
 * @property \App\Models\Subgroup $subgroup
 * @property \App\Models\UsersGroup $userGroup
 * @property \App\Models\UsersSubgroup $userSubgroup
 * @property \App\Models\UserLevel $userLevel
 * @property \App\Models\ManpowerInfo $manpowerInfo
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    protected $userModel;
    protected $subgroupModel;
    protected $userGroupModel;
    protected $userSubgroupModel;
    protected $userLevelModel;
    protected $manpowerInfoModel;
    protected $userTokenModel;
    protected $userTokenRepository;
    protected $questionTypeModel;
    protected $questionModel;
    protected $questionsSubmissionModel;

    /**
     * @desc class constructor
     *
     * @param \App\Models\{User, Subgroup, UsersGroup, UsersSubgroup, UserLevel, ManpowerInfo}
     * @return void
     */
    public function __construct(User $user, Subgroup $subgroup, UsersGroup $userGroup, UsersSubgroup $userSubgroup,
            UserLevel $userLevel, ManpowerInfo $manpowerInfo, UserToken $userToken, UserTokenRepositoryInterface $userTokenRepository,
            QuestionType $questionType, Question $question, QuestionsSubmission $questionsSubmission)
    {
        $this->userModel = $user;
        $this->subgroupModel = $subgroup;
        $this->userGroupModel = $userGroup;
        $this->userSubgroupModel = $userSubgroup;
        $this->userLevelModel = $userLevel;
        $this->manpowerInfoModel = $manpowerInfo;
        $this->userTokenModel = $userToken;
        $this->userTokenRepository = $userTokenRepository;
        $this->questionTypeModel = $questionType;
        $this->questionModel = $question;
        $this->questionsSubmissionModel = $questionsSubmission;
    }

    /**
     * @desc system users login function
     *
     * @param array $userLoginData
     * @return array
     */
    public function login(array $userLoginData): array
    {
        $userData = [];
        if (Auth::attempt($userLoginData)) {
            $user = Auth::user();

            if (in_array($user->user_status, [UserStatus::BLOCKED, UserStatus::EMAIL_VERIFICATION])) {
                return ['user_status' => $user->user_status];
                #} elseif ($user->user_status == UserStatus::ACTIVE) {
            } else {
                $userData = [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'user_type' => $user->user_type,
                    'user_status' => $user->user_status,
                    'token' => $user->createToken('MyApp')->accessToken,
                    'website_name' => ''
                ];
                if ($user->user_type == UserType::WEBSITE_MANAGER || $user->user_type == UserType::WEBSITE_COORDINATOR) {
                    $userData['website_name'] = $user->client->name;
                }
            }
        }
        return $userData;
    }

    /**
     * @desc This function will create website coordinator
     *
     * @param array $coordinatorData
     * @return ?array
     */
    public function addCoordinator(array $coordinatorData): array
    {
        // Prepare the data to create the record in users table:
        $coordinatorData['client_id'] = Auth::user()->client_id;
        $coordinatorData['user_type'] = UserType::WEBSITE_COORDINATOR;
        $coordinatorData['user_status'] = UserStatus::ACTIVE;
        $coordinatorData['password'] = Hash::make($coordinatorData['password']);

        // Create record in users table query:
        $model = $this->userModel::create($coordinatorData);

        // If the record is created, the function will return the created record as an array, otherwise it will return
        // null to the controller that is handling the request:
        return $model ? $model->toArray() : null;
    }

    /**
     * @desc This function will create website manpower record
     * @param CreateClientManpowerRequest $userData
     * @return array|null
     * @throws \Throwable
     */
    public function createClientUser(CreateClientManpowerRequest $userData): ?array
    {
        // Open transaction with the database, since we are inserting in many tables, if any error occurred during the
        // process, it will be thrown:
        $clientUser = DB::transaction(function() use ($userData) {

                    // Prepare the data to create the record in users table:
                    $userModelData = [
                        'first_name' => $userData->first_name,
                        'second_name' => $userData->second_name,
                        'third_name' => $userData->third_name,
                        'last_name' => $userData->last_name,
                        'username' => $userData->username,
                        'email' => $userData->email,
                        'password' => Hash::make($userData->password),
                        'mobile_number' => $userData->mobile_number,
                        'country_code_id' => $userData->country_code_id,
                        'birthday' => Carbon::parse($userData->birthday),
                        'gender' => $userData->gender,
                        'country_id' => $userData->country_id,
                        'user_type' => $userData->user_type,
                        'client_id' => Auth::user()->client_id,
                        'user_status' => UserStatus::ACTIVE,
                    ];

                    // Call createUserRecord function that will create a record in users table:
                    $user = $this->createUserRecord($userModelData);

                    // Prepare the data to upload user's files to AWS S3:
                    $filesToUpload = [];

                    if ($userData->certificate !== null) {
                        $filesToUpload['certificate'] = $userData->certificate;
                    }

                    if ($userData->national_id !== null) {
                        $filesToUpload['national_id'] = $userData->national_id;
                    }

                    // Call uploadUserFilesToAwsS3 function that will upload the user certificate and national id files to AWS using
                    // S3 service:
                    if (!empty($filesToUpload)) {
                        $userFilesUrls = $this->uploadUserFilesToAwsS3($filesToUpload);

                        foreach ($userFilesUrls as $key => $userFileUrl) {
                            $manpowerInfoModelData[$key] = $userFileUrl;
                        }
                    }

                    // Prepare the data to create the record in manpower_info table:
                    $manpowerInfoModelData['user_id'] = $user['id'];
                    $manpowerInfoModelData['major_id'] = $userData->major_id;
                    $manpowerInfoModelData['sub_major'] = $userData->sub_major;
                    $manpowerInfoModelData['points'] = 0;
                    $manpowerInfoModelData['user_level_id'] = $userData->user_level_id;
                    $manpowerInfoModelData['skype_account_id'] = $userData->skype_account_id;

                    // Call createManpowerInfoRecord function that will create a record in manpower_info table:
                    $this->createManpowerInfoRecord($manpowerInfoModelData);

                    // Prepare the data to create the record in users_groups table:
                    $subgroupsIds = $userData['subgroups_ids'];

                    // Call createUsersGroupsRecord function that will create a record in users_groups table:
                    $this->createUsersGroupsAndSubgroupsRecords($subgroupsIds, $user['id']);

                    // The return value of the function will be the created data in users, manpower_info, users_groups,
                    // and users_subgroups tables if the record was created, otherwise it will return null:

                    $result = $this->userModel::where('id', $user['id'])->with('manpower_info')
                                    ->with('groups')
                                    ->with('subgroups')->first();

                    return $result == null ? $result : $result->toArray();
                });

        return $clientUser;
    }

    /**
     * @desc This function will update website manpower
     * @param UpdateClientManpowerRequest $userData
     * @return array|null
     * @throws \Throwable
     */
    public function updateClientUser(UpdateClientManpowerRequest $userData, int $userId): ?array
    {
        // Open transaction with the database, since we are updating records in many tables, if any error occurred during the
        // process, it will be thrown:
        $clientUser = DB::transaction(function() use ($userData, $userId) {

                    if (isset($userData->password)) {
                        $userData->password = Hash::make($userData->password);
                    }
                    if (isset($userData->birthday)) {
                        $userData->birthday = Carbon::parse($userData->birthday);
                    }
                    if (isset($userData->status)) {
                        $userData['user_status'] = ($userData->status ? UserStatus::ACTIVE : UserStatus::BLOCKED);
                    }
                    // Prepare the data to create the record in users table:
                    $default = [
                        'first_name' => '',
                        'second_name' => '',
                        'third_name' => '',
                        'last_name' => '',
                        'username' => '',
                        'email' => '',
                        'password' => '',
                        'mobile_number' => '',
                        'birthday' => '',
                        'gender' => '',
                        'country_id' => '',
                        #'user_type' => '' ,
                        'client_id' => '',
                        'user_status' => '',
                        'country_code_id' => '',
                    ];
                    // check if the user in same client id
                    $user = $this->userModel::where(['id' => $userId, 'client_id' => Auth()->user()->client_id])->first();
                    if (!$user)
                        return null;
                    //dd($user);
                    $userModelData = array_intersect_key($userData->toArray(), $default);
                    if (count($userModelData)) {
                        // Update the record in users table:
                        $update = $user->update($userModelData);
                    }
                    // Prepare the data to create the record in manpower_info table:
                    /*
                      $manpowerInfoModelData['major_id'] = $userData->major_id;
                      $manpowerInfoModelData['sub_major'] = $userData->sub_major;
                      $manpowerInfoModelData['points'] = 0;
                      $manpowerInfoModelData['user_level_id'] = $userData->user_level_id;
                      $manpowerInfoModelData['skype_account_id'] = $userData->skype_account_id;
                     */
                    $default = [
                        'major_id' => '',
                        'sub_major' => '',
                        'points' => '',
                        'user_level_id' => '',
                        'skype_account_id' => '',
                    ];

                    $manpowerInfoModelData = array_intersect_key($userData->toArray(), $default);

                    // Prepare the data to upload user's files to AWS S3:
                    $filesToUpload = [];
                    if ($userData->certificate !== null) {
                        $filesToUpload['certificate'] = $userData->certificate;
                    }

                    if ($userData->national_id !== null) {
                        $filesToUpload['national_id'] = $userData->national_id;
                    }
                    $filesToUpload = array_filter($filesToUpload);
                    // Call uploadUserFilesToAwsS3 function that will upload the user certificate and national id files to AWS using
                    // S3 service:
                    if (count($filesToUpload)) {
                        $userFilesUrls = $this->uploadUserFilesToAwsS3($filesToUpload);
                        foreach ($userFilesUrls as $key => $userFileUrl) {
                            $manpowerInfoModelData[$key] = $userFileUrl;
                        }
                    }


                    if (count($manpowerInfoModelData)) {
                        // Update the record in manpower_info table:
                        // check if the record fround
                        $manpowerInfo = $this->manpowerInfoModel::where('user_id', $userId)->get();
                        if ($manpowerInfo->count())
                            $this->manpowerInfoModel::where('user_id', $userId)->update($manpowerInfoModelData);
                        else
                            $this->manpowerInfoModel::create(array_merge($manpowerInfoModelData, ['user_id' => $userId]));
                    }

                    if (isset($userData['groups_ids'])) {
                        $user->groups()->sync($userData['groups_ids']);
                    }

                    if (isset($userData['subgroups_ids'])) {
                        // Prepare the data to create the record in users_groups table:
                        $subgroupsIds = $userData['subgroups_ids'];

                        // Delete the users_groups and users_subgroups tables records:
                        $this->userGroupModel::where('user_id', $userId)->delete();
                        $this->userSubgroupModel::where('user_id', $userId)->delete();

                        // Call createUserGroupsAndSubgroupsRecords function that will create records in users_groups and
                        // user_subgroups tables:
                        $this->createUsersGroupsAndSubgroupsRecords($subgroupsIds, $userId);
                    }
                    // The return value of the function will be the created data in users, manpower_info, users_groups,
                    // and users_subgroups tables if the record was created, otherwise it will return null:

                    $result = $this->userModel::where('id', $userId)->with('manpower_info')
                                    ->with('groups')
                                    ->with('subgroups')->first();

                    return $result == null ? $result : $result->toArray();
                });

        return $clientUser;
    }

    public function updateClientCoordinatorInfo(array $coordinatorData, int $userId): ?array
    {
        /*
          | check if the request sent status
          | After that set the correct value for status
         */
        if (isset($coordinatorData['status'])) {
            $coordinatorData['user_status'] = UserStatus::BLOCKED;
            if ($coordinatorData['status']) {
                $coordinatorData['user_status'] = UserStatus::ACTIVE;
            }
            unset($coordinatorData['status']);
        }
        if (isset($coordinatorData['password'])) {
            $coordinatorData['password'] = Hash::make($coordinatorData['password']);
        }

        $coordinator = $this->userModel::where([
                    'id' => $userId,
                    'user_type' => UserType::WEBSITE_COORDINATOR,
                    'client_id' => Auth()->user()->client_id
                ])->update($coordinatorData);

        return $coordinator ? $this->userModel::where('id', $userId)->select(['id',
                    'first_name', 'last_name', 'username', 'email'])->first()->toArray() : null;
    }

    /**
     * @desc This function will create website manpower record in users table
     *
     * @param array $userModelData
     * @return ?array
     */
    public function createUserRecord(array $userModelData): ?array
    {
        // Create record in users table query:
        $user = $this->userModel::create($userModelData);

        // The return value will be an array of the created record if the creation of record successfully happened,
        // otherwise the return of value this function will be null:
        return $user ? $user->toArray() : null;
    }

    /**
     * @desc This function will upload manpower files to AWS S3
     *
     * @param array $filesToUpload
     * @return array
     */
    public function uploadUserFilesToAwsS3(array $filesToUpload): array
    {
        // This function will loop over the files that should be uploaded to S3, upload them, and return the link of the
        // uploaded file:
        foreach ($filesToUpload as $key => $fileToUpload) {
            // Get the file extension:
            $fileExtension = strtolower($fileToUpload->extension());

            // Generate the file unique name:
            $currentTimeStamp = time();
            $fileRandomName = md5(mt_rand(100000, 900000)) . "-" . $currentTimeStamp . "." . $fileExtension;

            // Upload the file to S3 bucket in the required folder, and get the file link:
            $fileName = "users-" . $key . "s/" . $fileRandomName;
            AWSHelper::upload($fileName, file_get_contents($fileToUpload), 'public');
            $fileUrl = AWSHelper::getUrl($fileName);
            $uploadedFilesUrls[$key . "_url"] = $fileUrl;
        }

        // Return the uploaded files links in array:
        return $uploadedFilesUrls;
    }

    /**
     * @desc This function will create website manpower record in manpower_info table
     *
     * @param array $manpowerInfoModelData
     * @return ?array
     */
    public function createManpowerInfoRecord(array $manpowerInfoModelData): ?array
    {
        // Create record in manpower_info table query:
        $manpowerInfo = $this->manpowerInfoModel::create($manpowerInfoModelData);

        // The return value will be an array of the created record if the creation of record successfully happened,
        // otherwise the return of value this function will be null:
        return $manpowerInfo ? $manpowerInfo->toArray() : null;
    }

    /**
     * @desc This function will create website manpower record in users_groups, and users_subgroups table
     *
     * @param array $subgroupsIds
     * @param int $userId
     * @return ?array
     */
    public function createUsersGroupsAndSubgroupsRecords(array $subgroupsIds, int $userId): ?array
    {
        // This function will loop over the subgroupsIds array, check their parent groups, and create a record in
        // users_groups table:
        foreach ($subgroupsIds as $subgroupId) {
            // Get the parent group id:
            $groupId = $this->subgroupModel::where('id', $subgroupId)->with('group')->first()->group->id;

            // This is defensive coding that will occur if the parent group id isn't returned, and the function return
            // value will be null:
            if (!$groupId) {
                return null;
            }

            // Create record in users_groups table query:
            if (!$this->userGroupModel::where([['user_id', '=', $userId], ['group_id', '=', $groupId]])->first()) {
                $userGroup = $this->userGroupModel::create([
                            'user_id' => $userId,
                            'group_id' => $groupId,
                ]);
            }

            // Create record in users_groups table query:
            $userSubgroup = $this->userSubgroupModel::create([
                        'user_id' => $userId,
                        'subgroup_id' => $subgroupId,
            ]);

            // This is defensive coding that will occur if the record isn't created in users_groups table, and the
            // function return value will be null:
            if (!$userGroup || !$userSubgroup) {
                return null;
            }

            // Fill the $userGroups array with the created data in users_groups table:
            $userGroups[] = $userGroup->toArray();
            $userSubgroups[] = $userSubgroup->toArray();
        }

        return [
            'user_groups' => $userGroups,
            'user_subgroups' => $userSubgroups,
        ];
    }

    /**
     * @desc This function will return client's manpower user info
     *
     * @param int $userId
     * @return array
     */
    public function getClientManpowerInfoByUserId(int $userId, $userTypes = 0): array
    {

        // Get the required user information by the user id:
        $user = $this->userModel::where(['id' => $userId, 'client_id' => Auth()->user()->client_id])->with(['manpower_info', 'subgroups']);
        if ($userTypes != 0)
            $user = $user->whereIn('user_type', (array) $userTypes);

        $user = $user->first();

        // Check if the user model value is null, then the return value of the function will be empty array:
        if (is_null($user)) {
            return [];
        }
        // check if the user is Clients Manpowoer
        if (!in_array($user->user_type, UserType::getManpowerUserTypesIds())) {
            return [];
        }
        // Loop over the user's subgroups and save their ids:
        $subgroupsIds = array_column($user->subgroups->toArray(), 'id');
        // Prepare the user data that has to be returned:
        $userData = [
            'first_name' => $user->first_name,
            'second_name' => $user->second_name,
            'third_name' => $user->third_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
            'country_code_id' => (isset($user->country_code) ? $user->country_code->id : ''),
            'mobile_number' => $user->mobile_number,
            'birthday' => $user->birthday,
            'gender' => $user->gender,
            'country_id' => $user->country_id,
            'major' => $user->manpower_info->major->id,
            'sub_major' => $user->manpower_info->sub_major,
            'skype_account_id' => $user->manpower_info->skype_account_id,
            'user_level_id' => $user->manpower_info->user_level_id,
            'certificate_url' => $user->manpower_info->certificate_url,
            'national_id_url' => $user->manpower_info->national_id_url,
            'subgroups_ids' => $subgroupsIds,
        ];

        return $userData;
    }

    /**
     * @desc This function will return client's coordinator user info
     *
     * @param int $userId
     * @return array
     */
    public function getClientCoordinatorInfoByUserId(int $userId): array
    {
        // Get the required user information by the user id:
        $user = $this->userModel::where([
                    'id' => $userId,
                    'user_type' => UserType::WEBSITE_COORDINATOR,
                    'client_id' => Auth()->user()->client_id
                ])->first();

        // Check if the user model value is null, then the return value of the function will be empty array:
        if (is_null($user)) {
            return [];
        }

        // Prepare the user data that has to be returned:
        $userData = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
        ];

        return $userData;
    }

    /**
     * Get All Writers
     * @param array $data
     * @return array
     */
    public function getWriters(array $data): array
    {
        $data['user_type'] = UserType::WRITER;
        return $this->getManpowerUsers($data);
    }

    /**
     * Get All Proofreaders
     * @param array $data
     * @return array
     */
    public function getProofreaders(array $data): array
    {
        if (!isset($data['user_type']) || (isset($data['user_type']) && $data['user_type'] == 'all')) {
            $data['user_type'] = UserType::getProofreadersUserTypesIds();
        }
        return $this->getManpowerUsers($data);
    }

    /**
     * Get All General Proofreaders
     * @param array $data
     * @return array
     */
    public function getGeneralProofreaders(array $data): array
    {
        $data['user_type'] = UserType::PROOFREADER;
        return $this->getManpowerUsers($data);
    }

    /**
     * Get All Language Proofreaders
     * @param array $data
     * @return array
     */
    public function getLanguageProofreaders(array $data): array
    {
        $data['user_type'] = UserType::LANGUAGE_PROOFREADER;
        return $this->getManpowerUsers($data);
    }

    /**
     * Get All Content Proofreaders
     * @param array $data
     * @return array
     */
    public function getContentProofreaders(array $data): array
    {
        $data['user_type'] = UserType::CONTENT_PROOFREADER;
        return $this->getManpowerUsers($data);
    }

    /**
     * get Manpwoer Users without 
     * @param array $data
     * @return array
     */
    public function getManpowerUsers(array $data): array
    {

        $perPage = BusinessCriteria::NUMBER_OF_PAGES_PER_PAGINATION;
        // Remove Pagination argument from search parameters
        if (isset($data['page'])) {
            unset($data['page']);
        }
        if (isset($data['per_page'])) {
            $perPage = $data['per_page'];
            unset($data['per_page']);
        }

        if ($data['user_type'] == UserType::WEBSITE_COORDINATOR)
            return [];
        $data['client_id'] = Auth()->user()->client_id;

        $userType = '';
        if (isset($data['user_type'])) {
            $userType = (array) $data['user_type'];
            unset($data['user_type']);
        }
        $users = $this->userModel::where($data)
                ->with(['subgroups:name', 'manpower_info:user_id,major_id,sub_major'])
                ->with('manpower_info.major:id,name');
        if (!empty($userType))
            $users->whereIn('user_type', $userType);


        $users = $users->paginate($perPage);

        $users->each(function($user) {
            $user->manpower_info->makeVisible('major');
            $user->major = $user->manpower_info->major->name . ' - ' . $user->manpower_info->sub_major;
            $user->status = [
                'id' => $user->id,
                'is_active' => ($user->user_status == UserStatus::BLOCKED ? 0 : 1)
            ];
            $user->user_type_name = UserType::getUserTypeNameById($user->user_type);
            $user->name = str_replace('  ', ' ', $user->first_name . ' ' . $user->second_name . ' ' . $user->third_name . ' ' . $user->last_name);
            $user->makeHidden($user->getVisible());
            $user->makeHidden(['birthday', 'created_at', 'updated_at', 'deleted_at', '']);
            $user->makeVisible(['id', 'name', 'username', 'major', 'status', 'subgroups', 'user_type', 'user_type_name']);
        });

        $usersArray = $users->toArray();
        foreach ($usersArray['data'] as $key => $user) {
            $usersArray['data'][$key]['index'] = $key + $usersArray['from'];
        }

        return $usersArray;
    }

    /**
     * This function get Current user information
     * @return array User information
     */
    public function getCurrentUserProfile(): array
    {
        $user = Auth()->user();
        return [
            #'name' => trim($user->first_name . " " . $user->second_name . " " . $user->third_name . " " . $user->last_name),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'username' => $user->username,
            'email' => $user->email,
                #'mobile' => $user->mobile_number
        ];
    }

    /**
     * This function for update logged in User Profile
     * @param array $data User information
     * @return bool
     */
    public function updateCurrentUserProfile(array $data): bool
    {
        $user = Auth()->user();
        // check if the user is current user
        if (isset($data['current_password'])) {
            if (Hash::check($data['current_password'], $user->password))
                $data['password'] = Hash::make($data['new_password']);
            else
                return false;
        }
        return $user->update($data);
    }

    /**
     * get All Coordinators
     * @param array $data
     * @return array
     */
    public function getCoordinators(array $data): array
    {
        $data['user_type'] = UserType::WEBSITE_COORDINATOR;
        $data['client_id'] = Auth()->user()->client_id;
        $users = $this->userModel::where($data)->get();
        $users->each(function($user) {
            $user->status = [
                'id' => $user->id,
                'is_active' => ($user->user_status == UserStatus::BLOCKED ? 0 : 1)
            ];
            $user->name = str_replace('  ', ' ', $user->first_name . ' ' . $user->second_name . ' ' . $user->third_name . ' ' . $user->last_name);
            $user->makeHidden($user->getVisible());
            $user->makeHidden(['birthday', 'created_at', 'updated_at', 'deleted_at', '']);
            $user->makeVisible(['id', 'name', 'username', 'status', 'email']);
        });

        $usersArray = $users->toArray();
        foreach ($usersArray as $key => $user) {
            $usersArray[$key]['index'] = $key + 1;
        }

        return $usersArray;
    }

    public function registrationUser(array $data): array
    {
        //dd(Mail::to($this->userModel::find)->send($user));
        $data['user_status'] = UserStatus::EMAIL_VERIFICATION;

        $user = $this->createUserRecord($data);
        if (!$user || !isset($user['id']))
            return [];
        // Create Token email
        $token = $this->userTokenRepository->createUserEmailToken($user['id']);
        //dd(Mail::to($user)->send($user));
        return $token;
    }

    public function registrationUserStepTwo(UpdateClientManpowerRequest $userData): array
    {
        $userData['user_status'] = UserStatus::PHONE_ACTIVATION;
        // Get Default User Level
        $userLevel = $this->userLevelModel::where('user_type', Auth()->user()->user_type)->where('is_default', true)->first();
        if (!$userLevel)
            return [];
        $userData['user_level_id'] = $userLevel->id;
        $updateResult = $this->updateClientUser($userData, Auth()->user()->id);
        if (!$updateResult)
            return [];
        $token = $this->userTokenRepository->createUserMobileToken(Auth()->user()->id);
        return array_merge($updateResult, $token);
    }

    public function registrationUserStepFour(UpdateClientManpowerRequest $userData): array
    {
        $userData['user_status'] = UserStatus::FILES_ARE_UPLOADED;
        $updateResult = $this->updateClientUser($userData, Auth()->user()->id);
        if (!$updateResult)
            return [];
        return $updateResult;
    }

    public function getManpowerUsersExams(): array
    {
        $user = Auth()->user()->user_type;
        // get Question that is active and for Current user type
        $questionTypes = $this->questionTypeModel::where(['user_type' => Auth()->user()->user_type, 'is_active' => true])->with('questions')->get();
        if (!$questionTypes)
            return [];
        $questions = [];
        // check if the Question type has questions and get only random one
        $questions = array_map(function($questiontype) {
            if (count($questiontype['questions'])) {
                // random Question
                $randIndex = array_rand($questiontype['questions']);
                $question = $questiontype['questions'][$randIndex];
                return [
                    'id' => $question['id'],
                    'title' => $question['header'],
                    'body' => $question['body'],
                    'question_type_name' => $questiontype['name']
                ];
            }
            return false;
        }, $questionTypes->toArray());
        // Remove Empty Array
        $questions = array_filter($questions);
        return $questions;
    }

    public function createManpowerUsersExams(array $data): bool
    {
        // ids
        // answers
        $insertData = [];
        foreach ($data['ids'] as $key => $id) {
            $insertData[] = $this->questionsSubmissionModel::create([
                        'user_id' => Auth()->user()->id,
                        'question_id' => $data['ids'][$key],
                        'answer' => $data['answers'][$key]
            ]);
        }
        if (count($insertData) != count($data['ids'])) {
            array_map(function ($qustion) {
                $qustion->forceDelete();
            }, $insertData);
            return false;
        }
        Auth()->user()->update(['user_status' => UserStatus::EXAM_IS_PERFORMED]);
        return true;
    }

}
