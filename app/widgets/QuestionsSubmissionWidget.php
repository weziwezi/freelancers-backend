<?php

namespace App\widgets;

use App\Models\User;
use App\Constants\UserStatus;

class QuestionsSubmissionWidget
{

    public static function getCountPendingQuestionSubmission()
    {
        $count = User::where('user_status', UserStatus::EXAM_IS_PERFORMED)->get()->count();
        return ($count > 9999 ? '+9999' : $count);
    }

}
