<?php

namespace App\widgets;

use App\Models\User;
use App\Constants\UserStatus;

class RegistrationRequestWidget {

    public static function getCountPendingRegistrationRequest() {
        $count =  User::where('user_status', UserStatus::FILES_ARE_UPLOADED)->get()->count();
        return ($count > 9999 ? '+9999' : $count);
    }

}
