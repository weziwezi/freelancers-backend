<?php

namespace App\widgets;

use App\Models\UserLevel;
use App\Constants\UserType;

class UserLevelWidget
{

    public static function getCountUserLevelsNotHasDefaultValue()
    {
        if (UserLevel::where('is_default', true)->get()->count() != count(UserType::getManpowerUserTypesIds())) {
            return true;
        }
        return false;
    }

}
