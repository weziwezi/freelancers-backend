<?php

namespace App\Helpers;

class AWSHelper
{
    public static function upload($filePath, $file, $visibility)
    {
        $s3 = \Storage::disk('s3');
        $info = $s3->put($filePath, $file, $visibility);
    }

    public static function getUrl($filePath){
        return \Storage::cloud()->url($filePath);
    }

    public static function getContent($fileName)
    {
        $exists = \Storage::disk('s3')->exists($fileName);
        if ($exists) {
            $s3 = \Storage::disk('s3');
            $contents = $s3->get($fileName);
            return $contents;
        } else {
            return false;
        }
    }
}

// HOW TO UPLOAD?

// IF IMAGE EXAMPLE:

// $image = \Image::make($request->file('file'));
// $imageExtension = $request->file('file')->extension();
// $randomNumber = mt_rand(100000, 900000);
// $lessonName = $request->session()->get('lesson')->title;
// $filename = "questions-images/" . $lessonName . '-' . $randomNumber . '.' . $imageExtension;
// $image->filename = $lessonName . '-' . $randomNumber . '.' . $imageExtension;
//AWSHelper::upload($filename,(string) $image->stream(), 'public');
//$result = \AWSHelper::getUrl($filename);
// IF PDF EXAMPLE:


// $file = $request->file('link');
// $extension = strtolower($file->extension());
// $filename = str_random(12).".".$extension;
// if($extension=='mp4'){
//      $filename="videos/".$filename;
// }

// \AWSHelper::upload($filename, file_get_contents($file), 'public');
// $result=\AWSHelper::getUrl($filename);
// $video->uri  = $result;
// $video->description = $request->get('description');
// $video->thumbnail_uri = $thumbnailResult;
// $video->save();