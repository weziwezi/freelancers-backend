<?php


namespace App\Interfaces;

interface PenaltyRepositoryInterface
{
    public function getSystemPenaltiesByUserType(int $userType) : array;
    
    public function setClientPenalties(array $data) : array;
}