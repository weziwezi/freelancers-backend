<?php

namespace App\Interfaces;

interface PaymentMethodRepositoryInterface
{

    public function getPaymentMethods() : array;
    
}
