<?php

namespace App\Interfaces;

interface UserLevelRepositoryInterface
{

    public function getUserLevelsByUserType(int $userType) : array;
    
}
