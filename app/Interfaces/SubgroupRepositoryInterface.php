<?php

namespace App\Interfaces;

interface SubgroupRepositoryInterface
{

    public function getClientSubgroups() : array;

    public function getClientSubgroup(int $subgroupId) : array;

    public function createSubgroupAndReferences(array $data) : ?array;

    public function updateSubgroupAndReferences(int $subgroupId, array $data) : ?array;
}
