<?php

namespace App\Interfaces;

interface ReferanceRepositoryInterface
{

    public function getReferencesBySubgroupId(int $subgroupId) : array;
}
