<?php

namespace App\Interfaces;

interface GroupRepositoryInterface
{

    public function getSystemGroups() : array;
}
