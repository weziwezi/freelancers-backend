<?php

namespace App\Interfaces;

interface CountryRepositoryInterface
{
    public function getCountries() : array;
}
