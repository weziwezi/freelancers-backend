<?php

namespace App\Interfaces;

interface ArticleLevelRepositoryInterface
{

    public function getArticleLevel(int $articleLevelId): array;

    public function getArticleLevels(array $data): array;

    public function addArticleLevel(array $data): array;

    public function editArticleLevel(array $data, int $articleLevelId): array;
}
