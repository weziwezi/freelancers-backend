<?php

namespace App\Interfaces;

interface UserTokenRepositoryInterface
{

    public function generateToken(int $userId): string;

    public function createUserEmailToken(int $userId): array;

    public function createUserMobileToken(int $userId): array;

    public function verificationToken(array $data): bool;
}
