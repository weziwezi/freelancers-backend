<?php

namespace App\Interfaces;

use App\Http\Requests\API\User\{
    CreateClientManpowerRequest,
    UpdateClientManpowerRequest
};

interface UserRepositoryInterface
{

    public function login(array $userLoginData): array;

    public function addCoordinator(array $coordinatorData): ?array;

    public function createClientUser(CreateClientManpowerRequest $userData): ?array;

    public function createUserRecord(array $userModelData): ?array;

    public function uploadUserFilesToAwsS3(array $filesToUpload): array;

    public function createManpowerInfoRecord(array $manpowerInfoModelData): ?array;

    public function createUsersGroupsAndSubgroupsRecords(array $subgroupsIds, int $userId): ?array;

    public function getWriters(array $data): array;

    public function getProofreaders(array $data): array;

    public function getGeneralProofreaders(array $data): array;

    public function getLanguageProofreaders(array $data): array;

    public function getContentProofreaders(array $data): array;

    public function getManpowerUsers(array $data): array;

    public function updateClientUser(UpdateClientManpowerRequest $userData, int $userId): ?array;

    public function getClientManpowerInfoByUserId(int $userId, $userTypes): array;

    public function getClientCoordinatorInfoByUserId(int $userId): array;

    public function updateClientCoordinatorInfo(array $coordinatorData, int $userId): ?array;

    public function getCurrentUserProfile(): array;

    public function updateCurrentUserProfile(array $data): bool;

    public function getCoordinators(array $data): array;

    public function registrationUser(array $data): array;

    public function registrationUserStepTwo(UpdateClientManpowerRequest $userData): array;

    public function registrationUserStepFour(UpdateClientManpowerRequest $userData): array;

    public function getManpowerUsersExams(): array;

    public function createManpowerUsersExams(array $data): bool;
}
