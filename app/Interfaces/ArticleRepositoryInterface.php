<?php

namespace App\Interfaces;

interface ArticleRepositoryInterface
{

    public function insertArticles(array $data): array;

    public function insertArticle(array $data): array;

    public function updateArticle(array $data, int $articleId): bool;

    public function getArticle(int $articleId): array;

    public function getArticles(array $data): array;

    public function deleteArticle(int $articleId): bool;

    public function approvedArticles(array $articlesIds): ?array;
}
