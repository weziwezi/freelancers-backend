<?php

namespace App\Interfaces;

interface ClientRepositoryInterface
{

    public function getAuthAPILink() : array;
}
